class VacApplicationMailer < ActionMailer::Base
 default from: "UNDP Nigeria Vendor Database Management System <no-reply@rosters.ng.undp.org>"


  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.vac_application_mailer.application_received.subject
  #
  def application_received(message={})
    
    send_to = message[:to]
    m = message
    @message = message
     
     
    mail :to =>message[:email] ,  :bcc => 'hakimkal@gmail.com',:subject =>message[:subject]
    
    
 end
    
   
end
