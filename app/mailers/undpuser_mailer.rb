class UndpuserMailer < ActionMailer::Base
  include SessionsHelper
  include ApplicationHelper
  default from: "UNDP Nigeria Vendor Database Management System <no-reply@rosters.ng.undp.org>"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.undpuser_notifier.signup.subject
  #
  def signup(msg={},current_user=nil)
    @message = msg
    if (!current_user.nil?)
      @current_user = current_user
    mail(:to =>"#{msg[:email]}",:bcc=>"hakimkal@gmail.com",:subject=>"UNDP Nigeria Vendor Database Enlistment Confirmation")
    else
    mail(:to =>"#{msg[:email]}",:bcc=>"hakimkal@gmail.com",:subject=>"UNDP Nigeria Vendor Signup Confirmation")
    
    end
  end
   
   
  def send_activation_link(msg= {})
    @message = msg

    mail(:to =>"#{msg[:email]}",:bcc=>"hakimkal@gmail.com",:subject=>"UNDP Nigeria Vendor Database Activation link")
    
  end
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.undpuser_notifier.forgotten_password.subject
  #
  def forgot_password(msg={})
    @message = msg

   mail(:to =>"#{msg[:email]}",:bcc=>"hakimkal@gmail.com",:subject=>"UNDP Nigeria Vendor Database Password Reset Link")
    
  end
end
