class NotificationMailer < ActionMailer::Base
  include Resque::Mailer
   default from: "UNDP Nigeria Vendor Database Management System <no-reply@rosters.ng.undp.org>"


  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notification_mailer.send_to_ngo.subject
  #
  
  
   
    
   
  
  def send_auto_email(user,msg)
    @message = msg
    msg = msg.symbolize_keys!
    u = user
        
   mail(:to =>msg[:email] ,:subject => msg[:subject])
       
  end
  
  def send_to_ngo
    @greeting = "Hi"

    mail to: "to@example.org"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notification_mailer.send_to_supplier.subject
  #
  def send_to_supplier
    @greeting = "Hi"

    mail to: "to@example.org"
  end
  
  
  
  
end
