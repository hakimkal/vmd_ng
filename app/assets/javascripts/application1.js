// NOTICE!! DO NOT USE ANY OF THIS JAVASCRIPT
// IT'S ALL JUST JUNK FOR OUR DOCS!
// ++++++++++++++++++++++++++++++++++++++++++

!function ($) {


  $(function(){


	
    // Disable certain links in docs
    $('section [href^=#]').click(function (e) {
      e.preventDefault()
    })
	
	//vsses/_new partial
	$('#vss_vendor_type_id').change(function(){
		vid = $('#vss_vendor_type_id').val();
		if(vid == '')
		{
			alert("vendor Type Category Cannot be empty");
		} 
		else{
		$.get('/vpas/listed/'+vid,
		function(data){
			eval(data);
			$("#vss_vpa_id").html("");
			$("#vss_vpa_id").removeAttr("disabled");
			 
	if(data.length > 0) {
		addOptions(data,'vss_vpa_id');}
		}) ;
		}
	});
	
    // Report bit for supplier
    $('#supplier_report_vpa_name').change(function(){

    vid = 3;
   vpa_id = $('#supplier_report_vpa_name').val();


if(vid == '')
{
alert("Vendor Type Category Cannot be empty");
}
 
if(vpa_id == '')
{
$("#vss_s").html('');
alert("Programme Area Cannot be empty");
}
 
else{
$.get("/vsses/listed"+'/'+vid + "/"+vpa_id,
function(data){
eval(data);
$("#vss_s").removeAttr("disabled");
$("#vss_s").html('');
if(data.length > 0) {
addOptions(data,'vss_s');}
},"json") ;
}});



// Report bit for NGO
    $('#ngo_report_vpa_name').change(function(){

    vid = 2;
   vpa_id = $('#ngo_report_vpa_name').val();


if(vid == '')
{
alert("Vendor Type Category Cannot be empty");
}
 
if(vpa_id == '')
{
$("#vss").html('');
alert("Programme Area Cannot be empty");
}
 
else{
$.get("/vsses/listed"+'/'+vid + "/"+vpa_id,
function(data){
eval(data);
$("#vss").removeAttr("disabled");
$("#vss").html('');
if(data.length > 0) {
addOptions(data,'vss');}
},"json") ;
}});

    // Disable certain links in docs
    $('section [href^=#]').click(function (e) {
      e.preventDefault()
    })
    // make code pretty
    window.prettyPrint && prettyPrint()

    // add-ons
    $('.add-on :checkbox').on('click', function () {
      var $this = $(this)
        , method = $this.attr('checked') ? 'addClass' : 'removeClass'
      $(this).parents('.add-on')[method]('active')
    })

    

     
    // fix sub nav on scroll
    var $win = $(window)
      , $nav = $('.subnav')
      , navTop = $('.subnav').length && $('.subnav').offset().top - 40
      , isFixed = 0

    processScroll()

    // hack sad times - holdover until rewrite for 2.1
    $nav.on('click', function () {
      if (!isFixed) setTimeout(function () {  $win.scrollTop($win.scrollTop() - 47) }, 10)
    })

    $win.on('scroll', processScroll)

    function processScroll() {
      var i, scrollTop = $win.scrollTop()
      if (scrollTop >= navTop && !isFixed) {
        isFixed = 1
        $nav.addClass('subnav-fixed')
      } else if (scrollTop <= navTop && isFixed) {
        isFixed = 0
        $nav.removeClass('subnav-fixed')
      }
    }

    
     
    // button state demo
    $('#fat-btn')
      .click(function () {
        var btn = $(this)
        btn.button('loading')
        setTimeout(function () {
          btn.button('reset')
        }, 3000)
      })

 

    // javascript build logic
    var inputsComponent = $("#components.download input")
      , inputsPlugin = $("#plugins.download input")
      , inputsVariables = $("#variables.download input")

    // toggle all plugin checkboxes
    $('#components.download .toggle-all').on('click', function (e) {
      e.preventDefault()
      inputsComponent.attr('checked', !inputsComponent.is(':checked'))
    })

    $('#plugins.download .toggle-all').on('click', function (e) {
      e.preventDefault()
      inputsPlugin.attr('checked', !inputsPlugin.is(':checked'))
    })

    $('#variables.download .toggle-all').on('click', function (e) {
      e.preventDefault()
      inputsVariables.val('')
    })

    // request built javascript
    $('.download-btn').on('click', function () {

      var css = $("#components.download input:checked")
            .map(function () { return this.value })
            .toArray()
        , js = $("#plugins.download input:checked")
            .map(function () { return this.value })
            .toArray()
        , vars = {}
        , img = ['glyphicons-halflings.png', 'glyphicons-halflings-white.png']

    $("#variables.download input")
      .each(function () {
        $(this).val() && (vars[ $(this).prev().text() ] = $(this).val())
      })

      $.ajax({
        type: 'POST'
      , url: /\?dev/.test(window.location) ? 'http://localhost:3000' : 'http://bootstrap.herokuapp.com'
      , dataType: 'jsonpi'
      , params: {
          js: js
        , css: css
        , vars: vars
        , img: img
      }
      })
    })
  })
// Tree Menu
 $("#tree").dynatree({
//          autoCollapse: true,
      minExpandLevel: 1,
//          persist: true,
      onPostInit: function(isReloading, isError) {
        this.reactivate();
      },
      onActivate: function(node) {
          
        if( node.data.href ){
          // Open target
          if (node.data.target){
           
          window.open(node.data.href, node.data.target);
          }
         
         else{
         	 
         	 window.open(node.data.href, '_self');
         }
        }
      }
    });


}(window.jQuery);

tinyMCE.init({
	mode: 'textareas',
	theme: 'advanced',
	plugins : "pagebreak,table,advhr,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

	theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
    theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,cleanup,code,|,preview,|,forecolor,backcolor",
    theme_advanced_buttons4 : "styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,pagebreak",
   
	add_form_submit_trigger: true
	
});
function addOptions(cl,id) {
//enable child select and clear current child options
//repopulate child list with array from helper page
var prog = document.getElementById(id);
$("#"+id).html("").removeAttr("disabled");

prog.options[0] = new Option("", 0);
for(var i = 0; i < cl.length; i++) {
	 
prog.options[i+1] = new Option(cl[i].name, cl[i].id);
}
 
}

	// Submit Conservices via ajax
	
	function removeForm(id) {
 id = document.getElementById(id);
 //$(id).remove();
 $(id).clearForm();
}

function appendContent(id,content){
	
 id = document.getElementById(id);

$(id).html(content);
}