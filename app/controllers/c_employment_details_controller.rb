class CEmploymentDetailsController < ApplicationController
  before_filter :login_required , :only => ['index','new','create','update','edit','destroy','show']
  before_filter :consultants_only 
  layout :get_user_layout
  def new
  end

  def index
  end

  def create
    session[:emp_recs] = nil
     emp_recs = []
     er_ct = params[:c_employment_record].size
     session[:er_ct] = er_ct
     #for i in (0..(er_ct - 1))
     params[:c_employment_record].delete_if {|v| v[:from].blank? || v[:from] == ''}
     params[:c_employment_record].each do |p|
      emp_recs << {
        
      
        
        :from            => p[:from] ,
  
                      
  :to                => p[:to],                   
  :starting_gross_salary               =>p[:starting_gross_salary], 
  :final_gross_salary                   => p[:final_gross_salary], 
  :functional_title                     => p[:functional_title], 
  :un_grade                     => p[:un_grade],         
  :last_un_step                        => p[:last_un_step], 
  :name_of_employer                   => p[:name_of_employer],   
  :address_of_employer                  => p[:address_of_employer], 
  :description_of_duties                => p[:description_of_duties], 
  :reason_for_leaving                   => p[:reason_for_leaving], 
  :type_of_business                     => p[:type_of_business], 
  :employment_type                     => p[:employment_type], 
  :type_of_contract                   => p[:type_of_contract],   
  :name_of_supervisor                   => p[:name_of_supervisor], 
  :email_of_supervisor                => p[:email_of_supervisor], 
  :telephone_of_supervisor              => p[:telephone_of_supervisor], 
  :did_you_supervise_staff             => p[:did_you_supervise_staff], 
  :number_of_staff_supervised         => p[:number_of_staff], 
  :number_of_support_staff_supervised   => p[:number_of_support_staff_supervised], 
 
  :consultant_id                        => p[:consultant_id], 
   
        
        }
       
      end
      
      
      emp_recs.delete_if {|vss|  CEmploymentRecord.count(:conditions => vss) >  0 }   #=> ["a"]
      er =""   
     
         
      @emp_detail = CEmploymentDetail.new(params[:c_employment_detail])
      
      ems = CEmploymentRecord.create!(emp_recs)
       CEmploymentRecord.calculate_work_experience(params[:c_employment_detail][:consultant_id])
      if @emp_detail.save || ems
        
       
     
        flash[:notice] = "Successfully saved"
      else
       
        @emp_detail.errors.full_messages.each do |msg|
        er += msg + "\\n"
          
        end
        er += "Name of Employer and Business of Employer can not be blank."
        er +=   "\\n"
       
            
      flash[:error] = er 
        
      end
    respond_to do |f|
      f.js
    
    end
    
  end

  def update
    @consultant = CEmploymentDetail.find(:first, :conditions => {:id =>params[:id]})
     @consultant_records = CEmploymentRecord.find(:first,:conditions =>{:consultant_id =>params[:c_employment_record][:consultant_id],:id =>params[:c_employment_record][:id]})
   
                                 
    if (@consultant.update_attributes(params[:c_employment_detail]) && @consultant_records.update_attributes(params[:c_employment_record]))
      CEmploymentRecord.calculate_work_experience(params[:c_employment_record][:consultant_id])
      flash[:notice] = "Successfully updated"
    else
      flash[:error] = "Unable to update, try again later"
    end
    respond_to do |f|
      f.html {  if identify_vendor.nil? 
              if request.env["HTTP_REFERER"].nil?
                verify_user_section
                return false
                else
                 redirect_to request.env["HTTP_REFERER"]
                 return false
                 end
              else
                redirect_to c_employment_record_path(:id => @consultant.consultant_id ,:consultant_id => @consultant.consultant_id) 
              end
                 }
      f.json
      f.js
    end
  end
  

  def edit
  end

  def destroy
  end

  def show
  end
end
