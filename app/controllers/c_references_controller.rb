class CReferencesController < ApplicationController
  
   before_filter :login_required, :only=>['destroy','index','show','new','create','update']
  before_filter :consultants_only , :only=>['destroy','index','show','new','create','update']
  before_filter :check_consultant  ,:except => ['create','add','remove']
  
  layout :get_user_layout
  def new
  end

  def create
    refs = []
 
    ct = params[:reference].size
     session[:ct] = ct
    for i in 0..(ct-1) 
    
     refs << {
        :firstname => params[:reference]["#{i}"][:firstname],
        :lastname =>  params[:reference]["#{i}"][:lastname] ,
        :town =>      params[:reference]["#{i}"][:town],
        :state =>     params[:reference]["#{i}"][:state],
        :street =>    params[:reference]["#{i}"][:street],
        :country =>   params[:reference]["#{i}"][:country],
        :mobile =>    params[:reference]["#{i}"][:mobile] ,
        :fax =>       params[:reference]["#{i}"][:fax],
        :email =>      params[:reference]["#{i}"][:email],
        :work =>      params[:reference]["#{i}"][:work],
       :consultant_id =>  params[:reference]["#{i}"][:consultant_id]
          }
     
     end
    
    refs.delete_if {|key| key[:firstname].blank? }
     
    if CReference.create(refs)
      flash[:notice]  = "saved References details sucessfully"
    else
      flash[:error] = "unable to save, try again later"
    end
    respond_to do |f|
      f.js
    end
  end

  def edit
    
    if current_user.group_id == 4
      @reference = CReference.find(:first, :conditions => {:consultant_id => params[:consultant_id],:id=>params[:id]})      
      if @reference.nil?
        flash[:error] = "Invalid referee selected"
        verify_user_section
      end
     else
       
     @reference = CReference.find(:first, :conditions => {:consultant_id => params[:consultant_id],:id=>params[:id]})      
      if @reference.nil?
         flash[:error] = "Invalid consultant referee selected"
        verify_user_section
      end
      end
  end

  def update
    if request.put?
      @reference = CReference.find(:first,:conditions=>{:id => params[:id]})
      #debugger
      if @reference && @reference.update_attributes(params[:c_reference])
        flash[:notice] = "Updated reference successfully"
       else
         flash[:error] = "Unable to update reference, try again later."
         session[:"model_error"] = @reference
      end
      respond_to do |format|
        format.html { 
                     if identify_vendor.nil?
                       if request.env["HTTP_REFERER"].nil?
                verify_user_section
                return false
                else
                 redirect_to request.env["HTTP_REFERER"]
                 return false
                 end
                     else
                       redirect_to :controller =>"c_references", 
                         :action=>"show",
                         :consultant_id => identify_vendor
                        
                     end
                     }
        format.js 
        format.json {}
        
      end
    end
  end

  def show
    
    if current_user.group_id == 4
      @consultant = Consultant.find_by_undpuser_id(current_user.id,:include => [:c_references])      
      if @consultant.nil? || @consultant.c_references.empty?
        flash[:error] = "No record found"
        verify_user_section
      end
     else
        @consultant = Consultant.find_by_id(params[:consultant_id],:include => [:c_references])
           if !@consultant || @consultant.nil? || @consultant.c_references.empty?
        flash[:error] = "no record found"
        if request.env["HTTP_REFERER"].nil?
        redirect_to undpusers_url(:type => 'vendors')
        return false
        else
         redirect_to request.env["HTTP_REFERER"]
         return false
      end
     else 
       @user_title = "For #{@consultant.firstname}  #{@consultant.lastname} :: Consultant"
      end
     
    end
  end

  def destroy
    if @reference = CReference.find(:first,:conditions=>{:id => params[:id]}).destroy()
      flash[:notice] = "Successfully deleted referee"
    else
      flash[:error] = "unable to delete referee"
    end
     respond_to do |format|
        format.html { 
                     if identify_vendor.nil?
                       if request.env["HTTP_REFERER"].nil?
                        verify_user_section
                        return false
                        else
                         redirect_to request.env["HTTP_REFERER"]
                         return false
                         end
                     else
                       redirect_to :controller =>"c_references", 
                         :action=>"show",
                         :consultant_id => identify_vendor
                        
                     end
                     }
        format.js 
        format.json {}
        
      end
  end

  def index
  end
end
