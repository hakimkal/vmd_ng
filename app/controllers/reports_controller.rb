class ReportsController < ApplicationController
  
  before_filter :login_required , :only => ['index','new','create','update','edit','destroy','show']
  before_filter :staff_only 
  #before_filter :check_consultant  ,:except => ['create','add','remove']
  layout :get_user_layout

  def new
    @c_report = ConsultantReport.new
    @n_report = NgoReport.new
    @s_report = SupplierReport.new
    
    @c_eds =  ConsultantReport.select("distinct on(qualification,c_post_qual) qualification as q1 ,c_post_qual as q2 ")
   @c_services =  ConsultantReport.select("distinct on(vss_name,service) vss_name ,service ")
    c_ser = []
     c_eds  = []
     @c_eds.each do |c|
      c_eds << c.q1.strip.upcase unless c.q1.nil?
      c_eds << c.q2.strip.upcase unless c.q2.nil?
       
     end 
     @c_eds = c_eds.uniq
    @c_services.each do |s|
      
      c_ser <<  s.vss_name.strip.upcase  unless s.vss_name.nil?
      c_ser << s.service.strip.upcase unless s.service.nil?
    end
   @c_services = c_ser.uniq
   @c_eds.delete_if{|c| c.blank?}
   @c_services.delete_if{|s| s.blank?}
 
 
 
  end

  def index
  end

  def show
  end
end
