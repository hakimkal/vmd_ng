class GroupsController < ApplicationController
   before_filter :login_required , :only => ['index','new','create','update','edit','destroy','show']
  before_filter :staff_only 
  #before_filter :check_consultant  ,:except => ['create','add','remove']
  before_filter :is_logged_in_user_admin
    
  layout :get_user_layout

  def edit
    @group = Group.find(params[:id])
  end

  def update
     @group = Group.find(params[:id])
     if @group.update_attributes(params[:group])
       flash[:notice] = "Saved successfully"
     else 
       flash[:error] = "Unable to save!"
     end
     redirect_to groups_url
     return false
  end

  def index
     @groups = Group.paginate(:page =>params[:page],:per_page =>params[:per_page])
  end
end
