class NgoReportsController < ApplicationController
  before_filter :login_required , :only => ['index','new','create','update','edit','destroy','show']
  before_filter :staff_only 
  #before_filter :check_consultant  ,:except => ['create','add','remove']
  layout :get_user_layout

  def new
  end

  def create
  end

  def edit
  end

  def destroy
  end

  def index
     @ngo_report = NgoReport.new
     
    if request.post?
      s = params[:ngo_report]
      if !s[:vpa_name].blank? && (!s[:vss_name] || (s[:vss_name].size == 1 && s[:vss_name][0].blank? || s[:vss_name][0] == "-1")) && s[:others] == "-1"
       @ngo_reports = NgoReport.select("Distinct on (name) *").where("vpa_id = ?",s[:vpa_name]).paginate(:page=>params[:page],:per_page => 50)
      return false
      
      elsif s[:vpa_name].blank? && (!s[:vss_name] || (s[:vss_name].size == 1 && s[:vss_name][0].blank? || s[:vss_name][0] == "-1")) && s[:others] != "-1"
       @ngo_reports = NgoReport.select("Distinct on (name) *").where(" ? is not null",s[:others]).paginate(:page=>params[:page],:per_page => 50)
      return false
      
      elsif !s[:vpa_name].blank? && (!s[:vss_name] || (s[:vss_name].size == 1 && s[:vss_name][0].blank? || s[:vss_name][0] == "-1")) && s[:others] != "-1"
       @ngo_reports = NgoReport.select("Distinct on (name) *").where(" vpa_id = ? and ? is not null",s[:vpa_name],s[:others]).paginate(:page=>params[:page],:per_page => 50)
      return false
      
       elsif !s[:vpa_name].blank? && ((s[:vss_name].size >= 1 && !s[:vss_name][0].blank? && s[:vss_name][0] != "-1")) && s[:others] != "-1"
       @ngo_reports = NgoReport.select("Distinct on (name) *").where(" vpa_id = ? and vss_id in(?) and ? is not null",s[:vpa_name],s[:vss_name],s[:others]).paginate(:page=>params[:page],:per_page => 50)
      
     else
         @ngo_reports = NgoReport.select("Distinct on (name) *").paginate(:page=>params[:page],:per_page => 50)
     
      return false
     
      end
    end
    
    if request.get?
      
       @ngo_reports = NgoReport.select("Distinct on (name) *").paginate(:page=>params[:page],:per_page => 50)
   
      return false
    end
    
  end

  def show
  end

  def update
  end
end
