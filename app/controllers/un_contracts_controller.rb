class UnContractsController < ApplicationController
  
   before_filter :login_required , :only => ['index','new','create','update','edit','destroy','show']
  before_filter :staff_only 
  #before_filter :check_consultant  ,:except => ['create','add','remove']
  layout :get_user_layout
  
  def select_project_account
    @account = UnContract.new
   @projects =  @accounts = Project.select("id,name").includes(:fund)
    
  end
  def new
    
    @un_contract = UnContract.new
    if request.get?
      redirect_to select_project_account_un_contracts_url
      return false
    end
    if request.post?
      if params[:name].nil? || params[:vendor].nil?
        
        
        flash[:error] = "Project account, Vendor Name and Vendor Type are required! "
        redirect_to select_project_account_un_contracts_url
        return false
      end
      @name = params[:name]
      @vendor = params[:vendor]
      case params[:vendor].to_i
      when 1
        
        @vendors = ConsultantFullname.select("id,name").where("name ilike ? ","#{params[:name]}%")
         
        
      when 2
         @vendors = Ngo.select("id, name").where("name ilike ? ",params[:name]+'%')
   
      when 3
         @vendors = Supplier.select("id, name").where("name ilike ? ",params[:name]+'%')
   
      end
      
      if @vendors.blank?
           flash[:error] = "No Entry found for vendor"
           redirect_to select_project_account_un_contracts_url
           return false
        end
      @projects =  @accounts = Project.select("id,name").includes(:fund)
  
   
    end
  end

  def create
    if request.post?
      @un_contract = UnContract.new(params[:un_contract])
      @name = params[:name]
      @vendor = params[:vendor].to_i
      
      if !@un_contract.start_date.blank? && !@un_contract.end_date.blank? 
      @un_contract.duration = Date.parse(params[:un_contract][:end_date]) - Date.parse(params[:un_contract][:start_date])
      
      if @un_contract.duration < 0
        flash[:error] = "The Contract  Start Year cannot   be LATER than the  End Year"
       session[:new_model] = nil
       
        
     ## for the new action
      case params[:vendor].to_i
      when 1
         
        @vendors = ConsultantFullname.select("id,name").where("name ilike ? ","#{params[:name]}%")
         
        
      when 2
         @vendors = Ngo.select("id, name").where("name ilike ? ",params[:name]+'%')
   
      when 3
         @vendors = Supplier.select("id, name").where("name ilike ? ",params[:name]+'%')
   
      end
       
       
       @projects =  @accounts = Project.select("id,name").includes(:fund) 
      
      render :new
       
      
     return false
      end
    else
      
      flash[:error] = "Please fill in all required fields"
      ## for the new action
      case params[:vendor].to_i
      when 1
         
        @vendors = ConsultantFullname.select("id,name").where("name ilike ? ","#{params[:name]}%")
         
        
      when 2
         @vendors = Ngo.select("id, name").where("name ilike ? ",params[:name]+'%')
   
      when 3
         @vendors = Supplier.select("id, name").where("name ilike ? ",params[:name]+'%')
   
      end
       
       
       @projects =  @accounts = Project.select("id,name").includes(:fund) 
      
      render :new
       
      
     return false
      
      end
      
   ## ending the new action 
   contract = UnContract.check_available_fund(params[:un_contract])
   if  contract < 0
      if @un_contract.save
          session[:new_model] = nil
        flash[:error] = "Successfully saved new contract but this Project Account does not have sufficient fund, An dditional $#{contract * (-1)} is required "
        
       else
         flash[:error] = "Unable to save"
         
         session[:new_model] = @un_contract
         ## for the new action
      case params[:vendor].to_i
      when 1
         
        @vendors = ConsultantFullname.select("id,name").where("name ilike ? ","#{params[:name]}%")
         
        
      when 2
         @vendors = Ngo.select("id, name").where("name ilike ? ",params[:name]+'%')
   
      when 3
         @vendors = Supplier.select("id, name").where("name ilike ? ",params[:name]+'%')
   
      end
       
       
       @projects =  @accounts = Project.select("id,name").includes(:fund) 
      
      render :new
       
      
     return false
      end
      
      else
         if @un_contract.save
          session[:new_model] = nil
        flash[:notice] = "Successfully saved new contract"
        
       else
         flash[:error] = "Unable to save"
         
         session[:new_model] = @un_contract
         ## for the new action
      case params[:vendor].to_i
      when 1
         
        @vendors = ConsultantFullname.select("id,name").where("name ilike ? ","#{params[:name]}%")
         
        
      when 2
         @vendors = Ngo.select("id, name").where("name ilike ? ",params[:name]+'%')
   
      when 3
         @vendors = Supplier.select("id, name").where("name ilike ? ",params[:name]+'%')
   
      end
       
       
       @projects =  @accounts = Project.select("id,name").includes(:fund) 
      
      render :new
       
      
     return false
      end
   end
     
     redirect_to un_contracts_url
    return false
    end
    
  end

  def edit
      @projects =  @accounts = Project.select("id,name").includes(:fund)
  
    @un_contract = @un_account  = UnContract.find_by_id(params[:id])
    @account = Fund.find_by_id(@un_account.fund_id)
    if @un_account.blank?
      flash[:error] = "No Entry found"
      redirect_to un_contracts_url
      return false
    end
    
     @vessel = UnContract.get_awarded_id(@un_contract.organization_awarded)
      @vendors= eval(@vessel[1]).find(:all,:conditions=>{:id =>@vessel[0]}) 
       
       
  end

  def update
    
    if request.put?
      @un_contract = UnContract.find_by_id(params[:id])
       
      if !params[:un_contract][:start_date].blank? && !params[:un_contract][:end_date].blank? 
      params[:un_contract][:duration] = Date.parse(params[:un_contract][:end_date]) - Date.parse(params[:un_contract][:start_date])
      
      if params[:un_contract][:duration] < 0
        flash[:error] = "The Contract  Start Year cannot   be LATER than the  End Year"
       session[:new_model] = nil
       
        
     ## for the edit action
        @vessel = UnContract.get_awarded_id(@un_contract.organization_awarded)
      @vendors= eval(@vessel[1]).find(:all,:conditions=>{:id =>@vessel[0]}) 
    
       
       @projects =  @accounts = Project.select("id,name").includes(:fund) 
      
      render :edit
       
      
     return false
      end
    else
      flash[:error] = "Blank contract dates are not allowed!"
      
      @vessel = UnContract.get_awarded_id(@un_contract.organization_awarded)
      @vendors= eval(@vessel[1]).find(:all,:conditions=>{:id =>@vessel[0]}) 
    
       
       
      render :edit
       
       
      
     return false
      
      end
      
   ## ending the new action 
    contract = UnContract.check_available_fund(params[:un_contract])
    if contract < 0
      if @un_contract.update_attributes(params[:un_contract])
          session[:new_model] = nil
         flash[:error] = "Successfully saved new contract but this Project Account does not have sufficient fund, An dditional $#{contract * (-1)} is required "
       
       else
         flash[:error] = "Unable to update"
           @vessel = UnContract.get_awarded_id(@un_contract.organization_awarded)
      @vendors= eval(@vessel[1]).find(:all,:conditions=>{:id =>@vessel[0]}) 
    
         session[:new_model] = @un_contract
          
       
       @projects =  @accounts = Project.select("id,name").includes(:fund) 
      
      render :edit
       
      
     return false
      end
      else
        if @un_contract.update_attributes(params[:un_contract])
          session[:new_model] = nil
        flash[:notice] = "Successfully updated"
        
       else
         flash[:error] = "Unable to update"
           @vessel = UnContract.get_awarded_id(@un_contract.organization_awarded)
      @vendors= eval(@vessel[1]).find(:all,:conditions=>{:id =>@vessel[0]}) 
    
         session[:new_model] = @un_contract
          
       
       @projects =  @accounts = Project.select("id,name").includes(:fund) 
      
      render :edit
       
      
     return false
      end
    end
    end
    redirect_to un_contracts_url
    return false
  end

  def index
    session[:new_model] = nil
    @un_contracts = @funds = UnContract.select("*").includes(:project).paginate(:page=>params[:page],:per_page => 50)
  
  if params[:agency_department_id]
       @un_contracts = @funds = UnContract.select("*").where(:agency_department_id => params[:agency_department_id]).includes(:project).paginate(:page=>params[:page],:per_page => 50)
 
  end

  if params[:organization_awarded]
       @un_contracts = @funds = UnContract.select("*").where(:organization_awarded =>params[:organization_awarded]).includes(:project).paginate(:page=>params[:page],:per_page => 50)
 
  end  
   
  end
 
 def reports_for_lta
   qs = params[:qs]
   @qs = qs
   
   if qs == "lta_total_vendor"
    
     @un_contracts = @funds = UnContract.select("SUM(contract_value_in_usd) as contract_value_in_usd ,organization_awarded").group("organization_awarded").paginate(:page=>params[:page],:per_page => params[:per_page])

  end
  
  if qs == "lta_total_agency"
   @un_contracts = @funds = UnContract.select("SUM(contract_value_in_usd) as contract_value_in_usd ,agency_department_id").includes(:agency_department).group("agency_department_id").paginate(:page=>params[:page],:per_page => params[:per_page])
 
 end
 
 if qs == "valid"
   
   @un_contracts = @funds = UnContract.select("*").where("CAST(end_date as date) between ? and ?","#{Time.now.year}-01-01","#{Time.now.year}-12-31").includes(:agency_department).paginate(:page=>params[:page],:per_page => params[:per_page])
 
 end
 
 if qs == "invalid"
   @un_contracts = @funds = UnContract.select("*").where("CAST(end_date as date) < ? ","#{Time.now.year}-01-01").includes(:agency_department).paginate(:page=>params[:page],:per_page => params[:per_page])
  
 end
 if qs == "-1"
   flash[:error] = "No Query Selected for search!!"
   redirect_to new_report_url
   return false
 end
 end

  def show
     @un_contract = @un_account  = UnContract.find_by_id(params[:id])
    @account = Fund.find_by_id(@un_account.fund_id)
 
  end

  def destroy
    @un_contract = UnContract.find_by_id(params[:id])
    if @un_contract.destroy
      flash[:notice] = "Successfully deleted"
      
    else
      flash[:error] = "Unable to delete"
    end
     redirect_to un_contracts_url
    return false
  end
end
