class SysModulesController < ApplicationController
  
  before_filter :login_required , :only => ['index','new','create','update','edit','destroy','show']
  before_filter :staff_only 
  #before_filter :check_consultant  ,:except => ['create','add','remove']
  layout :get_user_layout

  def new
    @sys_module = @fund = SysModule.new
  end

  def create
    @sys_module  = SysModule.new params[:sys_module]
    
    if @sys_module.save
      flash[:notice] = "Saved Module!"
      
    else
      flash[:error]= "Failed to create"
      
    end
    
    redirect_to sys_modules_url
    return false
  end

  def index
    @sys_modules = @funds = SysModule.paginate(:page =>params[:page], :per_page => params[:per_page])
  end

  def edit
     @sys_module  = SysModule.find_by_id(params[:id])
   
  end

  def destroy
    
     @sys_module  = SysModule.find_by_id(params[:id])
      if @sys_module.destroy
        flash[:notice]  = "Module deleted"
      end
      redirect_to sys_modules_url
    return false
  end

  def show
    redirect_to sys_modules_url
    return false
  end

  def update
      @sys_module  = SysModule.find_by_id(params[:id])
      if @sys_module.update_attributes!(params[:sys_module])
        flash[:notice]  = "Module updated"
      end
      redirect_to sys_modules_url
    return false
  end
end
