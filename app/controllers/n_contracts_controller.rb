class NContractsController < ApplicationController
  
  before_filter :login_required, :only=>['destroy','edit','create','index','show','update','destroy']
   before_filter :ngos_only
  
  layout :get_user_layout 
  
  def new
  end

  def edit
  end

  

  def show
  end

  def index
  end
 
 def update
    if request.put?
      @nfinancial = NContract.find_by_id(params[:id])
      if @nfinancial.update_attributes(params[:n_contract])
        flash[:notice] = "successfully updated contract details"
      else
        flash[:error] = "unable to update contract details"
      end
      
       
    
   respond_to do |format|
     if identify_vendor.nil?
        if request.env["HTTP_REFERER"].nil?
                verify_user_section
                return false
                else
                 redirect_to request.env["HTTP_REFERER"]
                 return false
                 end
      else
        
      format.html {redirect_to new_ngo_url :id=> params[:n_contract][:ngo_id] 
                  return false
                  }
     end
   end
    end   
  end

  def create
    if request.post? 
      @nfinancial = NContract.new(params[:n_contract])
      if @nfinancial.save
        session[:new_model] = nil
        flash[:notice] = "successfully saved contract details"
        
      else
        flash[:error] = "unable to save contract details"
        session[:new_model] = @nfinancial
          redirect_to new_ngo_url :id=> params[:n_contract][:ngo_id]  
          return false
        
      end
      
       
    respond_to do |format|
      if identify_vendor.nil?
        
      else
        
      format.html {redirect_to new_ngo_url :id => params[:n_contract][:ngo_id] 
                  return false
                  }
     end
   end
    end
   return false 
  end


  def destroy
    
    @nfinancial = NContract.find_by_id(params[:id])
      if @nfinancial.destroy
        flash[:notice] = "successfully deleted contract details"
      else
        flash[:error] = "unable to delete contract details"
      end
      
       respond_to do |format|
      if identify_vendor.nil?
        
      else
        
      format.html {redirect_to new_ngo_url :id => params[:n_contract][:ngo_id] 
                  return false
                  }
     end
   end
  end
end
