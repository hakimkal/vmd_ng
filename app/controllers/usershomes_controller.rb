class UsershomesController < ApplicationController
  before_filter :login_required, :only=>['usershome']
  before_filter :consultants_only 
  
  layout :get_user_layout 
   
  def usershome
   
    if (Conservtype.find_by_undpuser_id(current_user.id).blank? and current_user.group_id == 4 and current_user.vendor_id == 1)
      flash[:notice] = 'Please select your professional area'
      redirect_to new_conservtype_url
      
    elsif current_user.group_id != 4
      flash[:error] = 'you are attempting to access a consultants only section, you have been redirected back your section'
      verify_user_section
    
    end
  end
end
