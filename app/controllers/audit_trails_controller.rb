class AuditTrailsController < ApplicationController
  
  before_filter :login_required, :only=>['destroy','edit','change_password','index','show','update','vendor_select']
  #before_filter :check_user_group , :only=>[] 
  before_filter :staff_only 
  layout :get_user_layout
 
  def index
   @activities =@audit_trails= PublicActivity::Activity.select("*").where("owner_id is not null").order("created_at desc").paginate(:per_page =>40 ,:page => params[:page])
  end

  def show
  end

  def destroy
  end
end
