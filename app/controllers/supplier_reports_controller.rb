class SupplierReportsController < ApplicationController
  before_filter :login_required , :only => ['index','new','create','update','edit','destroy','show']
  before_filter :staff_only 
  #before_filter :check_consultant  ,:except => ['create','add','remove']
  layout :get_user_layout

  def index
      @supplier_report = SupplierReport.new
    if request.post?
      s = params[:supplier_report]
      
      if s[:vpa_name] == "" && s[:vss_name].size == 1 && s[:vss_name][0] == '-1' && s[:others] == '-1' && !s[:service]
         @supplier_reports = SupplierReport.select("Distinct on (name) *").paginate(:page=>params[:page],:per_page => 50)
         return false
         
         elsif s[:vpa_name] != "" && (!s[:vss_name] || s[:vss_name].blank?) && s[:others] == '-1' && !s[:service]
           @supplier_reports = SupplierReport.select("Distinct on (name) *").where("vpa_id = ? ",s[:vpa_name]).paginate(:page=>params[:page],:per_page => 50)
        return false
   

         elsif s[:vpa_name] != "" && (!s[:vss_name] || s[:vss_name].blank?) && s[:others] == '-1' && !s[:service].blank?
           @supplier_reports = SupplierReport.select("Distinct on (name) *").where("vpa_id = ?  and service in(?) ",s[:vpa_name],s[:service]).paginate(:page=>params[:page],:per_page => 50)
        return false
   
    
    elsif s[:vpa_name] != "" && (!s[:vss_name] || s[:vss_name].blank?) && s[:others] != '-1' && !s[:service]
           @supplier_reports = SupplierReport.select("Distinct on (name) *").where("vpa_id = ?  and ? is not null",s[:vpa_name],s[:others].to_s).paginate(:page=>params[:page],:per_page => 50)
        return false
   
    elsif s[:vpa_name] != "" && s[:vss_name].size == 1 && (s[:vss_name][0] == '-1' || s[:vss_name][0].blank?) && s[:others] != '-1' && !s[:service]
         @supplier_reports = SupplierReport.select("Distinct on (name) *").where("vpa_id = ? and vss_id in(?)  and ?  is not null",s[:vpa_name],s[:others].to_s).paginate(:page=>params[:page],:per_page => 50)
         return false
 
 elsif s[:vpa_name] == "" && (!s[:vss_name] || (s[:vss_name].size == 1 && (s[:vss_name][0] == '-1' || s[:vss_name][0].blank?))) && s[:others] != '-1' && !s[:service].blank?
         @supplier_reports = SupplierReport.select("Distinct on (name) *").where("?  is not null and service in (?)",s[:others].to_s,s[:service]).paginate(:page=>params[:page],:per_page => 50)
         return false
 
 
   elsif s[:vpa_name] != "" && s[:vss_name].size >= 1 && s[:vss_name][0] != '-1' && s[:others] != '-1' && !s[:service]
     
            @supplier_reports = SupplierReport.select("Distinct on (name) *").where("vpa_id = ?  and vss_id in(?) and ?  is not null",s[:vpa_name],s[:vss_name],s[:others].to_s).paginate(:page=>params[:page],:per_page => 50)
       return false
   
    
      elsif s[:vpa_name] == "" && s[:vss_name].size == 1 && s[:vss_name][0] == '-1' && s[:others] == '-1' && !s[:service].blank?
         @supplier_reports = SupplierReport.select("Distinct on (name) *").where("service in (?)",s[:service]).paginate(:page=>params[:page],:per_page => 50)
         return false
   
   
     elsif s[:vpa_name] != "" && !s[:vss_name] && s[:others] == '-1' && !s[:service]
         @supplier_reports = SupplierReport.select("Distinct on (name) *").where("vpa_id = ?",s[:vpa_name]).paginate(:page=>params[:page],:per_page => 50)
         return false
   
   elsif s[:vpa_name] != "" && s[:vss_name].size == 1 && (s[:vss_name][0] == '-1' || s[:vss_name][0].blank?) && s[:others] == '-1' && !s[:service]
         @supplier_reports = SupplierReport.select("Distinct on (name) *").where("vpa_id = ?",s[:vpa_name]).paginate(:page=>params[:page],:per_page => 50)
         return false
   
    elsif s[:vpa_name] != "" && s[:vss_name].size >= 1  && s[:others] == '-1' && !s[:service]
         @supplier_reports = SupplierReport.select("Distinct on (name) *").where("vpa_id = ? and vss_id in (?)",s[:vpa_name],s[:vss_name]).paginate(:page=>params[:page],:per_page => 50)
         return false
   
   elsif s[:vpa_name] == "" && s[:vss_name].size == 1 && (s[:vss_name][0] == '-1' || s[:vss_name][0].blank?) && s[:others] != '-1' && !s[:service]
         @supplier_reports = SupplierReport.select("Distinct on (name) *").where("? is not null",s[:others]).paginate(:page=>params[:page],:per_page => 50)
         return false
         
   
   elsif s[:vpa_name] != "" && s[:vss_name].size >= 1 && (s[:vss_name][0] == '-1' || !s[:vss_name][0].blank?) && s[:others] != '-1' && !s[:service].blank?
         @supplier_reports = SupplierReport.select("Distinct on (name) *").where("vpa_id = ? and vss_id in(?) and service in(?) and ?  is not null",s[:vpa_name],s[:vss_name],s[:service],s[:others].to_s).paginate(:page=>params[:page],:per_page => 50)
         return false
  else
      
   @supplier_reports = SupplierReport.select("Distinct on (name) *").paginate(:page=>params[:page],:per_page => 50)
         return false
    
     
      end
      
      
    return false
    end
    
    if request.get?
  
    @supplier_reports = SupplierReport.select("Distinct on (name) *").paginate(:page=>params[:page],:per_page => 50)
    return false
    end
   
  end

  def show
  end
end
