class SupplieruserController < ApplicationController
  
  before_filter :login_required, :only=>['destroy','edit','supplieruser','index','show','update']
  before_filter :suppliers_only
  layout :get_user_layout 
    
  def index
  end

  def supplieruser
    
    if (Supplier.find_by_undpuser_id(current_user.id).nil? && current_user.group_id == 4 && current_user.vendor_id == 3)
       @supplier = Supplier.create({:undpuser_id => current_user.id})
         session[:sid]= @supplier.id
    elsif current_user.group_id != 4
    
     flash[:error] = 'you are attempting to access an NGOs only section, you have been redirected back your section'
     verify_user_section
    
    else
       @supplier = Supplier.find_by_undpuser_id(current_user.id)
     session[:sid]= @supplier.id
      @supplier_id = @supplier.id
      
   
    
    end
  end
end
