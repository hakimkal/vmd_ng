class NFinancialsController < ApplicationController
  
 before_filter :login_required, :only=>['destroy','edit','create','index','show','update','destroy']
   before_filter :ngos_only
  
  layout :get_user_layout 
  
  def new
  end

  def edit
  end

  def update
    if request.put?
      @nfinancial = NFinancial.find_by_id(params[:id])
      @nfinancial.update_attributes(params[:n_financial])
      
      @nbank = NBankInfo.find_by_ngo_id(params[:n_bank_info][:ngo_id])
      if !@nbank
        @nbank = NBankInfo.new(params[:n_bank_info])
        if @nbank.save 
          flash[:notice] = "sucessfully save financials."
          session[:new_model] = nil
        else
          flash[:error]  = "unable to save"
          session[:new_model] = @nbank
          redirect_to new_ngo_url(:id => params[:n_bank_info][:ngo_id])
          return false
        end
    else
      @nbank.update_attributes(params[:n_bank_info])
      flash[:notice] = "successfully updated financials."   
    end
    
    respond_to do |format|
      format.html {
      if identify_vendor.nil?
        redirect_to :back
        return
       
    
      else
        
     redirect_to new_ngo_url :id=> params[:n_financial][:ngo_id] 
                  return false
                end 
                  }
     
   end
    end   
  end

  def create
    if request.post? 
      @nfinancial = NFinancial.new(params[:n_financial])
      if @nfinancial.save
        session[:new_model] = nil
        flash[:notice] = "successfully saved financial details"
        
      else
        flash[:error] = "unable to save financial details"
        session[:new_model] = @nfinancial
          redirect_to new_ngo_url :id=> params[:n_financial][:ngo_id]  
          return false
        
      end
      
      if params[:n_bank_info]
        @nbank = NBankInfo.new(params[:n_bank_info])
        if @nbank.save
           session[:new_model] = nil
          flash[:notice] = "successfully saved financial and  bank details"
        else
          flash[:notice] = nil
          flash[:error] = "unable to save financial and bank details"
          session[:new_model] = @nbank
          redirect_to new_ngo_url(:id=> params[:n_financial][:ngo_id])
          return false 
          
        end
      end
    respond_to do |format|
      if identify_vendor.nil?
        
      else
        
      format.html {redirect_to new_ngo_url :id=> params[:n_financial][:ngo_id] 
                  return false
                  }
     end
   end
    end
   return false 
  end

  def show
  end

  def destroy
  end

  def index
  end
end
