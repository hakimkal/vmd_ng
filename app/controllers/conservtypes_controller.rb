class ConservtypesController < ApplicationController
  
  before_filter :login_required, :only=>['usershome','new']
  before_filter :consultants_only 
  layout :get_user_layout 
   
  def index
  end

  def new
    @conservices =  Conservice.find(:all,:order=>["id desc"])
     if current_user.group_id == 4 and current_user.vendor_id == 1 
      consultant = Consultant.find(:first,:conditions => {:undpuser_id=>current_user.id})
      if consultant.nil?
         consultant = Consultant.create({:undpuser_id => current_user.id})
         session[:cid]= consultant.id
      else
      @consultant_id = consultant.id
      end
      @vendor_type = VendorType.find(:first , :conditions =>{:id => current_user.vendor_id})  
      
      @vpa_id = Vpa.find(:first , :conditions =>{:id => current_user.vpa_id})  
      @vss_id = Vss.find(:first , :conditions =>{:id => current_user.vss_id})  
      @undpuser_id = current_user.id
      @conservtype = Conservtype.new
    
    else
     user = Undpuser.find_by_id(params[:undpuser_id])
     if !user.nil? and user.group_id == 4 && user.vendor_id == 1
      @vendor_type = VendorType.find(:first , :conditions =>{:id =>user.vendor_id})  
      @vpa_id = Vpa.find(:first , :conditions =>{:id => user.vpa_id})  
      @vss_id = Vss.find(:first , :conditions =>{:id => user.vss_id})  
      @undpuser_id = user.id
      consultant = Consultant.find(:first,:conditions => {:undpuser_id=>user.id})
      if consultant.nil?
         consultant = Consultant.create({:undpuser_id => user.id})
      else
      @consultant_id = consultant.id
      end
      @consultant_id = consultant.id
      @conservtype = Conservtype.new
     else
       flash[:error] = "Invalid User selected, User is not a consultant" 
       redirect_to request.env['HTTP_REFERER'] unless request.env['HTTP_REFERER'].nil?
       verify_user_section
     end
    end
    
  end

  def create
    if request.post?
      selected_conservtypes = params[:conservtype][:conservice_id]
    if selected_conservtypes.nil? || selected_conservtypes.blank?
      verify_user_section
    end  
   selections =[]
   selected_conservtypes.each{|sc| selections << {:undpuser_id => params[:conservtype][:undpuser_id],:conservice_id => sc,:consultant_id=>params[:conservtype][:consultant_id]} unless sc.empty?}
   selections.delete_if {|scn| Conservtype.count(:conditions => scn) > 0}
    
    if selections.size <= 0
      flash[:error] = 'You have already selected all the submitted services'
      verify_user_section
    elsif Conservtype.create(selections)
      flash[:notice] = "Successfully selected Service(s)"
      verify_user_section
    else
      flash[:error] = 'Unable to add your selections '
      verify_user_section
      
    end
    end
    
  end

  def destroy
  end

  def edit
  end

  def show
  end

  def update
  end
end
