class ProjectsController < ApplicationController
  
   before_filter :login_required , :only => ['index','new','create','update','edit','destroy','show']
  before_filter :staff_only 
  #before_filter :check_consultant  ,:except => ['create','add','remove']
  layout :get_user_layout
  
  def new
    @project = Project.new
     @accounts = Fund.select("id,account_number,operating_unit,approving_officer")
   
  end

  def create
     @account =  Project.new params[:project]
     @accounts = Fund.select("id,account_number")
   
     @project = @account
    if @account.save
      session[:new_model] = nil
      flash[:notice] = "Successfully added a new project #{@account.name}!"
      
    else
      flash[:error] = "Unable to create project!"
      session[:new_model] = @account
      render :new
      return
    end
    redirect_to projects_url
    return false
    
  end

  def edit
   @project =  @account = Project.find_by_id(params[:id])
    @accounts = Fund.select("id,account_number,operating_unit,approving_officer")
   
  end

  def update
    @account = Project.find_by_id(params[:id])
    @v = params[:project]
    
    if @account.update_attributes(@v)
      session[:new_model] = nil
      flash[:notice] = "Successfully updated project #  #{@account.name}!"
    else
      flash[:error] = "Unable to update project!"
      sesion[:new_model] = @account
      render :edit
      return false
    end
    redirect_to projects_url
    return false
  end

  def index
    session[:new_model] = nil
    @funds = @projects = Project.select("*").includes(:fund,:agency_department).paginate(:page=>params[:page],:per_page =>params[:per_page])
  end

  def show
    redirect_to projects_url
    return false
  end

  def destroy
  end
end
