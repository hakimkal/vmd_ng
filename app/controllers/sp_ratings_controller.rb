class SpRatingsController < ApplicationController
   before_filter :login_required , :only => ['index','new','select_cons','create','update','edit','destroy','show']
  before_filter :staff_only 
  #before_filter :check_consultant  ,:except => ['create','add','remove']
  layout :get_user_layout

  def index
    
     if params[:name].blank?
    @ratings = @sp_ratings= SpRating.paginate(:per_page=>50 , :page=>params[:page])
    
    else
    @ratings = @sp_ratings= SpRating.where("supplier_name ilike ?",params[:name]+"%").paginate(:per_page=>50 , :page=>params[:page])
     
    end
  end

  def edit
  end

  def new
  end

  def create
  end

  def show
    @rating =  SpRating.find_by_id(params[:id])
  end

  def destroy
  end

  def update
  end
end
