class NRatingsController < ApplicationController
  
   before_filter :login_required , :only => ['index','new','select_ngos','create','update','edit','destroy','show']
   before_filter :staff_only 
  #before_filter :check_consultant  ,:except => ['create','add','remove']
  layout :get_user_layout
  
  def select_ngos
    if params[:cancel] == "1"
      session[:new_model] = nil
    end
    @n_rating = NRating.new
  end
  
  def new
    if params[:cancel] == "1"
      session[:new_model] = nil
    end
     if request.post?
       session[:new_model] = nil
      if !params[:n_rating][:name].blank?
      @ngos = Ngo.where("name ilike '#{params[:n_rating][:name]}%'")
       end
     if @ngos.blank?
       flash[:error] = "No entry found"
       redirect_to :back
       return false    
    end
    end
    @n_rating = NRating.new
    
    
  end

  def create
    
    @n_rating = NRating.new params[:n_rating]
   c  = Ngo.find_by_id(@n_rating.ngo_id) if  !@n_rating.ngo_id.blank?
   @n_rating.ngo_name = "#{c.name}" if !c.blank?
   if @n_rating.save
      session[:new_model] = nil
     flash[:notice] = " Sucessfully saved entry"
     redirect_to select_ngos_n_ratings_url
     return false 
     
   else
     flash[:error] = "Unable to save entry"
     session[:new_model] = @n_rating
    if @n_rating.ngo_id.blank?
       redirect_to select_ngos_n_ratings_url
       return false
     end
     @ngos = Ngo.find(:all,:conditions=>{:id=>c.id})
     
     render :new
   end
  end

  def edit
    
     @n_rating = NRating.find(params[:id])
     #@consultants = Consultant.find(@c_rating.consultant_id)
     @ngos = Ngo.where("id in(?)",@n_rating.ngo_id)
    
   
 
   
  end

  def update
    
    @reference = NRating.find(:first,:conditions=>{:id => params[:id]})
      #debugger
      if @reference && @reference.update_attributes(params[:n_rating])
        flash[:notice] = "Updated rating successfully"
       else
         flash[:error] = "Unable to update rating, try again later.<br/> **Blank fields are unacceptable!".html_safe
         session[:model] = nil
      end
      
      redirect_to ng_rating_url(@reference) and return
  end


  def index
  end

  def show
  end

  def destroy
    if @reference = NRating.find(:first,:conditions=>{:id => params[:id]}).destroy()
      flash[:notice] = "Successfully deleted rating"
    else
      flash[:error] = "unable to delete rating"
    end
     respond_to do |format|
        format.html { 
                       verify_user_section
                        return false
                         
                     }
                 end
  end
end
