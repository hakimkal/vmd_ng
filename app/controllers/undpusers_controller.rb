class UndpusersController < ApplicationController
  
  before_filter :login_required, :only=>['destroy','edit','change_password','index','show','update','vendor_select']
  #before_filter :check_user_group , :only=>[] 
   before_filter :staff_only , :only => ['index','create_vendor']
  before_filter :logout_required , :only => ['confirmation','forgot_password' ,'signup_change_password', 'request_confirmation_link']  
  layout :get_user_layout
  
  
  def forgot_password
    if request.post?
      
      @user = Undpuser.find(:first,:conditions => {"email"=>params[:undpuser][:email]})
      if !@user.nil?
      
      @user.uni_id = Digest::SHA2.hexdigest(Time.now.utc.to_s)
      if @user.save(:validate=>false)  && UndpuserMailer.forgot_password(@user).deliver
        flash[:notice] = 'Password reset link has been sent to you'
        redirect_to root_url
        return false
      else
        flash[:error]= "unable to send forgotten password link. please try again."
        redirect_to forgot_password_undpusers_url
        return false
      end
      else
        flash[:error]= "Invalid email  or usernmame provided"
        redirect_to forgot_password_undpusers_url
        return false
      end   
    
    end
  end
  
  def create_vendor
    if request.params[:undpuser_id].nil?
    @user = @undpuser = @model = params[:undpuser] = Undpuser.new
    @vendor = VendorType.select("id,name")
    @remove_password_fields = 0
   else
   @user = @undpuser = @model = params[:undpuser] = Undpuser.find(params[:undpuser_id])
   @vendor = VendorType.select("id,name")
   @remove_password_fields = 1
  
   end
  end
  
  
  def create_staff
    if params[:id].nil?
    @user = @undpuser = params[:undpuser] = @model = Undpuser.new
    @vendor = [{:name=>"admin",:id=>1},{:name=>"manager",:id=>2},{:name=>"staff",:id=>3}]
  else
    
   @user = @undpuser = params[:undpuser] = @model = Undpuser.find(params[:id])
    @vendor = [{:name=>"admin",:id=>1},{:name=>"manager",:id=>2},{:name=>"staff",:id=>3}]
 
  end
 
  end
  
  def new
   # flash[:error] = "System Under Maintenance, Please come back on monday 04th February,2013!!"
      if signed_in? 
      flash[:error] = "You must logout to access that section"
      verify_user_section
      
      
    end
     @user = @model= Undpuser.new
    # redirect_to root_url
     #return false
   end
                        
  def create
      time = Time.now.to_s
      if params[:undpuser][:password].nil?
      params[:undpuser][:password] =  "aA9defoh"
      params[:undpuser][:password_confirmation] = "aA9defoh"
      params[:undpuser][:uni_id] = Digest::SHA2.hexdigest(time)
    end
    @user = @model= Undpuser.new(params[:undpuser])
    
    if @user.save
       session[:new_model] = nil
     @m = params[:undpuser]
     if signed_in?
       signedin_user = current_user
      else
        signedin_user = nil
     end
       if UndpuserMailer.signup(@m,signedin_user).deliver
          if signed_in?
          flash[:notice] = "A confirmation email has been sent to the provided email, please advice user to check his email. Note that, confirmation email may be in Spam/Bulk Mail!"
          else
            flash[:notice] = "A confirmation email has been sent to your email, please check and follow the link. Note that,Your confirmation email may be in your Spam/Bulk Mail!"
         
          end
          if signed_in?
            verify_user_section
            return false
          else
          redirect_to root_path
          return false
          end
        else
          flash[:error] = "A confirmation email was not sent to the email as it does not appear to be a valid email address"
          redirect_to root_path
          return false
       end
       
    else
       
      flash.now[:error]= "Signup was NOT sucessful!"
      session[:new_model] = @model = @user 
      if signed_in? && params[:undpuser][:usertype] == 'vendor'
        @user = @undpuser = @model = Undpuser.new
       @vendor = VendorType.select("id,name")
        render :create_vendor
      elsif signed_in? && params[:undpuser][:usertype] == 'staff'
        @user = @undpuser = @model = Undpuser.new
       @vendor = [{:name=>"admin",:id=>1},{:name=>"manager",:id=>2},{:name=>"staff",:id=>3}]
 
        render :create_staff
      else
          render :new 
      end
    end
  end
  
  def edit 
    
     @user = @model= Undpuser.find_by_id(params[:id],:include=>[:vpa,:vss])
  end
  
  def destroy
    
    if current_user.group_id != 4   
      if params[:task] == 'suspend'
        @user = Undpuser.find(params[:id])
        @user.suspend = 1
        if @user.save
          flash[:notice] = "requested task was successful"
          redirect_to :back
          return
        else
          flash[:error] = "requested task was not completed, try again later"
          redirect_to :back
          return
        end
      elsif params[:task]== 'reactivate'
        @user = Undpuser.find(params[:id])
        @user.suspend = 0
        if @user.save
          flash[:notice] = "requested task was successful"
          redirect_to :back
          return
        else
          flash[:error] = "requested task was not completed, try again later"
          redirect_to :back
          return
        end
    
      elsif params[:task]== "delete"
        session[:return_to] = request.env["HTTP_REFERER"]
        @type = params[:type] 
        @user = @undpuser = Undpuser.find(params[:id])
     
     elsif params[:delete]== "1"
     
      @user = @undpuser = Undpuser.find(params[:id])
       if @user.destroy
          flash[:notice] = "requested operation  was successful"
          redirect_to  undpusers_url(:type=>params[:type])
          return false
       
       else
        flash[:error] = "requested operation was not completed, try again later"
        redirect_to session[:return_to] unless session[:return_to].nil?
        return false
       end
   
 
      end 
    
    else
      
     
      if params[:task]== "delete"
    session[:return_to] = request.env["HTTP_REFERER"]
    @type = params[:type] 
     @user = @undpuser = Undpuser.find(current_user.id)
     
     elsif params[:delete]== "1"
     
      @user = @undpuser = Undpuser.find(current_user.id)
      if @user.destroy
        flash[:notice] = "Your account has been permanently deleted!"
        redirect_to logout_url
         
        return false
       
      else
        flash[:error] = "requested operation was not completed, try again later"
      verify_user_section
    
    return false
    end
      
      end
    
    end
    
    respond_to do |f|
      f.html  {}
      f.js
      f.json  
    end
  end
  
  def index
    
   if params[:sort].nil?
   sort = "firstname"
   else
   sort = params[:sort]
   end
    if (params[:type].nil? && params[:supend].nil?)
   @users= @undpusers = @undpusers= Undpuser.where(:suspend => 0.to_s).paginate(:page=>params[:page],:per_page=>40).order(sort)
    
    elsif(!params[:type].nil? && params[:suspend].nil?)
     
      case params[:type]
      when 'vendors'
    @title = 'All Vendors'  
    @users =  @undpusers= Undpuser.where({:group_id => 4,:suspend => 0.to_s}).paginate(:page=>params[:page],:per_page=>40).order(sort)
  when 'staff'
    @title = 'All Staff'  
    @users =  @undpusers= Undpuser.where({:usertype =>'staff',:suspend =>  0.to_s}).paginate(:page=>params[:page],:per_page=>40).order(sort)
   when 'manager'
    @title = 'All Managers'  
    @users =  @undpusers= Undpuser.where({:usertype =>'staff',:group_id => 2,:suspend =>  0.to_s}).paginate(:page=>params[:page],:per_page=>40).order(sort)
   when 'consultants'
    @title = 'All Consultants'  
    @users =  @undpusers= Undpuser.where({:usertype =>'vendor',:vendor_id=>1,:suspend => 0.to_s}).paginate(:page=>params[:page],:per_page=>40).order(sort)
  
  when 'ngos'
    @title = 'All NGOs'  
    @users =  @undpusers= Undpuser.where({:usertype =>'vendor',:vendor_id=>2,:suspend =>  0.to_s}).paginate(:page=>params[:page],:per_page=>40).order(sort)
  
when 'suppliers'
    @title = 'All Suppliers'  
    @users =  @undpusers= Undpuser.where({:usertype =>'vendor',:vendor_id=>3,:suspend =>  0.to_s}).paginate(:page=>params[:page],:per_page=>40).order(sort)
  
      
      end
  elsif !params[:type].nil? && !params[:suspend].nil?
    
     case params[:type]
      when 'vendors'
    @title = 'All Vendors'  
    @users =  @undpusers= Undpuser.where({:group_id => 4,:suspend => 1.to_s}).paginate(:page=>params[:page],:per_page=>40).order(sort)
  when 'staff'
    @title = 'All Staff'  
    @users =  @undpusers= Undpuser.where({:usertype =>'staff',:suspend => 1.to_s}).paginate(:page=>params[:page],:per_page=>40).order(sort)
   when 'manager'
    @title = 'All Managers'  
    @users =  @undpusers= Undpuser.where({:usertype =>'staff',:group_id => 2,:suspend => 1.to_s}).paginate(:page=>params[:page],:per_page=>40).order(sort)
   when 'consultants'
    @title = 'All Consultants'  
    @users =  @undpusers= Undpuser.where({:usertype =>'vendor',:vendor_id=>1,:suspend => 1.to_s}).paginate(:page=>params[:page],:per_page=>40).order(sort)
  
  when 'ngos'
    @title = 'All NGOs'  
    @users =  @undpusers= Undpuser.where({:usertype =>'vendor',:vendor_id=>2,:suspend => 1.to_s}).paginate(:page=>params[:page],:per_page=>40).order(sort)
  
when 'suppliers'
    @title = 'All Suppliers'  
    @users =  @undpusers= Undpuser.where({:usertype =>'vendor',:vendor_id=>3,:suspend => 1.to_s}).paginate(:page=>params[:page],:per_page=>40).order(sort)
  
      
      end
    end
    
     if params[:field] != "" && params[:searchString] !=""
       
    case params[:field]
    when "firstname"
      @title = "Showing search  result (s)"
    @users= @undpusers = Undpuser.where(' suspend = ? and firstname ilike ?',0.to_s,"#{params[:searchString]}%").paginate(:page=>params[:page],:per_page=>40).order(:firstname)
    return
     when "lastname"
       @title = "Showing search  result (s)"
    @users= @undpusers = Undpuser.where('suspend = ? and lastname ilike ?',0.to_s,"#{params[:searchString]}%").paginate(:page=>params[:page],:per_page=>40).order(:firstname)
    return
     when "username"
       @title = "Showing search  result (s)"
    @users= @undpusers = Undpuser.where('suspend = ? and username ilike ?',0.to_s,"#{params[:searchString]}%").paginate(:page=>params[:page],:per_page=>40).order(:firstname)
    return
    end
    else
      flash[:error] = "No query given for search."
        @users= @undpusers=  Undpuser.where(:suspend=>0).paginate(:page=>params[:page],:per_page=>40).order(:firstname)
    return
    end
    
  end
  
  def change_password
    session[:new_model] = nil
     @user = @model= Undpuser.find_by_id(params[:id])
     if request.put? && Undpuser.find(params[:id])
       @user.id = current_user.id
       @user.password = params[:undpuser][:password]
       @user.password_confirmation = params[:undpuser][:password_confirmation]
       if @user.save(:validate=>true)
         session[:new_model] =nil
         flash[:notice] = "You have successfully changed your password!"
        verify_user_section
       else
         flash[:error] = "Unable to change your password, Please try again later!"
         session[:new_model] = @user
         
       end
     end
  end
  def signup_change_password
     
     if request.put? 
       @user = Undpuser.find(params[:undpuser][:id])
      if !@user.nil?
       @user.password = params[:undpuser][:password]
       @user.uni_id = nil
       @user.password_confirmation = params[:undpuser][:password_confirmation]
       if @user.save
         flash[:notice] = "password successfully changed!"
         session[:new_model] = nil
          login_audit @user
          sign_in @user
          verify_user_section
          return
       else
         flash[:error] = "unable to update password!"
         session[:new_model] = @user
         redirect_to :back
         return
       end
      end
     
     else
       flash[:error] = "Please request for a new activation link"
       redirect_to request_confirmation_link_undpusers_url
       
     end
  end
  
  def edit_profile
    if current_user.id != params[:id]
    @user = @model= Undpuser.find_by_id(params[:id])  
   
   else
     flash[:error] = "Invalid user"
       
     redirect_to request.env["HTTP_REFERER"]
  end
  end
  
  def show 
    @Undpuser = Undpuser.find_by_id(params[:id])
  end
  
 def update
   
   @user = Undpuser.find_by_id(params[:id])
   
    request.raw_post
    if @user.update_attributes!(params[:undpuser])
     flash[:notice] = "#{params[:undpuser][:firstname]}  #{params[:undpuser][:lastname]}  has been updated successfully!"
      if request.env["HTTP_REFERER"].nil?
       verify_user_section
       return false
       else
         redirect_to undpusers_url
         return
       end
    else
     @model=@user
     #session[:new_model] = @user.errors
     session[:model]  = nil
      flash[:error] = "unable to edit the user "
      if params[:undpuser][:redirect_to].nil? 
        render :edit_profile
      else
        m = ""
        @user.errors.full_messages.each {|e| m += e + "\," }
         flash[:error] = m
     
       redirect_to edit_profile_undpuser_path current_user
      end
    
   
   end
 end
 
 def confirmation
   
   @user = Undpuser.find_by_uni_id(params[:uni_id])
   
   if @user.nil? 
     st = 'You do not appear to have a genuine Activation Link.Did you receive a link in your email? Please Signup here</a> <br/>
            or Request for an activation Link :<a href="'+ request_confirmation_link_undpusers_path+'">Activation Link</a>'
   flash[:error]  = st.html_safe
   redirect_to signup_path
    
   else
      
   
   end
   
   
 end
 
 def report
   #Just a static view
   
 end
 def vendor_select
   layout = :application
   if current_user.group_id !=4 
     flash[:warning] =  'the requested section is restricted to  [vendors] only , kindly sign in as a vendor to access that section!'
     verify_user_section
   end
   if request.get?
   @vendor = VendorType.select("id, name") 
   #flash[:notice] = 'Welcome back! Please make your selections according to your specialty'
   end
   
   
   if request.put?
     @vendor_id = params[:undpuser][:vendor_id]
     @vpa_id = params[:undpuser][:vpa_id]
     @vss_id = params[:undpuser][:vss_id]
     if(@vendor_id.nil? or @vpa_id.nil? or @vss_id.nil?)
       flash[:error] = 'You must select all three categories that best describes you'
       redirect_to vendor_select_undpuser_url current_user
     else
        
      params[:undpuser][:vendor_id] = @vendor_id
       params[:undpuser][:vpa_id] = @vpa_id
       params[:undpuser][:vss_id] = @vss_id
       user = Undpuser.find_by_id(current_user.id) 
       
       if user.update_attributes(params[:undpuser])
         flash[:notice] = 'You have made your selections successfully'
         current_user.vendor_id = @vendor_id
         current_user.vpa_id = @vpa_id
         current_user.vss_id = @vss_id
         
         verify_user_section
      end
     end
   end
 end
 
 def request_confirmation_link
   flash[:notice] = "We would Confirm your record and send the link to the specified email." if request.get?
   
   if request.post?
     @user = @model = Undpuser.find_by_email(params[:undpuser][:email])
     if @user.nil?
       flash[:error] = 'No record found for specified email.'
       render :request_confirmation_link
     else
      
       @user.uni_id = Digest::SHA2.hexdigest(Time.now.utc.to_s)
       if @user.save(:validate=>false) && UndpuserMailer.send_activation_link(@user).deliver
        
        flash[:notice] = "Activation link sent"
       else
         flash[:error] = "Unable to send email activation, try again later" 
      end
     end   
   end
 end
 
 
 def export_to_excel
   if !params[:type]
     @clients =Undpuser.find(:all,:conditions => {"usertype" => 'vendor'}) 
   end
   case params[:type]
   when params[:type].nil?
      @clients =Undpuser.find(:all,:conditions => {"usertype" => 'vendor'})
   when 'consultants'
      @clients =Undpuser.find(:all,:conditions => {"usertype" => 'vendor','vendor_id' => 1})
   when 'ngos'
      @clients =Undpuser.find(:all,:conditions => {"usertype" => 'vendor','vendor_id' => 2})

  when 'suppliers'
      @clients =Undpuser.find(:all,:conditions => {"usertype" => 'vendor','vendor_id' => 3})
 
   end 
  require 'spreadsheet'
  respond_to do |format|
      format.pdf
      format.xls {
        clients = Spreadsheet::Workbook.new
        list = clients.create_worksheet :name => 'All Vendors'
        list.row(0).concat %w[Name Category Programme-Area Special-Area Created]
        list.row(0).height = 30
        list.column(0).width = 60
        list.column(1).width = 60
        list.column(2).width = 30
        list.column(3).width = 30
       
        @category = {"Consultant" => "1" , "NGO" =>2,"Supplier" =>3}
        
        @clients.each_with_index { |client, i|
          if client.vpa.nil?  || client.vss.nil?
            
          list.row(i+1).push client.firstname + " " + client.lastname , @category.key(client.vendor_id),"", "" , client.created_at.to_date
          
       else
          list.row(i+1).push client.firstname + " " + client.lastname , @category.key(client.vendor_id),client.vpa.name, client.vss.name , client.created_at.to_date
       end
        }
        header_format = Spreadsheet::Format.new :color => :blue, :weight => :bold ,:align =>:center
        list.row(0).default_format = header_format
        #output to blob object
        blob = StringIO.new("")
        clients.write blob
        #respond with blob object as a file
        send_data blob.string, :type => :xls, :filename => "Vendors.xls"
      }
end
    
 end
end
