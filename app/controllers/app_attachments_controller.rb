class AppAttachmentsController < ApplicationController
  
  before_filter :login_required , :only => ['new','show','create','update','edit','destroy','index']
  # before_filter :vendors_only ,:only => ['new','create','update','edit']
  before_filter :staff_only
  layout :get_user_layout
 

  def new
  end

  def create
  end

  def index
    @app_attachments = AppAttachment.select("*").where({:vacancy_id => params[:vacancy_id]}).includes(:vac_application).paginate(:page=>params[:page],:per_page=>50)
  end

  def edit
  end

  def update
  end

  def show
  end

  def destroy
  end
end
