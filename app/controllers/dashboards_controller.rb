class DashboardsController < ApplicationController
  
  before_filter :login_required, :only=>['destroy','edit','index','show','update']
  before_filter :staff_only
  
  layout :get_user_layout 
    
  def index
    if session[:first_login] == 1
      session[:first_login] = 0
      redirect_to metrics_path
    end
  end
end
