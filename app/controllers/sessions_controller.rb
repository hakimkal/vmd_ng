class SessionsController < ApplicationController
  
  
  def new
    
     
     
    session[:new_model] = nil
     
     
    if signed_in?
       
      verify_user_section
    end
  end
  
  def create  
  s=params[:session] if request.post?
  #@s=s
 @chk= check_username_or_email(s) if request.post?
  if @chk.nil?
   flash.now[:warning] = "Invalid Username or Password"
   render :new
   
 elsif @chk.suspend == "1"  
   flash.now[:error] = "You are currently suspended from the system, please send an email to vdms.support@undp.org for a resolution of your status!"
   render :new
   
 else
   session[:first_login] = 1
   sign_in @chk
   session[:login_id] = login_audit(@chk)
   cookies[:login_id] = session[:login_id]
   verify_user_section
 end
end
    
  
 def destroy
   session[:user] = nil
   flash[:notice] = "See you soon, #{current_user.username}. Bye!" unless !signed_in?
   sign_out(cookies[:login_id])
   redirect_to  root_path
   return false
 end
 

 
 private 
 
 def check_username_or_email (str={})
    
    if str[:username].blank?
      return nil
    end
   email_format = /\A[\w+\-\d+.]+@[a-z\d+\-.]+\.[a-z]+\z/i
    if str[:username] =~ email_format
    # return str[:username]
      Undpuser.authenticate({:username=> str[:username],:password=> str[:password],:column=>'email'})
    else
       Undpuser.authenticate({:username => str[:username],:password => str[:password],:column=>'username'})
     #return str[:username]
     end
    
 end
end
