class CLanguagesController < ApplicationController
  
   before_filter :login_required , :only => ['index','new','create','update','edit','destroy','show']
  before_filter :consultants_only 
  layout :get_user_layout
 
  def add
    #SESSION tRACKING OF FORMS ADDED
    if session[:added_lng_field].nil? 
      session[:added_lng_field] = 1
    else
    session[:added_lng_field]+= 1
    end
    respond_to do |f|
      f.js
    end
  end
  def new
  end

  def index
  end

  def create
  end

  def edit
    @consultant =CLanguage.find(:first, :conditions => {:consultant_id => params[:consultant_id],:id =>params[:id]})
    if !@consultant 
      flash[:error] = "invalid request"
      verify_user_section
    end
  end

  def update
    @consultant = CLanguage.find(:first, :conditions => {:id =>params[:id]})
    if @consultant.update_attributes params[:c_language]
      flash[:notice] = "Successfully updated"
    else
      flash[:error] = "Unable to update, try again later"
    end
    respond_to do |f|
      f.html {  if identify_vendor.nil? 
                  if request.env["HTTP_REFERER"].nil?
                verify_user_section
                return false
                else
                 redirect_to request.env["HTTP_REFERER"]
                 return false
                 end
              else
                redirect_to c_misc_path :id => @consultant.id , :consultant_id => @consultant.consultant_id 
              end
                 }
      f.json
      f.js
    end
  end


 def destroy
    @consultant = CLanguage.find(:first, :conditions => {:id =>params[:id]})
    if @consultant.destroy
      flash[:notice] = "successfully deleted!"
    else
      flash[:error] = "unable to delete!"
    end
   
     respond_to do |f|
      f.html {  if identify_vendor.nil? 
                if request.env["HTTP_REFERER"].nil?
                verify_user_section
                return false
                else
                 redirect_to request.env["HTTP_REFERER"]
                 return false
                 end
              else
                redirect_to c_misc_path :id => @consultant.id , :consultant_id => @consultant.consultant_id 
              end
                 }
      f.json
      f.js
  end
  end




  def show
  end
end
