class SBankInfosController < ApplicationController
  before_filter :login_required, :only=>['destroy','edit','create','index','show','update','destroy']
   before_filter :suppliers_only
  
  layout :get_user_layout 
  def new
  end

  def create
  end

  def edit
  end

  def update
  end

  def show
  end

  def destroy
  end

  def index
  end
end
