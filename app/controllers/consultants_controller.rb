class ConsultantsController < ApplicationController
  
  before_filter :login_required , :only => ['new','index','create','destroy','edit','update','show']
  before_filter :consultants_only 
  before_filter :staff_only , :only => ['index']
  layout :get_user_layout
  def index
    
       if params[:sort].nil?
         sort = "firstname"
         
       else
         sort = params[:sort]
       end
        @title = 'All Consultants'  
        @consultants=  Consultant.paginate(:page=>params[:page],:per_page=>40).order(sort.to_sym)
    
    if params[:field] != "" && params[:searchString] !=""
       
    case params[:field]
    when "firstname"
      @title = "Showing search  result (s)"
    @consultants = Consultant.where(' firstname ilike ?',"#{params[:searchString]}%").paginate(:page=>params[:page],:per_page=>40).order(sort.to_sym)
    return
     when "lastname"
       @title = "Showing search  result (s)"
    @consultants = Consultant.where(' lastname ilike ?',"#{params[:searchString]}%").paginate(:page=>params[:page],:per_page=>40).order(sort.to_sym)
    return
     when "nationalities"
       @title = "Showing search  result (s)"
    @consultants = Consultant.where(' nationalities ilike ?',"#{params[:searchString]}%").paginate(:page=>params[:page],:per_page=>40).order(sort.to_sym)
    return
    end
    else
      flash[:error] = "No query given for search."
        @consultants=  Consultant.paginate(:page=>params[:page],:per_page=>40).order(sort.to_sym)
    return
    end
  end

  def new
    session[:new_model] = nil
    if params[:undpuser_id].nil?
      @consultant = Consultant.find_by_undpuser_id(current_user.id)
      session[:cid] = @consultant.id
    else
     @consultant = Consultant.find_by_undpuser_id(params[:undpuser_id])
     session[:cid] = @consultant.id
    end
    if @consultant.nil?
      redirect_to request.env['HTTP_REFERER'] unless request.env['HTTP_REFERER'].nil?
      flash[:error] = 'Invalid Consultant chosen'
      verify_user_section 
    end
  end

  def create
  end

  def edit
    if params[:id].nil?
      redirect_to new_consultant_path 
    elsif params[:id] && current_user.group_id == 4
       @consultant = Consultant.find_by_undpuser_id(current_user.id)
    elsif params[:id] && current_user.group_id != 4
      @consultant = Consultant.find_by_id(params[:id])
  end
  end

  def update
    if request.put?
      @consultant = Consultant.find_by_id(params[:id])
      if @consultant.update_attributes(params[:consultant])
       flash[:notice]  = "Successfully Saved data!"
        respond_to do |format|
        format.js
        format.html {
          
          if identify_vendor.nil?
            if request.env["HTTP_REFERER"].nil?
                verify_user_section
                return false
                else
                 redirect_to request.env["HTTP_REFERER"]
                 return false
                 end
          else  
          
          verify_user_section
          end
          }
        end
        
      else
        flash[:error] = "Unable to save data, try again later!"
        respond_to do |format|
        format.js
               end
    end
    end
  end

  def show
    if current_user.group_id == 4
      @consultant = Consultant.find_by_undpuser_id(current_user.id,:include => [:c_contact_details,:ctelephone])
      session[:consultant] =nil
      
    else
      @consultant = Consultant.find_by_id(params[:id],:include => [:c_contact_details,:ctelephone])
      
      if !@consultant || @consultant.nil?
        flash[:error] = "Invalid consultant"
        if request.env["HTTP_REFERER"].nil?
        redirect_to undpusers_url(:type => 'vendors')
        return false
        else
         redirect_to request.env["HTTP_REFERER"]
         return false
      end
     else 
       @user_title = "For #{@consultant.firstname}  #{@consultant.lastname} :: Consultant"
      end
    end
  end

  def destroy
  end
  
  
  def export_to_excel
   if params[:consultants].nil?
     @clients = Consultant.find(:all,:include=>[:undpuser,:ctelephone,:c_references]) 
   
  elsif params[:consultants] == 'selected' && !session[:consultants].nil?
  @clients = Consultant.find(session[:consultants],:include=>[:undpuser,:ctelephone,:c_references]) 
   
   else
     flash[:error] = "No consultants selections detected"
     redirect_to :back
     return
  end
  
  
 



  require 'spreadsheet'
  respond_to do |format|
      format.pdf
      format.xls {
        clients = Spreadsheet::Workbook.new
        list = clients.create_worksheet :name => 'Consultant Vendors'
        list.row(0).concat %w[Name Maiden-name DOB Place-of-birth Nationality-at-birth Gender Marital-status Disability-from-air-travel nationality(ies)  Email Home-Phone Mobile Work Fax]
        list.row(0).height = 30
        list.column(0).width = 60
        list.column(1).width = 50
        list.column(2).width = 50
        list.column(3).width = 50
       list.column(4).width = 50
        list.column(5).width = 50
        list.column(6).width = 50
       list.column(7).width = 50
        list.column(8).width = 50
        list.column(9).width = 50
       list.column(10).width = 50
        list.column(11).width = 50
        list.column(12).width = 50
       list.column(13).width = 50
       
        
        
        @clients.each_with_index { |client, i|
          if !client.ctelephone.nil? && !client.nil?
            
          list.row(i+1).push(client.firstname + " " + client.lastname ,
          client.other_name,client.dob,client.place_of_birth,client.nationality_at_birth,
          client.gender,client.marital_status,
          client.disability_from_air_travel,client.nationalities ,client.undpuser.email, client.ctelephone.home,client.ctelephone.mobile,client.ctelephone.work , client.ctelephone.fax,
          
         
            
            )
 
    
       elsif client.ctelephone.nil? && !client.nil?
         
           list.row(i+1).push("#{client.firstname}  #{client.lastname}" ,
          client.other_name,client.dob,client.place_of_birth,client.nationality_at_birth,
          client.gender,client.marital_status,
          client.disability_from_air_travel,client.nationalities ,client.undpuser.email
          
          )
             
       end
        }
        
        header_format = Spreadsheet::Format.new :color => :blue, :weight => :bold ,:align =>:center
        list.row(0).default_format = header_format
        #output to blob object
        session[:consultants] = session[:user_ids] = nil
        blob = StringIO.new("")
        clients.write blob
        #respond with blob object as a file
        send_data blob.string, :type => :xls, :filename => "Consultant_Vendors_exported_#{Time.now}.xls"
      }
end
    
 end

end
