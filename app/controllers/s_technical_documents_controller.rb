class STechnicalDocumentsController < ApplicationController
  
  before_filter :login_required, :only=>['destroy','edit','create','index','show','update','destroy']
   before_filter :suppliers_only
  
  layout :get_user_layout 
  def new
  end

  def index
  end

  def create
    if request.post?
         
      @nupload = STechnicalDocument.new params[:s_technical_document]
      if @nupload.save
        session[:new_model] = nil
        flash[:notice] = "Successfully saved upload(s)"
       redirect_to new_supplier_url(:id => params[:s_technical_document][:supplier_id])
       return false
      else
        flash[:error] = "unable to save uploads...Please make sure to upload each resource not more than 2.9Mb. Duplicates are not acceptable. <br/> You may edit your upload(s) at any time!".html_safe
           session[:new_model] = @nil
        redirect_to new_supplier_url(:id => params[:s_technical_document][:supplier_id])
        return false
      end
    
    end
     
  end

  def edit
  end

  def update
    
    if request.put?
         
      @nupload = STechnicalDocument.find_by_id(params[:id])
       if params[:s_technical_document][:written] && !params[:s_technical_document][:intl_representation] && !params[:s_technical_document][:goods_services] && !params[:s_technical_document][:quality_assurance] && !params[:s_technical_document][:fin_report]
        @nupload.written = nil
        @nupload.save
       
      elsif !params[:s_technical_document][:written] && params[:s_technical_document][:intl_representation] && !params[:s_technical_document][:goods_services] && !params[:s_technical_document][:quality_assurance] && !params[:s_technical_document][:fin_report]
        @nupload.intl_representation = nil
        @nupload.save
       
       elsif !params[:s_technical_document][:written] && !params[:s_technical_document][:intl_representation] && params[:s_technical_document][:goods_services] && !params[:s_technical_document][:quality_assurance] && !params[:s_technical_document][:fin_report]
       @nupload.goods_services = nil
        @nupload.save
       
       elsif !params[:s_technical_document][:written] && !params[:s_technical_document][:intl_representation] && !params[:s_technical_document][:goods_services] && !params[:s_technical_document][:quality_assurance] && params[:s_technical_document][:fin_report]
        @nupload.fin_report = nil
        @nupload.save
        
       elsif params[:s_technical_document][:written] && !params[:s_technical_document][:intl_representation] && !params[:s_technical_document][:goods_services] && params[:s_technical_document][:quality_assurance] && !params[:s_technical_document][:fin_report]
        @nupload.quality_assurance = nil
        @nupload.save
       
       elsif params[:s_technical_document][:written] && params[:s_technical_document][:intl_representation] && params[:s_technical_document][:goods_services] && params[:s_technical_document][:quality_assurance] && params[:s_technical_document][:fin_report]
        @nupload.written = nil
         @nupload.quality_assurance = nil
          @nupload.fin_report = nil
           @nupload.goods_services = nil
            @nupload.intl_representation = nil
        @nupload.save
       
       end
       
      if @nupload.update_attributes params[:s_technical_document]
         
        flash[:notice] = "Successfully saved upload(s)"
       redirect_to new_supplier_url(:id => params[:s_technical_document][:supplier_id])
       return false
  else
        flash[:error] = "unable to update upload(s)...Please make sure to upload each resource not more than 2.9Mb. Duplicates are not acceptable. <br/> You may edit your upload(s) at any time!".html_safe
           
       redirect_to new_supplier_url(:id => params[:s_technical_document][:supplier_id])
         return false
      end
    
    end
  end


  def edit
  end

  def destroy
  end

  def show
  end
end
