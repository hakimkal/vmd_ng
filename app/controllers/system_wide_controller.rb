class SystemWideController < ApplicationController
   before_filter :login_required
   before_filter :staff_only 
  
  layout :get_user_layout
  def new
  end

  def index
  end

  def metrics
    
    @vendors = Undpuser.count(:conditions=> {:group_id=>4})
   @consultants =  Undpuser.count(:conditions=>{:group_id=>4,:vendor_id=>1})
   @ngos =  Undpuser.count(:conditions=>{:group_id=>4,:vendor_id=>2})
   @suppliers= Undpuser.count(:conditions=>{:group_id=>4,:vendor_id=>3})
   @vacancies_opened = Vacancy.count(:conditions=>{:status=>0})
   @vacancies_all = Vacancy.count
  end

  
  def collate_for_export
     
    case params[:export_selected][:types]
    when "consultants"
      v_ids =  []
      if params[:consultants].nil?
        return 
      end
      params[:consultants].each do |v|
        v_ids << v
      end
      vendors = Consultant.find(v_ids)
      @vendors = []
      @vendor_users = []
      vendors.each do |v|
        @vendors << v.id
        @vendor_users << v.undpuser_id
       
      end 
      
     
       stored_consultants = session[:consultants] if !session[:consultants].nil?
       stored_users = session[:user_ids]  if !session[:user_ids].nil?
     
        if stored_consultants.nil?
       session[:consultants] = @vendors
       elsif !stored_consultants.nil?
        @vendors.concat(stored_consultants)
         session[:consultants] = @vendors.uniq
      end
       if stored_users.nil?
       session[:user_ids] = @vendor_users
       elsif !stored_users.nil?
         @vendor_users.concat(stored_users)
         session[:user_ids] = @vendor_users.uniq
       end
    
    ###Suppliers Export Processing
    
     when "suppliers"
      v_ids =  []
      if params[:suppliers].nil?
        return 
      end
      params[:suppliers].each do |v|
        v_ids << v
      end
      vendors = Supplier.find(v_ids)
      @vendors = []
      @vendor_users = []
      vendors.each do |v|
        @vendors << v.id
        @vendor_users << v.undpuser_id
       
      end 
      
     
       stored_suppliers = session[:suppliers] if !session[:suppliers].nil?
       stored_users = session[:user_ids]  if !session[:user_ids].nil?
     
        if stored_suppliers.nil?
       session[:suppliers] = @vendors
       elsif !stored_suppliers.nil?
        @vendors.concat(stored_suppliers)
         session[:suppliers] = @vendors.uniq
      end
       if stored_users.nil?
       session[:user_ids] = @vendor_users
       elsif !stored_users.nil?
         @vendor_users.concat(stored_users)
         session[:user_ids] = @vendor_users.uniq
       end
    
    ##for Ngo Export Processing
    
     when "ngos"
      v_ids =  []
      if params[:ngos].nil?
        return 
      end
      params[:ngos].each do |v|
        v_ids << v
      end
      vendors = Ngo.find(v_ids)
      @vendors = []
      @vendor_users = []
      vendors.each do |v|
        @vendors << v.id
        @vendor_users << v.undpuser_id
       
      end 
      
     
       stored_ngos = session[:ngos] if !session[:ngos].nil?
       stored_users = session[:user_ids]  if !session[:user_ids].nil?
     
        if stored_ngos.nil?
       session[:ngos] = @vendors
       elsif !stored_ngos.nil?
        @vendors.concat(stored_ngos)
         session[:ngos] = @vendors.uniq
      end
       if stored_users.nil?
       session[:user_ids] = @vendor_users
       elsif !stored_users.nil?
         @vendor_users.concat(stored_users)
         session[:user_ids] = @vendor_users.uniq
       end
    
    ## Application processing
    
    when "applicants"
      application_ids =  []
      if params[:applicants].nil?
        return 
      end
      params[:applicants].each do |v|
        application_ids << v
      end
      vendors = VacApplication.find(application_ids)
      @vendors = []
      @vendor_users = []
      vendors.each do |v|
        @vendors << v.id
        @vendor_users << v.undpuser_id
       
      end 
      
     
       stored_applicants = session[:applicants] if !session[:applicants].nil?
       stored_users = session[:user_ids]  if !session[:user_ids].nil?
     
        if stored_applicants.nil?
       session[:applicants] = @vendors
       elsif !stored_applicants.nil?
        @vendors.concat(stored_applicants)
         session[:applicants] = @vendors.uniq
      end
       if stored_users.nil?
       session[:user_ids] = @vendor_users
       elsif !stored_users.nil?
         @vendor_users.concat(stored_users)
         session[:user_ids] = @vendor_users.uniq
       end
    
    
    
    ## recruitment processing
 ssing
    
    when "recruits"
      recruit_ids =  []
      if params[:recruits].nil?
        return 
      end
      params[:recruits].each do |v|
        recruit_ids << v
      end
      vendors = Recruitment.find(recruit_ids)
      @vendors = []
      @vendor_users = []
      vendors.each do |v|
        @vendors << v.id
        @vendor_users << v.undpuser_id
       
      end 
      
     
       stored_applicants = session[:recruits] if !session[:recruits].nil?
       stored_users = session[:user_ids]  if !session[:user_ids].nil?
     
        if stored_applicants.nil?
       session[:recruits] = @vendors
       elsif !stored_applicants.nil?
        @vendors.concat(stored_applicants)
         session[:recruits] = @vendors.uniq
      end
       if stored_users.nil?
       session[:user_ids] = @vendor_users
       elsif !stored_users.nil?
         @vendor_users.concat(stored_users)
         session[:user_ids] = @vendor_users.uniq
       end
    
    end
    
    respond_to do |f|
      
      f.js 
      
    end
  end
  
  def create
  end

  def post
  end

  def edit
  end

  def destroy
  end

  def show
  end
end
