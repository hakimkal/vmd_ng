class ConservicesController < ApplicationController
   before_filter :login_required, :only=>['destroy','index','show','new']
  before_filter :check_user_group , :only=>['destroy','index','show','new']
  
  layout :get_user_layout
 
  def index
    @conservices = @vpas= Conservice.paginate(:page=>params[:page], :per_page => 40)
  end

  def new
  end

  def create
     if request.post?
       @conservice = Conservice.new(params[:conservice])
       respond_to do |format|
      if @conservice.save
        @content = "Successfully created service"
        format.js 
     else
     @content = "Failed to create service, Duplicates are not allowed"
      format.js 
      end
    
  end
end
end
  def destroy
    if  Conservice.find(params[:id]).destroy
       flash[:notice] = "Successfully deleted service area "
       redirect_to  conservices_url 
     else
       flash[:error] = "Unable to delete service area" 
       redirect_to  conservices_url
     end
      
 
  end

  def edit
    @conservice = @vpa = Conservice.find_by_id(params[:id])
  end

  def show
  end

  def update
    if request.put?
     @conservice =  @vss = Conservice.find_by_id(params[:id])
      if @vss && @vss.update_attributes(params[:conservice])
        flash[:notice] = "Successfully updated  #{params[:conservice][:name]}"
       
      else
         flash[:error] = "Failed to update service label or title : #{params[:conservice][:name]} , Try again later."
         
    end
    redirect_to conservices_url
  end
  end
end
