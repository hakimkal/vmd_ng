class AcoArosController < ApplicationController
  before_filter :login_required , :only => ['index','new','create','update','edit','destroy','show']
  before_filter :staff_only 
  
  before_filter :only => [:new, :edit] {
    |co| 
    #session[:co] = co.params
    co.check_admin co.params
    
    }
  #before_filter :check_consultant  ,:except => ['create','add','remove']
  layout :get_user_layout

  def new
    @aco_aro = AcoAro.new
    @modules = SysModule.select("id,name").order("id")
    if request.post?
    
     case params[:atype]
     when "u"
       
       @user = Aro.find_by_undpuser_id(params[:undpuser_id])
       if AcoAro.count(:conditions => {:aro_id =>@user.id}) > 0
         redirect_to edit_aco_aro_url(:id=>@user.id, :aro_id => @user.id,:atype =>"u")
       end
     when "g"
       @user = Aro.find(:first, :conditions=>{:group_id => params[:group_id], "undpuser_id" => nil})
       
       if AcoAro.count(:conditions => {:aro_id =>@user.id}) > 0
         redirect_to edit_aco_aro_url(:id=>@user.id,:aro_id => @user.id,:atype =>"g")
       end
     end
    else
      flash[:error]  =  "Invalid request!" 
      verify_user_section
    return false
    end
    
   
  end

  def create
    if request.post?
      
      acos = params[:aco_aro][:aco_id]
      acos_store = []
      acos.each do |aco,val|
      
      acos_store <<  {
                        :aco_id => aco , 
                        :aro_id => params[:aco_aro][:aro_id],
                        :allow =>val
                      } 
      end
      
      
      if AcoAro.create(acos_store)
        flash[:notice] = "Successfully saved permissions details for #{params[:aco_aro][:alias]}"
       else
        flash[:error] = "Sunable to save permissions details for #{params[:aco_aro][:alias]}"
         
      end
     verify_user_section
     return false
    end
  end

  def index
  end

  def edit
    
   
    @modules = SysModule.select("id,name").order("id")
    if request.get?
    
     case params[:atype]
     when "u"
       
        @user = Aro.find_by_id(params[:aro_id])
       if AcoAro.count(:conditions => {:aro_id =>params[:aro_id]}) > 0
      # @aco_aro = AcoAro.find(:all,:conditions => {:aro_id =>params[:aro_id]},:include=>[:aco,:aro])
       
       end
     when "g"
       @user = Aro.find_by_id(params[:aro_id])
       
       if AcoAro.count(:conditions => {:aro_id =>params[:aro_id]}) > 0
       # @aco_aro = AcoAro.find(:all,:conditions => {:aro_id =>params[:aro_id]},:include=>[:aco,:aro])
      
       end
     end
    else
      flash[:error]  =  "Invalid request!" 
      verify_user_section
    return false
    end
    
  end

  def destroy
  end

  def show
  end

  def update
    
    if request.put?
      acos = params[:aco_aro][:aco_id]
      
     # @aro = AcoAro.find(:all,:conditions => {:aro_id =>params[:id]})
      
        aco_ar  = []
        
       params[:aco_aro][:id].each do |a,v|   
           
       acos.each do |ac,vl|
            if a == ac
                     aco_ar << { 
                     :id => v.to_i,
                    :aco_id => ac.to_i , 
                    :allow =>vl ,
                    :aro_id => params[:id]
                  }
              
            end
           end
        end
                  #if aco_ar.update_attributes(h)
                  session[:aco_ar] =  aco_ar
                 flash[:notice] = []
                 flash[:error] = []
                  aco_ar.each do |r|
                    ac_r = AcoAro.find(r[:id].to_i)
                    
                    if ac_r.update_attributes r 
                    
                   flash[:notice] << "Successfully saved permissions details for #{params[:aco_aro][:alias]}"
                  else
                  session[:new_model] = aco_ar
                  flash[:error] << "unable to save permissions details for #{params[:aco_aro][:alias]}"
               
                  end
            
                    
                  end
                   
            s = flash[:notice].size
            f = flash[:error].size
            flash[:notice] = flash[:notice].uniq
             flash[:notice] =  "#{s} were #{flash[:notice]}" 
           
            flash[:error] = flash[:error].uniq
             flash[:error] =  "#{f}  #{flash[:notice]} occurred!" 
              
                 
         
     
       
       
       
     verify_user_section
     return false
    end
    
  end
  
end
