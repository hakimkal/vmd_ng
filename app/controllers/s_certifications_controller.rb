class SCertificationsController < ApplicationController
  
  
   before_filter :login_required, :only=>['destroy','edit','create','index','show','update','destroy']
   before_filter :suppliers_only
  
  layout :get_user_layout 
  def new
  end

  
  def update
    if request.put?
      @nfinancial = SCertification.find_by_id(params[:id])
      if params[:s_certification][:certify] == "No"
         @nfinancial.update_attributes(params[:s_certification])
        flash[:notice] = "we have received your declination, your data will be wiped out in the next system cleanup exercise!"
       session[:new_model] = nil
      elsif params[:s_certification][:certify] != "No"
      
     @nfinancial.update_attributes(params[:s_certification])
        flash[:notice] = "successfully updated your certification"
        session[:new_model] = nil
      else
        flash[:error] = "unable to update changes"
        session[:new_model] = @nfinancial
      end
      
       
    
   respond_to do |format|
     if identify_vendor.nil?
        
      else
        
      format.html { redirect_to new_supplier_url :id=> params[:s_certification][:supplier_id]  
           
                  return false
                  }
     end
   end
    end   
  end

  def create
    
      @nfinancial = SCertification.new(params[:s_certification])
      
      if (@nfinancial.certify == "No")
          if @nfinancial.save
          flash[:notice] = "we have received your declination, your data will be wiped out in the next system cleanup exercise!"
          session[:new_model] = nil
         else
          flash[:error] = "unable to save certification"
          session[:new_model] = @nfinancial
            redirect_to new_supplier_url :id=> params[:s_certification][:supplier_id]  
            return false
         end
      
      elsif(@nfinancial.certify != "No")
            
     
         if @nfinancial.save
          session[:new_model] = nil
          flash[:notice] = "we acknowledge your certification, thank you!"
          
        else
          flash[:error] = "unable to save certification, please try again later"
          session[:new_model] = @nfinancial
           redirect_to new_supplier_url :id=> params[:s_certification][:supplier_id]  
             return false
        end
  
     end
       
    respond_to do |format|
      if identify_vendor.nil?
        
      else
        
      format.html { redirect_to new_supplier_url :id=> params[:s_certification][:supplier_id]  
           
                  return false
                  }
     end
   end
    
    
  end


  def edit
  end

  def index
  end

  def show
  end

  def destroy
  end
end
