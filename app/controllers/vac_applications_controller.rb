class VacApplicationsController < ApplicationController
  before_filter :login_required , :only => ['new','show','create','update','edit','destroy','index']
  before_filter :vendors_only ,:only => ['new','create','update','edit']
  layout :get_user_layout
 
  def new
      flash.keep
    #if p11_completed?(identify_vendor).nil?
    # flash.keep
     
    #  redirect_to :back
     # return false
   # end
    
    @vacancy = Vacancy.find(:first,:conditions=>{:id=>params[:id],:status=>0}) 
    
    
     
    
    if !@vacancy.blank?
    
      if !Vacancy.vac_deadline_status?(@vacancy.deadline)
      
     flash[:error] = " This Vacancy is past the deadline!"
     if !request.env["HTTP_REFERER"].blank?
       redirect_to :back 
       return false
     else
       redirect_to view_vacancies_url
       return false
     end
    
     end
       
    else
      flash[:error] = "Invalid Vacancy!!!"
      redirect_to view_vacancies_url
      return false
    end
    @application =  VacApplication.new
  
    
  end

  def create
    if request.post?
       @applicant = VacApplication.new params[:vac_application]
  
      if(VacApplication.count(:conditions=>{:vacancy_id=>@applicant.vacancy_id,:undpuser_id =>@applicant.undpuser_id}) > 0 )     
       flash[:error] = "You have earlier applied for this position"
      redirect_to vacancy_url(params[:vac_application][:vacancy_id])
      return false
      end
     
     
    if @applicant.requires_attachment == "1"
       @app_attachment = AppAttachment.new params[:app_attachment]
      
       if @applicant.save
        
          @app_attachment.application_id = @applicant.id
          @app_attachment.vacancy_id = @applicant.vacancy_id
          if @app_attachment.save
            m = {}
            m[:email] = current_user.email
            m[:subject] = "Application received for #{@applicant.vac_name} Vacancy Position"
            m[:firstname] = current_user.firstname
            m[:lastname] = current_user.lastname
            m[:vacancy] = @applicant.vac_name
            VacApplicationMailer.application_received(m).deliver
            flash[:notice] = "Successfully received your application and a notification has been sent to your email."
            #send email
         else
           flash[:error] = "unable to process your application."
           
          end
      
    end 
      
     elsif @applicant.requires_attachment == "0"
     
           if @applicant.save
            m = {}
            m[:email] = current_user.email
            m[:subject] = "Application received for #{@applicant.vac_name} Vacancy Position"
            m[:firstname] = current_user.firstname
            m[:lastname] = current_user.lastname
            m[:vacancy] = @applicant.vac_name
            VacApplicationMailer.application_received(m).deliver
          
           flash[:notice] = "Successfully received your application and a notification has been sent to your email."
             
              
            
           else
             
             flash[:error] = "unable to process your application."
           
           end
        
   end
   
  respond_to do |format|
    format.html {redirect_to view_vacancies_url and return }
  end
   end 
  end

  def edit
  end

  def update
  end

  def show
    @vacancy = @vac_applications = VacApplication.select("*").where(:vacancy_id => params[:vacancy_id]).includes([:app_attachments,:vacancy,:undpuser]).paginate(:per_page=>50,:page=>params[:page])
    @requires_attachment = params[:req]
    @attachments = AppAttachment.count(:conditions=>{:vacancy_id => params[:vacancy_id]})
    @vac_name = Vacancy.find_by_id(params[:vacancy_id])
   if @vacancy.blank?
     flash[:notice] = "No entry found for the requested position"
     redirect_to :back
   end
  end

  def index
    
    
   
    case params[:show]
     when 'all'  
      
     @vacancies = @vac_applications = Vacancy.select("*").includes(:vac_applications).paginate(:per_page=>50,:page=>params[:page])
     when nil
      @vacancies = @vac_applications = Vacancy.select("*").includes(:vac_applications).paginate(:per_page=>50,:page=>params[:page])
      
      when 'cons'
         @vacancies = @vac_applications = Vacancy.where(:vendor_id => 1).includes(:vac_applications).paginate(:per_page=>50,:page=>params[:page])
     
      when 'ngos'
        @vacancies = @vac_applications = Vacancy.where(:vendor_id => 2).includes(:vac_applications).paginate(:per_page=>50,:page=>params[:page])
      
      when 'supp'
         @vacancies = @vac_applications = Vacancy.where(:vendor_id => 3).includes(:vac_applications).paginate(:per_page=>50,:page=>params[:page])
     
     
    
     
    end
     if @vacancies.blank?
       flash[:error] = "No Application entries found"
       redirect_to :back
       return false
     end
    
    #end
  end


def export_to_excel
   

if params[:applicants].nil?
     @clients = VacApplication.find(:all,:conditions=>{:vacancy_id=>params[:vacancy_id]},:include=>[:undpuser,:vacancy]) 
     ruler = VacApplication.find(:first,:conditions=>{:vacancy_id=>params[:vacancy_id]},:include=>[:undpuser,:vacancy]) 
     
  elsif params[:applicants] == 'selected' && !session[:applicants].nil?
  @clients = VacApplication.find(:all,:conditions=>{:id=>session[:applicants],:vacancy_id=>params[:vacancy_id]},:include=>[:undpuser,:vacancy]) 
   ruler = VacApplication.find(:first,:conditions=>{:id=>session[:applicants],:vacancy_id=>params[:vacancy_id]},:include=>[:undpuser,:vacancy]) 
     
   else
     flash[:error] = "No  selection(s) detected"
     redirect_to :back
     return
  end

 



  require 'spreadsheet'
  respond_to do |format|
      format.pdf
      format.xls {
        clients = Spreadsheet::Workbook.new
        list = clients.create_worksheet :name => "#{ruler.vacancy.name}" + '_Applicants'
        list.row(0).concat %w[Applicant  Email  Vacancy  Job-location Deadline Contract-type Ad-Category Required-working-language   ]
        
          
 
        
        list.row(0).height = 23 
        list.column(0).width =  ruler.undpuser.firstname.length + ruler.undpuser.lastname.length + 5
        list.column(1).width = ruler.undpuser.email.length + 5
        list.column(2).width = ruler.vacancy.name.length + 5
        list.column(3).width = 18
       list.column(4).width = 18
        list.column(5).width = 18
        list.column(6).width = 18
       list.column(7).width = "Required-working-language".length
         
        
                
        @clients.each_with_index { |client, i|
          if !client.nil? 
        
          # Contract-type Ad-Category Required-working-language   ]
            
          list.row(i+1).push("#{client.undpuser.firstname} #{client.undpuser.lastname.to_s.upcase}", client.undpuser.email,client.vacancy.name, client.vacancy.location,
          client.vacancy.deadline.gsub("-","/"),  client.vacancy.contract_type,client.vacancy.vendor_type.name,client.vacancy.language_required )
         
            
            
 
    
        
           
       end
        }
        
        header_format = Spreadsheet::Format.new :color => :blue, :weight => :bold ,:align =>:center
        list.row(0).default_format = header_format
        #output to blob object
        session[:applicants] = session[:user_ids] = nil
        blob = StringIO.new("")
        clients.write blob
        #respond with blob object as a file
        send_data blob.string, :type => :xls, :filename => "applicants_for_" + "#{ruler.vacancy.name}" + "_exported_" + "#{Time.now}"+".xls"
      }
end
    


  end
  def destroy
    @vacancy = VacApplication.find(:all,:conditions=>{:vacancy_id => params[:vacancy_id]},:include =>[:vacancy,:undpuser])
    
    if @vacancy.blank?
      flash[:notice] = "No entries found"
      redirect_to :back
    end
    if params[:delete] == "1"
    @vacancy = VacApplication.find(:all,:conditions=>{:vacancy_id => params[:vacancy_id]})
     
    @vacancy.each do |application|
     VacApplication.find(application.id).destroy 
    end 
        
      flash[:notice] = "Successfully delisted all Applications"
      redirect_to vac_applications_url and return
    
     
    end
  end
  
   private
  
  def p11_completed? user
     flash.keep
    if current_user.vendor_id == 1
      
     
           c_detail = CContactDetail.count(:conditions=>{:consultant_id => user})
           if c_detail < 1
             flash[:error] = "Your Contact Detail(s) is blank, Please complete your Personal History Form before Applying for any position!"
             return nil
           end
           
           c_reference = CReference.count(:conditions=>{:consultant_id => user})
          
            if c_reference < 3
             flash[:error] = "Three Referees Required, Please complete your Personal History Form before Applying for any position!"
             return nil
           end
          
        c_education = CEducation.count(:conditions=>{:consultant_id => user})
        if c_education < 1
             flash[:error] = "Education Details Required, Please complete your Personal History Form before Applying for any position!"
             return nil
           end
          
         
        c_language = CLanguage.count(:conditions=>{:consultant_id => user})
        
        if c_language < 1
             flash[:error] = "Language Details Required, Please complete your Personal History Form (Misc Tab) before Applying for any position!"
             return nil
           end
        
        c_certification = CCertification.count(:conditions=>{:consultant_id => user})
        
        if c_certification < 1
             flash[:error] = "Certification Status should be Accept, Please complete your Personal History Form before Applying for any position!"
             return nil
           end
      # c_misc = CMisc.count(:condition=>{:undpuser_id => user.id}) 
        
        ctelephone = Ctelephone.count(:conditions=>{:consultant_id => user})
        if ctelephone < 1
             flash[:error] = "Telephone Details Required, Please complete your Personal History Form before Applying for any position!"
             return nil
             
        else
          return true
           end
               #c_upload = Cupload.count(:condition=>{:undpuser_id => user.id})
   
  else
   # flash[:notice] = "Not a consultant"
    return true
    end
    
  end
end
