class StatusesController < ApplicationController
  before_filter :login_required , :only => ['index','create','update','edit','destroy']
  before_filter :check_user_group
  layout :get_user_layout

  respond_to :json , :only => [:listed]
  def new
  end

  def create
    if request.post?
      @status =@model =  Status.new(params[:status])
      if  @status.save
        flash[:notice] = "Successfully added  status"
     else
         flash[:error]  = "unable to add status..."
         session[:new_model] = @status.errors
         
    
     end
         redirect_to new_vendor_type_url   
        
    end
  end
  
  def listed
    
    if request.get?
    list=  Status.select("id , name")
    respond_with list
    end
  end
  def index
    @title = 'Recruitment Status'
    @statuses = @vpas =Status.paginate(:page=>params[:page],:per_page=>50)
  end

  def edit
      @vss=@vpa = Status.find_by_id(params[:id])
     
   redirect_to request.env['HTTP_REFERER'] if @vss.nil?
  end

  def update
    if request.put?
      @vss = Status.find_by_id(params[:id])
      if @vss && @vss.update_attributes(params[:status])
        flash[:notice] = "Successfully updated Recruitment #{params[:status][:name]}"
       
       else
         flash[:error] = "Failed to update recruitment label or title : #{params[:status][:name]} , Try again later."
         
    end
    redirect_to statuses_url
  end
  end

  def show
  end

  def destroy
    if Status.find_by_id(params[:id]).destroy
      flash[:notice] = "Successfully deleted recruiment item"
      redirect_to statuses_url 
    else
      flash[:error] = "Unable to delete recruitment item"
    redirect_to statuses_url 
    end
     
  end
  
end
