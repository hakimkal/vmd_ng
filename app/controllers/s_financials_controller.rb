class SFinancialsController < ApplicationController
  
   before_filter :login_required, :only=>['destroy','edit','create','index','show','update','destroy']
   before_filter :suppliers_only
  
  layout :get_user_layout 
   
  def new
  end

   

  def edit
  end

   def update
    if request.put?
      @nfinancial = SFinancial.find_by_id(params[:id])
      @nfinancial.update_attributes(params[:s_financial])
      
      @nbank = SBankInfo.find_by_supplier_id(params[:s_bank_info][:supplier_id])
      if !@nbank
        @nbank = SBankInfo.new(params[:s_bank_info])
        if @nbank.save 
          flash[:notice] = "sucessfully saved financials."
          session[:new_model] = nil
        else
          flash[:error]  = "unable to save"
          session[:new_model] = @nbank
         redirect_to new_supplier_url :id=> params[:s_financial][:supplier_id]  
          
          return false
        end
    else
      @nbank.update_attributes(params[:s_bank_info])
      flash[:notice] = "successfully updated financials."
       session[:new_model] = nil   
    end
    
   respond_to do |format|
    format.html {
     if identify_vendor.nil?
        redirect_to :back 
        return false
      else
        
      redirect_to new_supplier_url :id=> params[:s_financial][:supplier_id]  
          
                  return false
                end
                  }
    
   end
    end   
  end

  def create
    if request.post? 
      @nfinancial = SFinancial.new(params[:s_financial])
      if @nfinancial.save
        session[:new_model] = nil
        flash[:notice] = "successfully saved financial details"
        
      else
        flash[:error] = "unable to save financial details"
        session[:new_model] = @nfinancial
          redirect_to new_supplier_url :id=> params[:s_financial][:supplier_id]  
          return false
        
      end
      
      if params[:s_bank_info]
        @nbank = SBankInfo.new(params[:s_bank_info])
        if @nbank.save
           session[:new_model] = nil
          flash[:notice] = "successfully saved financial and  bank details"
        else
          flash[:notice] = nil
          flash[:error] = "unable to save financial and bank details"
          session[:new_model] = @nbank
          redirect_to new_ngo_url(:id=> params[:n_financial][:ngo_id])
          return false 
          
        end
      end
    respond_to do |format|
      if identify_vendor.nil?
        
      else
        
      format.html {  redirect_to new_supplier_url :id=> params[:s_financial][:supplier_id]  
          
                  return false
                  }
     end
   end
    end
   return false 
  end


  def show
  end

  def destroy
  end

  def index
  end
end
