class CContactDetailsController < ApplicationController
  
  before_filter :login_required, :only=>['destroy','index','show','new','create','update']
  before_filter :consultants_only , :only=>['destroy','index','show','new','create','update']
   before_filter :check_consultant  ,:except => ['create','add','remove']
 
  before_filter
  
  layout :get_user_layout
  def new
  end

  def index
  end

  def edit
    @consultant = CContactDetail.find(:first , :conditions => {:consultant_id => params[:consultant_id],:id => params[:id]})
    if !@consultant
      flash[:error] = "invalid request"
      verify_user_section
    end
  end

  def update
     @consultant = CContactDetail.find(:first , :conditions => {:id => params[:id]})
    if !@consultant
      flash[:error] = "invalid request"
      verify_user_section
    end
    if @consultant.update_attributes params[:c_contact_detail]
      flash[:notice] = "Successfully updated contact information"
     else
       flash[:error] = "unable to update contact information, please try again later"
    end
    
    respond_to do  |f|
      f.html { if identify_vendor.nil?
                if request.env["HTTP_REFERER"].nil?
                verify_user_section
                return false
                else
                 redirect_to request.env["HTTP_REFERER"]
                 return false
                 end
               else
               verify_user_section
               end
                    }
      f.json
      f.js
    end
  end

  def destroy
    
    if CContactDetail.find_by_id(params[:id]).destroy
      flash[:notice] = "Successfully deleted contact information"
     else
       flash[:error] = "unable to delete contact information, please try again later"
    end
    
    respond_to do  |f|
      f.html { if identify_vendor.nil?
                if request.env["HTTP_REFERER"].nil?
                verify_user_section
                return false
                else
                 redirect_to request.env["HTTP_REFERER"]
                 return false
                 end
               else
               verify_user_section
               end
                    }
      f.json
      f.js
    end
  end

  def show
  end

  def create
    
  
 
  @contacts_telephone = params[:c_contact_detail][:c_telephone]
  params[:c_contact_detail].delete(:c_telephone)
  #@contacts = CContactDetail.new(params[:c_contact_detail])
 
     #params[:c_contact_detail].each do |vss_name|
      #vendor_specialization << {:name =>vss_name, :vendor_type_id => params[:vss][:vendor_type_id] , :vpa_id => params[:vss][:vpa_id] }
     #end
     session[:refs] = nil
     session["refs"] = nil
     session[:params] = nil
     refs = []
    
    ct = params[:c_contact_detail].size
    session[:ct] = ct
    #session[:params] = params[:c_contact_detail]
    if params[:c_contact_detail]["0"][:address_equal] ==  1
      
       params[:c_contact_detail]["1"][:street_adress] =  params[:c_contact_detail]["0"][:street_adress]
          params[:c_contact_detail]["1"][:state] = params[:c_contact_detail]["0"][:state]
             params[:c_contact_detail]["1"][:town]= params[:c_contact_detail]["0"][:town]
         
           params[:c_contact_detail]["1"][:country]= params[:c_contact_detail]["0"][:country]
           params[:c_contact_detail]["1"][:telephone] =  params[:c_contact_detail]["0"][:telephone]
               params[:c_contact_detail]["1"][:address_type]= 'Present'
          params[:c_contact_detail]["1"][:consultant_id] =  params[:c_contact_detail]["0"][:consultant_id]
      
    end
    session[:prms] = nil
    
    if params[:c_contact_detail]["0"][:address_equal] ==  0
      for i in 0..(ct-1) 
    
     refs << {
        :street_address => params[:c_contact_detail]["#{i}"][:street_address],
        :state =>  params[:c_contact_detail]["#{i}"][:state] ,
        :town =>      params[:c_contact_detail]["#{i}"][:town],         
        :country =>   params[:c_contact_detail]["#{i}"][:country],
        :telephone =>    params[:c_contact_detail]["#{i}"][:telephone] ,
        :address_type =>       params[:c_contact_detail]["#{i}"][:address_type],
        :consultant_id =>  params[:c_contact_detail]["#{i}"][:consultant_id]
        
        
          }
     
     end
     
     else
        
    
       refs << {
        :street_address => params[:c_contact_detail]["0"][:street_address],
        :state =>  params[:c_contact_detail]["0"][:state] ,
        :town =>      params[:c_contact_detail]["0"][:town],         
        :country =>   params[:c_contact_detail]["0"][:country],
        :telephone =>    params[:c_contact_detail]["0"][:telephone] ,
        :address_type =>       params[:c_contact_detail]["0"][:address_type],
        :consultant_id =>  params[:c_contact_detail]["0"][:consultant_id]
          }
       refs << {
        :street_address => params[:c_contact_detail]["0"][:street_address],
        :state =>  params[:c_contact_detail]["0"][:state] ,
        :town =>      params[:c_contact_detail]["0"][:town],         
        :country =>   params[:c_contact_detail]["0"][:country],
        :telephone =>    params[:c_contact_detail]["0"][:telephone] ,
        :address_type =>       'Present',
        :consultant_id =>  params[:c_contact_detail]["0"][:consultant_id]
          }
    
     end
    session["refs"] = refs
    #refs.delete_if {|key| key[:street_address].nil? || key[:street_address].empty? || key[:street_address].blank?}
   session["refs"] = refs
   
   if CContactDetail.create(refs)
     if( !@contacts_telephone[:home].empty? || !@contacts_telephone[:mobile].empty? || !@contacts_telephone[:work].empty? || !@contacts_telephone[:fax].empty?)
       Ctelephone.create(@contacts_telephone)
      end
      flash[:notice] = "Successfully saved contact details"
     
   else
     flash[:error] = "Unable to save contact details"
     
    end
     respond_to do |f|
       f.js
  end
  end
end
