class NgosController < ApplicationController
  
  before_filter :login_required , :only => ['new','index','create','destroy','edit','update','show']
  before_filter :ngos_only 
  before_filter :staff_only , :only => ['index']
 
  layout :get_user_layout
  def new
    if !identify_vendor.nil?
    @services = Vss.select("id , name").where({:vendor_type_id =>2,:vpa_id => current_user.vpa_id})
    else
      @ngo_ = Ngo.find(params[:id])
      session[:ngo_] =nil
      if @ngo_.id.nil?
        flash[:error] = "No record found!"
        verify_user_section
        return false
      end
      ngo_u = Undpuser.find(@ngo_.undpuser_id)
      @services = Vss.select("id , name").where({:vendor_type_id =>2,:vpa_id =>ngo_u.vpa_id })
    
    end
    @ngo_service = NgoService.find(:all,:conditions => {:ngo_id =>params[:id]})
    
    if !@ngo_service || @ngo_service.nil? || @ngo_service.empty?
    @ngo_service = NgoService.new
    end
    
    @nfinancial = NFinancial.find(:first ,:conditions => {:ngo_id => params[:id]})
  
    if !@nfinancial
      @nfinancial = NFinancial.new
    end
    
   

  @ndocs = NTechnicalDocument.find(:first , :conditions => {:ngo_id =>params[:id]})
  
  if !@ndocs
      @ndocs = NTechnicalDocument.new
  end
  @ncontract = NContract.find(:first , :conditions => {:ngo_id =>params[:id]})
  
  if !@ncontract
      @ncontract = NContract.new
  end
    
    @ncertification = NCertification.find(:first , :conditions => {:ngo_id =>params[:id]})
    if !@ncertification
      @ncertification = NCertification.new
  end
    
    @ngo = Ngo.find_by_id(params[:id],:include => [:ntelephone,:n_contact,:n_financial,:n_bank_info,:n_technical_document,:n_contract,:n_certification])
  end
  def create
  end

  def edit
  end

  def update
    if request.put?
      @ngo =  Ngo.find_by_id(params[:id])
      if params[:type_of_regn0] == "Others"
        params[:ngo][:registration_type] = params[:type_of_regn0]
        params[:ngo][:registration_type_other] = params[:type_of_regn1].upcase
      
      else
       params[:ngo][:registration_type] = params[:type_of_regn0]
     
      end
      @ngo.update_attributes(params[:ngo])
      
      if(params[:n_contact][:id]== "" || params[:n_contact][:id].nil?)
        
         c_hash = params[:n_contact]
         c_hash.delete(:id)
         session[:prams] =nil
        @ncontact = NContact.new(c_hash)
        #
        if !@ncontact.save
          flash[:error] = "unable to save Contact"
          session[:new_model] = @ncontact
          redirect_to new_ngo_url :id =>params[:id]
          return false
        end
     
     else
       
        @ncontact = NContact.find_by_id(params[:n_contact][:id])
          if !@ncontact.nil?
             @ncontact.update_attributes(params[:n_contact])
           end
     end
      
       if(params[:ntelephone][:id] == "" || params[:ntelephone][:id].to_i.nil?)
          c_hash = params[:ntelephone]
          c_hash.delete(:id)
      
         
         @ntelephone = Ntelephone.new(c_hash)
          if !@ntelephone.save
            flash[:error] = "unable to save Telephone details"
            session[:new_model] = @ntelephone
            redirect_to new_ngo_url :id =>params[:id]
            return false
        
          end
     else
        @ntelephone = Ntelephone.find_by_id(params[:ntelephone][:id])
          if !@ntelephone.nil?
            @ntelephone.update_attributes(params[:ntelephone])
          end
    end
    session[:new_model] = nil
    flash[:notice] = "Successfully saved details for NGO"
    respond_to do |format|
      format.html {
      if identify_vendor.nil?
        redirect_to :back
        return
      else
        
    redirect_to new_ngo_url(:id => params[:id])
                  return false
                  
     end
      }
    end
 end
 return false
end

  def destroy
  end

  def show
    
    if identify_vendor.nil?
     @ngo = Ngo.find_by_id(params[:id],:include => [:undpuser,:ngo_services,:ntelephone,:n_contact,:n_financial,:n_bank_info,:n_technical_document,:n_contract,:n_certification])
     
     if @ngo.nil? 
        flash[:error] = "no matching record found"
        verify_user_section
      end
   else
       @ngo = Ngo.find_by_id(identify_vendor,:include => [:undpuser,:ngo_services,:ntelephone,:n_contact,:n_financial,:n_bank_info,:n_technical_document,:n_contract,:n_certification])
 
        if @ngo.nil? 
        flash[:error] = "no match found"
        verify_user_section
       end
    end
  end

  def index
    if params[:sort].nil?
         sort = "name"
         
       else
         sort = params[:sort]
       end
      
       if params[:field] != "" && params[:searchString] !=""
       
    case params[:field]
      
    when "name"
      @title = "Showing search  result (s)"
    @ngos = Ngo.where('name ilike ?',"#{params[:searchString]}%").paginate(:page=>params[:page],:per_page=>40).order(sort.to_sym)
    return
     when "country"
       @title = "Showing search  result (s)"
    @ngos = Ngo.where(' country ilike ?',"#{params[:searchString]}%").paginate(:page=>params[:page],:per_page=>40).order(sort.to_sym)
    return
       end
    else
      flash[:error] = "No query given for search."
        @ngos=  Ngo.paginate(:page=>params[:page],:per_page=>40).order(sort.to_sym)
    return
    end
      
      
      
        @title = 'NGOs'  
      @ngos=  @consultants=  Ngo.paginate(:page=>params[:page],:per_page=>40).order(sort.to_sym)
    
     
     
  end
  
  
  def export_to_excel
   

if params[:ngos].nil? and !params[:noname]
     @clients = Ngo.find(:all,:include=>[:undpuser,:ntelephone,:n_contact], :conditions => "name  IS NOT NULL") 
  
elsif params[:noname] && !params[:ngos]
     ids = []
     @clients =  Ngo.find(:all, :conditions => "name  IS  NULL and undpuser_id IS NOT NULL",:include=>[:undpuser]) 
    
  elsif params[:ngos] == 'selected' && !session[:ngos].nil?
  @clients = Ngo.find(session[:ngos],:include=>[:undpuser,:ntelephone,:n_contact]) 
  # :conditions => "name  IS NOT NULL") 
   
   
     #@clients_id.each do |client|
      ## end
   #@clients = Ngo.find(ids,:include=>[:undpuser,:ntelephone,:n_contact]) 
 
   else
     flash[:error] = "No NGO selections detected"
     redirect_to :back
     return
  end

 



  require 'spreadsheet'
  respond_to do |format|
      format.pdf
      format.xls {
        clients = Spreadsheet::Workbook.new
        list = clients.create_worksheet :name => 'NGO Vendors'
        list.row(0).concat %w[Name Parent Email Telephone Street-Address  City Country Mailing-address  Date-of-Establishment Date-of-registration Working-Language(s) ]
        
          
#  street_address      :string(255)
#  postal_code         :string(255)
#  city                :string(255)
#  country             :string(255)
#  mailing_address     :text
#  parent_company      :text
#  subsidiaries        :string(255)
#  nature_of_business  :string(255)
#  type_of_business    :string(255)
#  doe                 :string(255)
#  number_of_employees :string(255)
#  license_number      :string(255)
#  state_where_regd    :string(255)
#  tax_number          :string(255)
#  dor                 :string(255)
#  working_language    :string(255)
#  state               :string(255)

        
        
        list.row(0).height = 30
        list.column(0).width = 60
        list.column(1).width = 50
        list.column(2).width = 50
        list.column(3).width = 50
       list.column(4).width = 50
        list.column(5).width = 50
        list.column(6).width = 50
       list.column(7).width = 50
        list.column(8).width = 50
        list.column(9).width = 50
       list.column(10).width = 50
        
         
        
  #[Name Parent Type-of-business Email Telephone Street-Address  City Country Mailing-address Nature-of-business Date-of-Establishment Date-of-Reg Working-Language ]
              
        @clients.each_with_index { |client, i|
          if !client.ntelephone.nil? 
            
          list.row(i+1).push(client.name, client.parent_ngo,
          client.undpuser.email,client.ntelephone.mobile + "\,"+ client.ntelephone.office,client.street_address,
          client.city,client.country,
          client.mailing_address ,client.doe, client.dor,client.working_languages
          
         
            
            )
 
    
       else
          list.row(i+1).push(client.name, client.parent_ngo,
          client.undpuser.email,"" ,client.street_address,
          client.city,client.country,
          client.mailing_address ,client.doe, client.dor,client.working_languages
            
          )
           
       end
        }
        
        header_format = Spreadsheet::Format.new :color => :blue, :weight => :bold ,:align =>:center
        list.row(0).default_format = header_format
        #output to blob object
        session[:ngos] = session[:user_ids] = nil
        blob = StringIO.new("")
        clients.write blob
        #respond with blob object as a file
        send_data blob.string, :type => :xls, :filename => "NGO_Vendors_exported_#{Time.now}.xls"
      }
end
    


  end
  
  def notifications
    
    @n_tech= NTechnicalDocument.where("financial_file_name is null OR job_evidence_file_name is null OR written_file_name is null")
     required_files = []
    @n_tech.each do |n|
     if n.financial_file_name.nil? || n.job_evidence_file_name.nil? || n.written_file_name.nil?
       required_files << n.ngo_id
    end
       
     end
  end
end
