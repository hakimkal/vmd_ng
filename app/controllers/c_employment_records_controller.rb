class CEmploymentRecordsController < ApplicationController
  
  before_filter :login_required , :only => ['index','new','create','update','edit','destroy','show']
  before_filter :consultants_only 
  before_filter :check_consultant  ,:except => ['create','add','remove']
  layout :get_user_layout
  
  def new
  end
  def add
    #SESSION tRACKING OF FORMS ADDED
    if session[:added_emp_field].nil? 
      session[:added_emp_field] = 1
    else
    session[:added_emp_field]+= 1
    end
    respond_to do |f|
      f.js
    end
  end
  
  def remove
    respond_to do |f|
      f.js
    end
  end
  def index
  end

  def create
  end

   

  def show
    
    if current_user.group_id == 4
      @consultant = Consultant.find_by_undpuser_id(current_user.id,:include => [:c_employment_detail,:c_employment_records])      
      if @consultant.blank? or @consultant.c_employment_detail.blank?
        flash[:error] = "no record found"
        verify_user_section
      end
     else
        @consultant = Consultant.find_by_id(params[:consultant_id],:include => [:c_employment_detail,:c_employment_records])
        if !@consultant || @consultant.nil?  || @consultant.c_employment_detail.nil?
        flash[:error] = "no record found"
        if request.env["HTTP_REFERER"].nil?
        redirect_to undpusers_url(:type => 'vendors')
        return false
        else
         redirect_to request.env["HTTP_REFERER"]
         return false
      end
     else 
       @user_title = "For #{@consultant.firstname}  #{@consultant.lastname} :: Consultant"
      end   
     
    end
  end

   def edit
    @consultant = CEmploymentDetail.find(:first,:conditions => {"consultant_id" =>  params[:consultant_id]})
    @consultant_records = CEmploymentRecord.find(:first,:conditions =>{:consultant_id =>params[:consultant_id],:id =>params[:id]})
   
     
    if !@consultant 
      flash[:error] = "invalid request"
      verify_user_section
    end
  end

  
  def destroy
    @consultant = CEmploymentRecord.find(:first, :conditions => {:id =>params[:id]})
    
    if @consultant.destroy
        CEmploymentRecord.calculate_work_experience(@consultant.consultant_id)
    
      flash[:notice] = "successfully deleted employment record item"
    else
      flash[:error] = "unable to delete"
    end
   
     respond_to do |f|
      f.html {  if identify_vendor.nil? 
                
                  if request.env["HTTP_REFERER"].nil?
                verify_user_section
                return false
                else
                 redirect_to request.env["HTTP_REFERER"]
                 return false
                 end
              else
                redirect_to c_employment_record_path(:id => @consultant.consultant_id ,:consultant_id => @consultant.consultant_id) 
               
              end
                 }
      f.json
      f.js
  end
  end



end
