class CtelephonesController < ApplicationController
  
  before_filter :login_required, :only=>['destroy','index','show','new','create','update']
  before_filter :consultants_only , :only=>['destroy','index','show','new','create','update']
   before_filter :check_consultant  ,:except => ['create','add','remove']
 
 
  
  layout :get_user_layout
  def new
  end

  def index
  end

  def edit
    
    @ctelephone = Ctelephone.find(:first , :conditions => {:consultant_id => params[:consultant_id],:id => params[:id]})
    if !@ctelephone
      flash[:error] = "invalid request"
      verify_user_section
    end
  end

  def update
    @consultant = Ctelephone.find(:first , :conditions => {:id => params[:id]})
    if !@consultant
      flash[:error] = "invalid request"
      verify_user_section
    end
    if @consultant.update_attributes params[:ctelephone]
      flash[:notice] = "Successfully updated telephone information"
     else
       flash[:error] = "unable to update telephone details, please try again later"
    end
    
    respond_to do  |f|
      f.html { if identify_vendor.nil?
                if request.env["HTTP_REFERER"].nil?
                verify_user_section
                return false
                else
                 redirect_to request.env["HTTP_REFERER"]
                 return false
                 end
               else
               verify_user_section
               end
                    }
      f.json
      f.js
    end
  end

  def destroy
    
     if  Ctelephone.find(:first , :conditions => {:id => params[:id]}).destroy
        flash[:notice] = "Successfully deleted telephone information"
     else
       flash[:error] = "unable to delete telephone details, please try again later"
   
     end
   respond_to do  |f|
      f.html { if identify_vendor.nil?
                if request.env["HTTP_REFERER"].nil?
                verify_user_section
                return false
                else
                 redirect_to request.env["HTTP_REFERER"]
                 return false
                 end
               else
               verify_user_section
               end
                    }
      f.json
      f.js
    end
  end

  def show
  end

  def create
  end
end
