class FundsController < ApplicationController
  
   before_filter :login_required , :only => ['index','new','create','update','edit','destroy','show']
  before_filter :staff_only 
  #before_filter :check_consultant  ,:except => ['create','add','remove']
  layout :get_user_layout
  def new
    @account = Fund.new
  end

  def create
    @account =  Fund.new params[:fund]
  #  @account.operating_unit = @account.operating_unit.upcase
    if  @account.financial_year_end.to_i -  @account.financial_year_start.to_i < 0
        
      flash[:error] = "The Contract Financial Start Year cannot   be EARLIER than the Financial End Year"
       session[:new_model] = @account
      render :new
      return
      end
    if @account.save
      session[:new_model] = nil
      flash[:notice] = "Successfully added a new account #{@account.account_number}"
      
    else
      flash[:error] = "Unable to create account"
      session[:new_model] = @account
      render :new
      return
    end
    redirect_to funds_url
    return false
    
  end

  def edit
    @account = Fund.find_by_id(params[:id])
  end

  def update
    @account = Fund.find_by_id(params[:id])
    @v = params[:fund]
   # @v[:operating_unit] = @v[:operating_unit].upcase
    if @account.update_attributes(@v)
      session[:new_model] = nil
      flash[:notice] = "Successfully updated funding account #  #{@account.account_number}"
    else
      flash[:error] = "Unable to update account"
      sesion[:new_model] = @account
      render :edit
      return false
    end
    redirect_to funds_url
    return false
  end

  def index
    session[:new_model] = nil
    @funds = Fund.includes(:agency_department).paginate(:page=>params[:page],:per_page =>params[:per_page])
  end

  def show
  end

  def destroy
    @fund = Fund.find_by_id(params[:id])
    
    if @fund.destroy
      flash[:notice] = "successfully deleted"
    else
      flash[:error] = "unable to delete"
    end
    redirect_to funds_url
    return
  end
end
