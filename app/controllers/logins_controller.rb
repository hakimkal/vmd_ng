class LoginsController < ApplicationController
  before_filter :login_required, :only=>['destroy','index','show','reset']
  before_filter :check_user_group  
  
  layout :get_user_layout
 
  def index
    @title = 'Audit Trail for Logins'
    #SELECT DISTINCT  fullname, user_id  , count(user_id)  from logins where user_id=user_id  group by fullname , user_id;
    @logins = Login.readonly.select("DISTINCT  fullname, user_id  , count(user_id) as times").where("user_id=user_id").group("fullname , user_id").paginate(:page=>params[:page],:per_page=>50)
   # @logins = @model = Login.paginate(:page=>params[:page],:per_page=>50).group("fullname,id")
  end

  def show
  end

  def view
    @title = 'Login Audit for '
    @logins = @model = Login.where("user_id" => params[:user_id]).order("created_at desc").paginate(:page=>params[:page],:per_page=>50)
     @logins.each do |k|
       @user = k.fullname
       @user_id = k.user_id
     end

  end
  
  def export_to_excel
    if params[:user_id].nil?
         
    @clients =Login.all
  require 'spreadsheet'
  respond_to do |format|
      format.pdf
      format.xls {
        clients = Spreadsheet::Workbook.new
        list = clients.create_worksheet :name => 'Login_Audit_Trail'
        list.row(0).concat %w{Name {Date-Time-In} {Date-Time-Out} {Session-Duration}}
        list.row(0).height = 30
        list.column(0).width = 60
        list.column(1).width = 60
        list.column(2).width = 30
        list.column(3).width = 30
       
        
        
        @clients.each_with_index { |client, i|
           list.row(i+1).push client.fullname,client.time_in,client.time_out , time_length(client.session_duration)
        }
        header_format = Spreadsheet::Format.new :color => :blue, :weight => :bold ,:align =>:center
        list.row(0).default_format = header_format
        #output to blob object
        blob = StringIO.new("")
        clients.write blob
        #respond with blob object as a file
        send_data blob.string, :type => :xls, :filename => "Login_Audit_Trail.xls"
      }
end
else
   user =Login.find(:first , :conditions => {"user_id"=> params[:user_id]})
  
   @clients =Login.find(:all , :conditions => {"user_id"=> params[:user_id]})
  require 'spreadsheet'
  respond_to do |format|
      format.pdf
      format.xls {
        clients = Spreadsheet::Workbook.new
        list = clients.create_worksheet :name => 'User_Login_Trail'
        list.row(0).push user.fullname
        list.row(1).concat %w{[Date-Time-In] [Date-Time-Out] [Session-Duration]}
        list.row(0).height = 30
         list.row(1).height = 30
       
        list.column(0).width = 60
        list.column(1).width = 60
        list.column(2).width = 30
        list.column(3).width = 30
       
        
        
        @clients.each_with_index { |client, i|
           list.row(i+2).push client.time_in,client.time_out , time_length(client.session_duration)
        }
         user_header_format = Spreadsheet::Format.new :color => :green, :weight => :bold ,:align =>:left
      
        header_format = Spreadsheet::Format.new :color => :blue, :weight => :bold ,:align =>:center
        list.row(0).default_format = user_header_format
        
        list.row(1).default_format = header_format
        #output to blob object
        blob = StringIO.new("")
        clients.write blob
        #respond with blob object as a file
        send_data blob.string, :type => :xls, :filename => "Login_Audit_Trail_for_#{user.fullname}.xls"
      }
end
   end 
     
  end
  
  def destroy
  end

  def reset
  end
end
