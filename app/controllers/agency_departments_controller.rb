class AgencyDepartmentsController < ApplicationController
  
  
  before_filter :login_required , :only => ['index','new','create','update','edit','destroy','show']
  before_filter :staff_only 
  #before_filter :check_consultant  ,:except => ['create','add','remove']
  layout :get_user_layout
  
  def new
    @agency =AgencyDepartment.new
  end

  def create
    @agency = AgencyDepartment.new params[:agency_department]
    
    if @agency.save
      session[:new_model] = nil
      flash[:notice] = "Successfully Added!"
     
     else
       
       flash[:error] = "Unable to save!"
       session[:new_model] = @agency
       render :new
       return false
       
    end
    
    redirect_to agency_departments_url
    return false
  end

  def edit
     @agency = AgencyDepartment.find_by_id(params[:id])
     
     if !@agency
       flash[:error] = "Entry not found"
        redirect_to agency_departments_url
        return false
       
     end
  end

  def update
    @agency = AgencyDepartment.find_by_id(params[:id])
    
     if @agency.update_attributes(params[:agency_department])
      session[:new_model] = nil
      flash[:notice] = "Successfully Added!"
     
     else
       
       flash[:error] = "Unable to save!"
       session[:new_model] = @agency
       render :new
       return false
       
    end
     redirect_to agency_departments_url
        return false
  end

  def index
    @agency_departments = AgencyDepartment.paginate(:page=>params[:page],:per_page =>50)
  end

  def show
     redirect_to agency_departments_url
        return false
  end

  def destroy
    @agency = AgencyDepartment.find_by_id(params[:id])
     
     if @agency.destroy
    
      flash[:notice] = "Successfully Deleted!"
     
     else
       
       flash[:error] = "Unable to delete!"
        
        
       
    end
     redirect_to agency_departments_url
        return false
  end
end
