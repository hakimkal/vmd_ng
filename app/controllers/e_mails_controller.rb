class EMailsController < ApplicationController
  
  before_filter :login_required , :only => ['index','new','create','update','edit','destroy','show']
  before_filter :staff_only ,:except => ['feedback','create']
  #before_filter :check_consultant  ,:except => ['create','add','remove']
  
  layout 'application' , :only => ['feedback']
  layout :get_user_layout ,:except => ['feedback']
  def new
    sent_to_id = ""
    if request.post?
    if params[:user_id] != 'group'
    to = Undpuser.find(params[:user_id])
    @to = "#{to.firstname}  #{to.lastname}< #{to.email}>"
    sent_to_id += to.id.to_s + ","
     
    end
    @mail = EMail.new
    @mail.sent_to = @to
    @mail.sent_to_ids = sent_to_id
    
     elsif request.get? 
       if session[:user_ids].nil?
         flash[:error] = "Sorry, No selections detected"
         redirect_to :back
         return
       end
     @to = ""
    to =  Undpuser.find(session[:user_ids])
    to.each do |t|
      @to += t.email + ","
      sent_to_id += t.id.to_s + ","
      session[:user_ids] = nil
    end
     
    @mail = EMail.new
    @mail.sent_to = @to
    @mail.sent_to_ids = sent_to_id
     
  
      #flash[:error] = "Please select user(s)"
       
      
    end
  end

  def index
    generate_password
    @emails = EMail.order("created_at desc").paginate(:per_page => 40, :page=>params[:page])
  end


  def show
    @email = EMail.find_by_id(params[:id])
   
    if @email.nil?
      flash[:error] = "No mail found"
      redirect_to :back
    end
  end
  
  def create
    
    @mail = EMail.new(params[:e_mail])
    
    if @mail.save
      if !params[:e_mail][:feedback] 
      if MailMailer.send_email(params[:e_mail]).deliver
        flash[:notice] = "Email successfully sent to the recipient(s)."
        verify_user_section
        return
      else
        flash[:error] = "unable to send email."
        verify_user_section
        return
      end
     else
        if MailMailer.send_feedback(params[:e_mail]).deliver
        flash[:notice] = "Email successfully sent to the recipient(s)."
        verify_user_section
        return
      else
        flash[:error] = "unable to send email."
        verify_user_section
        return
      end
      end
    end
  end

  def edit
  end

  def destroy
    if EMail.find_by_id(params[:id]).destroy
      flash[:notice] = "successfully deleted"
      redirect_to e_mails_url
      return
    end
  end

  def view
    @email = EMail.find(params[:id])
  end


  def feedback
        @email = EMail.new
  end
  def update
  end
  
  def send_auto_pass
    
  #NotificationMailer.vendor_auto_password_link(113).deliver
  
   
  
 #if NotificationMailer.vendor_auto_password_link("1056,").deliver
  users = Undpuser.select("id,firstname,uni_id,lastname,encrypted_password,email,vendor_id").where("encrypted_password IS NULL and group_id = 4 and vendor_id = 1 or vendor_id = 3")
       users.each do |u|  
       msg = {
         "subject" => 'UNDP Nigeria Vendor Database Password Reset Notice',
         "id" => u.id,
         "firstname" =>u.firstname,
         "lastname" => u.lastname,
         "uni_id" => u.uni_id,
         "encrypted_password" => u.encrypted_password,
         "email" => u.email,
         "vendor_id" => u.vendor_id
         }
       NotificationMailer.send_auto_email(u,msg).deliver
       
       end
   
  flash[:notice] = "Successfully sent email"
    
  #else
   # flash[:error] = "failed to send email"
   #end 
     
  end
end
