class SupservicesController < ApplicationController
  
   before_filter :login_required, :only=>['destroy','index','show','new']
  before_filter :check_user_group , :only=>['destroy','index','show','new']
  
  layout :get_user_layout
 
  def index
    @supservices = @vpas= Supservice.paginate(:page=>params[:page], :per_page => 40)
  end
   

  def new
  end

  def create
    
     if request.post?
       @conservice = Supservice.new(params[:supservice])
       respond_to do |format|
      if @conservice.save
        @content = "Successfully created service"
        format.js 
     else
     @content = "Failed to create service, Duplicates are not allowed"
      format.js 
      end
    
  end
end
  end

  def destroy
    if Supservice.find(params[:id]).destroy
      flash[:notice] = 'successfuly deleted supplier service'
    else
      flash[:error] = 'Unable to delete supplier service'
    end
    redirect_to supservices_url
  end

  def edit
    @supservice = @vpa = Supservice.find_by_id(params[:id])
    
    if @supservice.nil?
      flash[:error] = 'Invalid service'
      redirect_to supservices_url
    end 
  end

  def show
  end

  def update
    @supservice = Supservice.find_by_id(params[:id])
    if @supservice.update_attributes params[:supservice]
      flash[:notice] = 'successfully update supplier service'
    else
      flash[:error] = ' Unable to update supplier service'
    end
    redirect_to supservices_url
  end
end
