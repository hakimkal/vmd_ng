class VssesController < ApplicationController
  before_filter :login_required , :only => ['index','create','update','edit','destroy']
  before_filter :check_user_group ,  :except => [:listed]
  layout :get_user_layout ,  :except => [:listed]

  respond_to :json , :only => [:listed]
  def new
  end

  def create
    if request.post?
      vendor_specialization = []
      params[:vss][:name].each do |vss_name|
      vendor_specialization << {:name =>vss_name, :vendor_type_id => params[:vss][:vendor_type_id] , :vpa_id => params[:vss][:vpa_id] }
       
      end
      #session[:v_s] =  vendor_specialization.size 
      
      vendor_specialization.delete_if {|vs|  Vss.count(:conditions => vs) > 0 }   #=> ["a"]
        
       if vendor_specialization.size <= 0
        
        flash[:error]  = "Duplication Error: you attempted to create already existing special area(s). "
         
        
      elsif  Vss.create(vendor_specialization)
        flash[:notice] = "Successfully added  specialization area(s)"
      redirect_to new_vendor_type_url   
         return false
       else
         flash[:error]  = "unable to add specialization area..."
          redirect_to new_vendor_type_url   
         return false
      end
        # redirect_to new_vendor_type_url   
         #return false
    # debug vs
      
    end
  end
  
  def listed
    
    if request.get?
    list=  Vss.select("id , name").where("vendor_type_id" => params[:vendor_type_id] , "vpa_id" => params[:vpa_id]).order(:name)
    respond_with list
    end
  end
  def index
    @title = 'Special Area [Thematic]'
    @vsses = @vpas =Vss.paginate(:page=>params[:page],:per_page=>50)
  end

  def edit
    @vendors  = VendorType.select("id , name")
     @vss=@vpa = Vss.find_by_id(params[:id])
    @vpas = Vpa.select("id , name")
  
   redirect_to request.env['HTTP_REFERER'] if @vss.nil?
  end

  def update
    if request.put?
      @vss = Vss.find_by_id(params[:id])
      if @vss && @vss.update_attributes(params[:vss])
        flash[:notice] = "Successfully updated Special Area: #{params[:vss][:name]}"
       
       else
         flash[:error] = "Failed to update Special Area: #{params[:vss][:name]} , Try again later."
         
    end
    redirect_to vsses_url
  end
  end

  def show
  end

  def destroy
    if Vss.find_by_id(params[:id]).destroy
      flash[:notice] = "Successfully deleted special area"
      
    else
      flash[:error] = "Unable to delete special area"
    
    end
     redirect_to :back
     return
  end
end
