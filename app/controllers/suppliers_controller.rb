class SuppliersController < ApplicationController
  
  
  before_filter :login_required, :only=>['destroy','edit','new','index','show','update','destroy']
  before_filter :suppliers_only
  before_filter :staff_only , :only => ['index']
  layout :get_user_layout 
    
  def new
    @services = Supservice.select("id , name")
    session[:services] = nil
    
    @supplier_services = @ngo_service = Supservtype.find(:all,:conditions => {:supplier_id =>params[:id]})
    
    if !@supplier_services || @supplier_services.nil? || @supplier_services.empty?
    @supplier_services   = Supservtype.new
  
    end
    
    @sfinancial =@nfinancial =  SFinancial.find(:first,:conditions => {:supplier_id =>params[:id]})
    
    if !@sfinancial || @sfinancial.nil? 
   @sfinancial =@nfinancial =  SFinancial.new
   
    end
    
     @sdocs =@ndocs =  STechnicalDocument.find(:first,:conditions => {:supplier_id =>params[:id]})
    
    if !@sdocs || @sdocs.nil? 
   @sdocs =@ndocs =  STechnicalDocument.new
   
    end
    
     @scontract =@ncontract =  SContract.find(:first,:conditions => {:supplier_id =>params[:id]})
    
    if !@scontract || @scontract.nil? 
   @scontract =@ncontract =  SContract.new
   
    end
    
     @scertification =@ncertification =  SCertification.find(:first,:conditions => {:supplier_id =>params[:id]})
    
    if !@scertification || @scertification.nil? 
   @scertification =@ncertification =  SCertification.new
   
    end
    
    
   @supplier=  @ngo = Supplier.find_by_id(params[:id],:include => [:stelephone,:s_contact,:s_financial,:s_bank_info,:s_technical_document,:s_contract,:s_certification])
  
    
  end

  def create
  end

  def index
    
     if params[:sort].nil?
         sort = "name"
         
       else
         sort = params[:sort]
       end
    
     if params[:field] != "" && params[:searchString] !=""
       
    case params[:field]
      
    when "name"
      @title = "Showing search  result (s)"
    @suppliers = Supplier.where('name ilike ?',"#{params[:searchString]}%").paginate(:page=>params[:page],:per_page=>40).order(sort.to_sym)
    return
     when "country"
       @title = "Showing search  result (s)"
    @suppliers = Supplier.where(' country ilike ?',"#{params[:searchString]}%").paginate(:page=>params[:page],:per_page=>40).order(sort.to_sym)
    return
       end
    else
      flash[:error] = "No query given for search."
        @suppliers=  Supplier.paginate(:page=>params[:page],:per_page=>40).order(sort.to_sym)
    return
    end
      
      @title = 'Suppliers'  
      @suppliers=  @consultants=  Supplier.paginate(:page=>params[:page],:per_page=>40).order(sort.to_sym)
    
    
    
  end

  def edit
  end

     
    
    def update
    if request.put?
        @ngo =  Supplier.find_by_id(params[:id])
        
        @ngo.update_attributes(params[:supplier])
        
        if(params[:s_contact][:id]== "" || params[:s_contact][:id].nil?)
          
           c_hash = params[:s_contact]
           c_hash.delete(:id)
           session[:prams] =nil
          @ncontact = SContact.new(c_hash)
          #
          if !@ncontact.save
            flash[:error] = "unable to save Contact"
            session[:new_model] = @ncontact
            redirect_to new_supplier_url :id =>params[:id]
            return false
          end
       
       else
         
          @ncontact = SContact.find_by_id(params[:s_contact][:id])
            if !@ncontact.nil?
               @ncontact.update_attributes(params[:s_contact])
             end
       end
        
         if(params[:stelephone][:id] == "" || params[:stelephone][:id].to_i.nil?)
            c_hash = params[:stelephone]
            c_hash.delete(:id)
        
           
           @ntelephone = Stelephone.new(c_hash)
            if !@ntelephone.save
              flash[:error] = "unable to save Telephone details"
              session[:new_model] = @ntelephone
              redirect_to new_supplier_url :id =>params[:id]
              return false
          
            end
       else
          @ntelephone = Stelephone.find_by_id(params[:stelephone][:id])
            if !@ntelephone.nil?
              @ntelephone.update_attributes(params[:stelephone])
            end
      end
    session[:new_model] = nil
    flash[:notice] = "Successfully saved details for Supplier"
    respond_to do |format|
      format.html{
                    if identify_vendor.nil?
                     redirect_to :back 
                      return false
                    else
                      
                        redirect_to new_supplier_url(:id => params[:id])
                                return false
                                
                     end
      }
    end
 end
 return false
  
end
  def show
    
    if identify_vendor.nil?
   @supplier=  @ngo = Supplier.find_by_id(params[:id],:include => [:stelephone,:s_contact,:s_financial,:s_bank_info,:s_technical_document,:s_contract,:supservtypes,:s_certification])
     
      if @ngo.nil? || !@ngo
        flash[:error] = "Invalid supplier"
        verify_user_section
      end
   else
       @supplier =  @ngo = Supplier.find_by_id(identify_vendor,:include => [:stelephone,:s_contact,:s_financial,:s_bank_info,:supservtypes,:s_technical_document,:s_contract,:s_certification])
  
        if @ngo.nil? || !@ngo
        flash[:error] = "Invalid supplier"
        verify_user_section
       end
    end
  end


  def destroy
  end
  
  def export_to_excel
   

if params[:suppliers].nil?
     @clients = Supplier.find(:all,:include=>[:undpuser,:stelephone,:s_contact]) 
   
  elsif params[:suppliers] == 'selected' && !session[:suppliers].nil?
  @clients = Supplier.find(session[:suppliers],:include=>[:undpuser,:stelephone,:s_contact]) 
   
   else
     flash[:error] = "No suppliers selections detected"
     redirect_to :back
     return
  end

 



  require 'spreadsheet'
  respond_to do |format|
      format.pdf
      format.xls {
        clients = Spreadsheet::Workbook.new
        list = clients.create_worksheet :name => 'Supplier Vendors'
        list.row(0).concat %w[Name Parent Type-of-business Email Telephone Street-Address  City Country Mailing-address Nature-of-business Date-of-Establishment Date-of-registration Working-Language ]
        
          
 
        
        list.row(0).height = 30
        list.column(0).width = 60
        list.column(1).width = 50
        list.column(2).width = 50
        list.column(3).width = 50
       list.column(4).width = 50
        list.column(5).width = 50
        list.column(6).width = 50
       list.column(7).width = 50
        list.column(8).width = 50
        list.column(9).width = 50
       list.column(10).width = 50
        list.column(11).width = 50
         
        
                
        @clients.each_with_index { |client, i|
          if !client.stelephone.nil? 
            
          list.row(i+1).push(client.name, client.parent_company,
          client.type_of_business,client.undpuser.email,client.stelephone.mobile + "\,"+ client.stelephone.office,client.street_address,
          client.city,client.country,
          client.mailing_address,client.nature_of_business ,client.doe, client.dor,client.working_language
          
         
            
            )
 
    
       else
          list.row(i+1).push(client.name, client.parent_company,
          client.type_of_business,client.undpuser.email,"" ,client.street_address,
          client.city,client.country,
          client.mailing_address,client.nature_of_business ,client.doe, client.dor,client.working_language
            
          )
           
       end
        }
        
        header_format = Spreadsheet::Format.new :color => :blue, :weight => :bold ,:align =>:center
        list.row(0).default_format = header_format
        #output to blob object
        session[:suppliers] = session[:user_ids] = nil
        blob = StringIO.new("")
        clients.write blob
        #respond with blob object as a file
        send_data blob.string, :type => :xls, :filename => "Supplier_Vendors_exported_#{Time.now}.xls"
      }
end
    


  end
end
