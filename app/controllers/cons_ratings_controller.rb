class ConsRatingsController < ApplicationController
   before_filter :login_required , :only => ['index','new','select_cons','create','update','edit','destroy','show']
  before_filter :staff_only 
  #before_filter :check_consultant  ,:except => ['create','add','remove']
  layout :get_user_layout

  def new
  end

  def create
  end

  def edit
  end

  def update
  end

  def index
    if params[:name].blank?
    @ratings = @cons_ratings= ConsRating.paginate(:per_page=>50 , :page=>params[:page])
    
    else
    @ratings = @cons_ratings= ConsRating.where("consultant_name ilike ?",params[:name]+"%").paginate(:per_page=>50 , :page=>params[:page])
     
    end
  end

  def show
    @rating =  ConsRating.find_by_id(params[:id])
  end

  def destroy
  end
end
