class ConsultantReportsController < ApplicationController
  before_filter :login_required , :only => ['index','new','create','update','edit','destroy','show']
  before_filter :staff_only 
  #before_filter :check_consultant  ,:except => ['create','add','remove']
  layout :get_user_layout

  def index
    if request.post?
    @consultant_report = ConsultantReport.new
   @c_eds =  ConsultantReport.select("distinct on(qualification,c_post_qual) qualification as q1 ,c_post_qual as q2 ")
   @c_services =  ConsultantReport.select("distinct on(vss_name,service) vss_name ,service ")
    c_ser = []
     c_eds  = []
     @c_eds.each do |c|
      c_eds << c.q1
      c_eds << c.q2
       
     end 
     @c_eds = c_eds.uniq
    @c_services.each do |s|
      
      c_ser <<  s.vss_name 
      c_ser << s.service
    end
   @c_services = c_ser.uniq
   @c_eds.delete_if{|c| c.blank?}
   @c_services.delete_if{|s| s.blank?}
   
    
      s = params[:consultant_report]
      
      #none selected
      if s[:experience] == '-1' &&  s[:c_age] == '-1' && s[:qualification] == '' &&  s[:gender] == '-1' &&  s[:vss_name] == ''
       
      @consultant_reports = ConsultantReport.select("Distinct on (c_fname,c_lname)  *").paginate(:page=>params[:page],:per_page => 50)
    return false
    #experience only selected
    elsif s[:experience] != '-1' &&  s[:c_age] == '-1' && s[:qualification] == '' &&  s[:gender] == '-1' &&  s[:vss_name] == ''
       
      @consultant_reports = ConsultantReport.select("Distinct on (c_fname,c_lname)  *").where("experience between ? and ? and experience is not null",experience(s[:experience])[0],experience(s[:experience])[1]).paginate(:page=>params[:page],:per_page => 50)
    return false
    
    
    #age only selected
    elsif s[:experience] == '-1'and  s[:c_age] != '-1' and s[:qualification] == '' and  s[:gender] == '-1' and  s[:vss_name] == ''
      
      @consultant_reports = ConsultantReport.select("Distinct on (c_fname,c_lname)  *").where("c_age between ? and ? and c_age is not null",get_age(s[:c_age])[0],get_age(s[:c_age])[1]).paginate(:page=>params[:page],:per_page => 50)
    return false
    
    #education only
    elsif s[:experience] == '-1' &&  s[:c_age] == '-1' && s[:qualification] != '' &&  s[:gender] == '-1' &&  s[:vss_name] == ''
        
      @consultant_reports = ConsultantReport.select("Distinct on (c_fname,c_lname)  *").where("qualification ilike ? or c_post_qual ilike ?",s[:qualification],s[:qualification]).paginate(:page=>params[:page],:per_page => 50)
  return false
  
  #gender only
    elsif s[:experience] == '-1' &&  s[:c_age] == '-1' && s[:qualification] == '' &&  s[:gender] != '-1' &&  s[:vss_name] == ''
        
      @consultant_reports = ConsultantReport.select("Distinct on (c_fname,c_lname)  *").where("gender iLike ? ",s[:gender]+"%").paginate(:page=>params[:page],:per_page => 50)
    return false
    #service name
    elsif s[:experience] == '-1' &&  s[:c_age] == '-1' && s[:qualification] == '' &&  s[:gender] == '-1' &&  s[:vss_name] != ''
        
      @consultant_reports = ConsultantReport.select("Distinct on (c_fname,c_lname)  *").where("vss_name ilike ? or service ilike ?",s[:vss_name],s[:vss_name]).paginate(:page=>params[:page],:per_page => 50)
 
 return false
 # age and experience only
 
  
  elsif s[:experience] != '-1' &&  s[:c_age] != '-1' && s[:qualification] == '' &&  s[:gender] == '-1' &&  s[:vss_name] == ''
      
        
   @consultant_reports = ConsultantReport.select("Distinct on (c_fname,c_lname)  *").where("experience between ? and ? and c_age between ? and ?",experience(s[:experience])[0],experience(s[:experience])[1],
    get_age(s[:c_age])[0],get_age(s[:c_age])[1])
    .paginate(:page=>params[:page],:per_page => 50)
   
   return false
   #experience and education
    
  elsif s[:experience] != '-1' &&  s[:c_age] == '-1' && s[:qualification] != '' &&  s[:gender] == '-1' &&  s[:vss_name] == ''
    
     exp = experience(s[:experience])
          
    @consultant_reports = ConsultantReport.select("Distinct on (c_fname,c_lname)  *").where("experience between ? and ? and qualification ilike ? or c_post_qual ilike ?",exp[0],exp[1],s[:qualification],s[:qualification]).paginate(:page=>params[:page],:per_page => 50)
   
    return false
   #experience and service
   
     
  elsif s[:experience] != '-1' &&  s[:c_age] == '-1' && s[:qualification] == '' &&  s[:gender] == '-1' &&  s[:vss_name] != ''
      exp = experience(s[:experience])
        
        
     
        
     
        
    
       @consultant_reports = ConsultantReport.select("Distinct on (c_fname,c_lname)  *").where("experience between ? and ? and vss_name ilike ? or service ilike ?",exp[0],exp[1],s[:vss_name],s[:vss_name]).paginate(:page=>params[:page],:per_page => 50)
     return false
   
 #if gender is left out and other options  selected
 
 elsif s[:experience] != '-1' &&  s[:c_age] != '-1' && s[:qualification] != '' &&  s[:gender] == '-1' &&  s[:vss_name] != ''
  
  exp = experience(s[:experience])
        
        
     age = get_age(s[:c_age])
        
     
       @consultant_reports = ConsultantReport.select("Distinct on (c_fname,c_lname)  *").where("experience between ? and ? and c_age between ? and ? and qualification ilike ? or c_post_qual ilike ?   and vss_name ilike ? or service ilike ?",exp[0],exp[1],age[0],age[1],s[:qualification],s[:qualification],s[:vss_name],s[:vss_name]).paginate(:page=>params[:page],:per_page => 50)
      return false
    
 #all 5 selected
  
  elsif s[:experience] != '-1' &&  s[:c_age] != '-1' && s[:qualification] != '' &&  s[:gender] != '-1' &&  s[:vss_name] != ''
     exp = experience(s[:experience])
        
        
     age = get_age(s[:c_age])
        
       
       @consultant_reports = ConsultantReport.select("Distinct on (c_fname,c_lname)  *").where("experience between ? and ? and c_age between ? and ? and qualification ilike ? or c_post_qual ilike ? and gender iLike ?  and vss_name ilike ? or service ilike ?",exp[0],exp[1],age[0],age[1],s[:qualification],s[:qualification],"#{s[:gender]}%",s[:vss_name],s[:vss_name]).paginate(:page=>params[:page],:per_page => 50)
      return false
    
    
    #when it falls outside the expected search
    
    
    else
      flash[:error] = "Your search query was complex, hence system generated the default"
      @consultant_reports = ConsultantReport.select("Distinct on (c_fname,c_lname)  *").paginate(:page=>params[:page],:per_page => 50)
   
    return false
    end
    end
    
    if request.get?
    @consultant_report = ConsultantReport.new
    @consultant_reports = ConsultantReport.select("Distinct on (c_fname,c_lname)  *").paginate(:page=>params[:page],:per_page => 50)
    @c_eds =  ConsultantReport.select("distinct on(qualification,c_post_qual) qualification as q1 ,c_post_qual as q2 ")
   @c_services =  ConsultantReport.select("distinct on(vss_name,service) vss_name ,service ")
    c_ser = []
     c_eds  = []
     @c_eds.each do |c|
       
        
      c_eds << c.q1.strip.upcase unless c.q1.nil?
      c_eds << c.q2.strip.upcase unless c.q2.nil?
     
     end 
     @c_eds = c_eds.uniq
    @c_services.each do |s|
      
   c_ser <<  s.vss_name.strip.upcase  unless s.vss_name.nil?
      c_ser << s.service.strip.upcase unless s.service.nil?
    end
   @c_services = c_ser.uniq
   @c_eds.delete_if{|c| c.blank?}
   @c_services.delete_if{|s| s.blank?}
  end
 end
  def create
    
  end
  def show
  end
  
  private 
  
  def experience(value = nil)
    if value.nil?
      
    value = '0__5'
   
    end
    e1 = 0.0
    e2 = 0.0
    case value
       
       when '0__5'
        e1= 0.0
        e2= 5.0*365.0  
         
        when '5__10'
         e1= 5.0*365.0 
         e2= 10.0*365.0  
         
         when '10__15'
         e1= 10.0*365.0 
         e2= 15.0*365.0  
         
          when '15__20'
         e1= 15.0*365.0 
         e2= 20.0*365.0  
        
       end
       result = [e1,e2]
       return result
  end
  
  def get_age(value=nil)
   a1 = 0
   a2 = 0
    if value.nil?
      a1 = 30
      a2 = 40
    end
     case value
       when '21__30'
        a1= 21 * 365
        a2= 30 * 365 
         
        when '30__40'
         a1= 30 * 365
         a2= 40 * 365  
         
         when '40__50'
         a1= 40*365 
         a2= 50*365  
         
          when '50__100'
         a1= 50*365 
         a2= 100*365  
        
       end
    return [a1,a2]
  end
end
