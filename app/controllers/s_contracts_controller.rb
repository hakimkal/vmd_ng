class SContractsController < ApplicationController
  
   before_filter :login_required, :only=>['destroy','edit','create','index','show','update','destroy']
   before_filter :suppliers_only
  
  layout :get_user_layout 
  
 def update
    if request.put?
      @nfinancial = SContract.find_by_id(params[:id])
      if @nfinancial.update_attributes(params[:s_contract])
        flash[:notice] = "successfully updated contract details"
      else
        flash[:error] = "unable to update contract details"
      end
      
       
    
   respond_to do |format|
     if identify_vendor.nil?
        
      else
        
      format.html {    redirect_to new_supplier_url :id=> params[:s_contract][:supplier_id]  
        
                  return false
                  }
     end
   end
    end   
  end

  def create
    if request.post? 
      @nfinancial = SContract.new(params[:s_contract])
      if @nfinancial.save
        session[:new_model] = nil
        flash[:notice] = "successfully saved contract details"
        
      else
        flash[:error] = "unable to save contract details"
        session[:new_model] = @nfinancial
          redirect_to new_supplier_url :id=> params[:s_contract][:supplier_id]  
          return false
        
      end
      
       
    respond_to do |format|
      if identify_vendor.nil?
        
      else
        
      format.html {    redirect_to new_supplier_url :id=> params[:s_contract][:supplier_id]        
                  return false
                  }
     end
   end
    end
   return false 
  end


  def destroy
    
    @nfinancial = SContract.find_by_id(params[:id])
      if @nfinancial.destroy
        flash[:notice] = "successfully deleted contract details"
      else
        flash[:error] = "unable to delete contract details"
      end
      
       respond_to do |format|
      if identify_vendor.nil?
        
      else
        
      format.html {    redirect_to new_supplier_url :id=>[:supplier_id]  
         
                  return false
                  }
     end
   end
  end
 
 
 
  def new
  end

   
  def edit
  end

  def index
  end

  def show
  end

   
end
