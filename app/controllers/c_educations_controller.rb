class CEducationsController < ApplicationController
  before_filter :login_required , :only => ['index','new','create','update','edit','destroy','show']
  before_filter :consultants_only 
  before_filter :check_consultant  ,:except => ['create','add','remove']
  layout :get_user_layout
  
  def add
    #SESSION tRACKING OF FORMS ADDED
    if session[:added_edu_field].nil? 
      session[:added_edu_field] = 1
    else
    session[:added_edu_field]+= 1
    end
    respond_to do |f|
      f.js
    end
  end
  
  def remove
    respond_to do |f|
      f.js
    end
  end
 
  def new
  end

  def index
  end

  def create
    
 if request.post?
   cEducations = []
   params[:c_educations].each do |ce|
   cEducations <<   {
                    :attended_from => ce[:attended_from]["(1i)"] +"-" + ce[:attended_from]["(2i)"] +"-"+ ce[:attended_from]["(3i)"],
                    :attended_to  => ce[:attended_to]["(1i)"] +"-" + ce[:attended_to]["(2i)"] +"-"+ ce[:attended_to]["(3i)"] ,
                    :consultant_id => "#{ce[:consultant_id]}" ,
                    :country => "#{ce[:country]}" ,
                    :course_of_study => "#{ce[:course_of_study]}" ,
                    :institution => "#{ce[:institution]}" ,
                    :place => "#{ce[:place]}" ,
                    :qualification => "#{ce[:qualification]}" ,
                 
                    :study_type => "#{ce[:study_type]}"
                    }
                    end
   session[:c_educations] = nil
   session[:c_educations] = cEducations
   
   c_postqualifications = []

   params[:c_post_qualification].each do |ce|
   c_postqualifications <<   {
                    :attended_from => ce[:attended_from]["(1i)"] +"-" + ce[:attended_from]["(2i)"] +"-"+ ce[:attended_from]["(3i)"],
                    :attended_to  => ce[:attended_to]["(1i)"] +"-" + ce[:attended_to]["(2i)"] +"-"+ ce[:attended_to]["(3i)"] ,
                    :consultant_id => "#{ce[:consultant_id]}" ,
                    :country => "#{ce[:country]}" ,
                    :course_of_study => "#{ce[:course_of_study]}" ,
                    :institution => "#{ce[:institution]}" ,
                    :place => "#{ce[:place]}" ,
                    :qualification => "#{ce[:qualification]}" ,
                 
                    :study_type => "#{ce[:study_type]}"
                    }
          end 
   session[:c_post_qualification] = nil
   session[:c_post_qualification] = c_postqualifications
 

   c_pro_socs = []
   params[:c_pro_society].each do |cpr|
   
          c_pro_socs <<    {
                :consultant_id => "#{cpr[:consultant_id]}" ,
                :details => "#{cpr[:details]}" ,
                :name => "#{cpr[:name]}"
              }
            end
 session[:c_pro_society] = nil
 session[:c_pro_society] = c_pro_socs
 
 
   c_pubs = []
   params[:c_publication].each do |cpr|
   
          c_pubs <<    {
                :consultant_id => "#{cpr[:consultant_id]}" ,
                :details => "#{cpr[:details]}" ,
                :name => "#{cpr[:name]}"
              }
          end
   session[:c_publication] = nil
   session[:c_publication] = c_pubs
 
 

   c_un_lang = []
   params[:cun_language].each do |ce|
  
   c_un_lang << {
                    :attended_from => ce[:attended_from]["(1i)"] +"-" + ce[:attended_from]["(2i)"] +"-"+ ce[:attended_from]["(3i)"],
                    :attended_to  => ce[:attended_to]["(1i)"] +"-" + ce[:attended_to]["(2i)"] +"-"+ ce[:attended_to]["(3i)"] ,
                    :consultant_id => "#{ce[:consultant_id]}" ,
                    :country => "#{ce[:country]}" ,
                    :course_of_study => "#{ce[:course_of_study]}" ,
                    :institution => "#{ce[:institution]}" ,
                    :place => "#{ce[:place]}" ,
                    :qualification => "#{ce[:qualification]}" ,
                 
                    :study_type => "#{ce[:study_type]}"
                    }
  
                end

   session[:cun_language] = nil 
   session[:cun_language] = c_un_lang


 
   cundp_certifications = []
   params[:cundp_certificate].each do |ce|
    
    cundp_certifications << {
                    :attended_from => ce[:attended_from]["(1i)"] +"-" + ce[:attended_from]["(2i)"] +"-"+ ce[:attended_from]["(3i)"],
                    :attended_to  => ce[:attended_to]["(1i)"] +"-" + ce[:attended_to]["(2i)"] +"-"+ ce[:attended_to]["(3i)"] ,
                    :consultant_id => "#{ce[:consultant_id]}" ,
                    :country => "#{ce[:country]}" ,
                    :course_of_study => "#{ce[:course_of_study]}" ,
                    :institution => "#{ce[:institution]}" ,
                    :place => "#{ce[:place]}" ,
                    :qualification => "#{ce[:qualification]}" ,
                 
                    :study_type => "#{ce[:study_type]}"
                    }
            end

   session[:cundp_certificate] = nil
   session[:cundp_certificate] = cundp_certifications

    
    #My Database preparation
   #cEducations
   
  
  cEducations.delete_if {|c| CEducation.count(:conditions=>c) > 0}
  cEducations.delete_if {|c| c[:institution].nil? || c[:institution] == "" }
  
   if cEducations.size > 0
     CEducation.create(cEducations)
   
   end    
  
   
   #c_postqualifications
   c_postqualifications.delete_if {|c| CPostQualification.count(:conditions=>c) > 0}
  c_postqualifications.delete_if {|c| c[:institution].nil? || c[:institution] == ""}
  
   if c_postqualifications.size > 0
     CPostQualification.create(c_postqualifications)
   
   end 
    
    #c_pro_socs
    c_pro_socs.delete_if {|c| CProSociety.count(:conditions=>c) > 0}
  c_pro_socs.delete_if {|c| c[:name].nil? || c[:name]== ""}
  
   if c_pro_socs.size > 0
     CProSociety.create(c_pro_socs)
   
   end
    
    
    
    #c_pubs
    c_pubs.delete_if {|c| CPublication.count(:conditions=>c) > 0}
  c_pubs.delete_if {|c| c[:name].nil? || c[:name]== ""}
  
   if c_pubs.size > 0
     CPublication.create(c_pubs)
   
   end
    
    
    #c_un_lang 
    
     c_un_lang.delete_if {|c| CunLanguage.count(:conditions=>c) > 0}
     c_un_lang.delete_if {|c| c[:institution].nil? }
  
    if c_un_lang.size > 0
     CunLanguage.create(c_un_lang)
   
    end 
  #cundp_certifications
  
   cundp_certifications.delete_if {|c| CundpCertification.count(:conditions=>c) > 0}
  cundp_certifications.delete_if {|c| c[:institution].nil? }
  
   if cundp_certifications.size > 0
     CundpCertification.create(cundp_certifications)
   
   end 
    
    flash[:notice] = "Successfully save Education details."
 
  else
    flash[:error] = "misunderstood request..."    
 end
  respond_to do |f|
    f.js
    
  end
end

  def edit
    @consultant = CEducation.find(:first, :conditions => {:consultant_id => params[:consultant_id],:id =>params[:id]})
    if !@consultant 
      flash[:error] = "invalid request"
      verify_user_section
    end
  end

  def update
    @consultant = CEducation.find(:first, :conditions => {:id =>params[:id]})
    if @consultant.update_attributes params[:c_education]
      flash[:notice] = "Successfully updated"
    else
      flash[:error] = "Unable to update, try again later"
    end
    respond_to do |f|
      f.html {  if identify_vendor.nil? 
            if request.env["HTTP_REFERER"].nil?
                verify_user_section
                return false
                else
                 redirect_to request.env["HTTP_REFERER"]
                 return false
                 end
              else
                redirect_to c_education_path :id => @consultant.id , :consultant_id => @consultant.consultant_id 
              end
                 }
      f.json
      f.js
    end
  end

  def show
    
    if current_user.group_id == 4
      @consultant = Consultant.find_by_undpuser_id(current_user.id,:include => [:c_educations,:c_post_qualifications,:cun_languages,:cundp_certifications,:c_pro_societies,:c_publications])
      
      if @consultant.nil?
        flash[:error] = "Invalid consultant"
        verify_user_section
      end
     else
        @consultant = Consultant.find_by_id(params[:consultant_id],:include => [:c_educations,:c_post_qualifications,:cun_languages,:cundp_certifications,:c_pro_societies,:c_publications])
        if !@consultant || @consultant.nil?
        flash[:error] = "Invalid consultant"
        if request.env["HTTP_REFERER"].nil?
        redirect_to undpusers_url(:type => 'vendors')
        return false
        else
         redirect_to request.env["HTTP_REFERER"]
         return false
      end
     else 
       @user_title = "For #{@consultant.firstname}  #{@consultant.lastname} :: Consultant"
      
      end
    end
  end

  def destroy
    @consultant = CEducation.find(:first, :conditions => {:id =>params[:id]})
    if @consultant.destroy
      flash[:notice] = "successfully deleted institution"
    else
      flash[:error] = "unable to delete institution"
    end
   
     respond_to do |f|
      f.html {  if identify_vendor.nil? 
                  
                  if request.env["HTTP_REFERER"].nil?
                verify_user_section
                return false
                else
                 redirect_to request.env["HTTP_REFERER"]
                 return false
                 end
  
              else
                redirect_to c_education_path :id => @consultant.id , :consultant_id => @consultant.consultant_id 
              end
                 }
      f.json
      f.js
  end
  end
end
