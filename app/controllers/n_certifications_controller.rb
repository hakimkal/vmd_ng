class NCertificationsController < ApplicationController
   before_filter :login_required, :only=>['destroy','edit','create','index','show','update','destroy']
   before_filter :ngos_only
  
  layout :get_user_layout 
  
  
  def new
  end

  def edit
  end

   
  def show
  end

  def index
  end
 

  
  def update
    if request.put?
      @nfinancial = NCertification.find_by_id(params[:id])
      if params[:n_certification][:certify] == "No"
         @nfinancial.update_attributes(params[:n_certification])
        flash[:notice] = "we have received your declination, your data will be wiped out in the next system cleanup exercise!"
       session[:new_model] = nil
      elsif params[:n_certification][:certify] != "No"
      
     @nfinancial.update_attributes(params[:n_certification])
        flash[:notice] = "successfully updated your certification"
        session[:new_model] = nil
      else
        flash[:error] = "unable to update changes"
        session[:new_model] = @nfinancial
      end
      
       
    
   respond_to do |format|
     if identify_vendor.nil?
        
      else
        
      format.html {redirect_to new_ngo_url :id=> params[:n_certification][:ngo_id] 
                  return false
                  }
     end
   end
    end   
  end

  def create
    
      @nfinancial = NCertification.new(params[:n_certification])
      
      if (@nfinancial.certify == "No")
          if @nfinancial.save
          flash[:notice] = "we have received your declination, your data will be wiped out in the next system cleanup exercise!"
          session[:new_model] = nil
         else
          flash[:error] = "unable to save certification"
          session[:new_model] = @nfinancial
            redirect_to new_ngo_url :id=> params[:n_certification][:ngo_id]  
            return false
         end
      
      elsif(@nfinancial.certify != "No")
            
     
         if @nfinancial.save
          session[:new_model] = nil
          flash[:notice] = "we acknowledge your certification, thank you!"
          
        else
          flash[:error] = "unable to save certification, please try again later"
          session[:new_model] = @nfinancial
            redirect_to new_ngo_url :id=> params[:n_certification][:ngo_id]  
            return false
        end
  
     end
       
    respond_to do |format|
      if identify_vendor.nil?
        
      else
        
      format.html {redirect_to new_ngo_url :id => params[:n_certification][:ngo_id] 
                  return false
                  }
     end
   end
    
    
  end


  def destroy
    
     
  end
end
