class RecruitmentsController < ApplicationController
  
  before_filter :login_required , :only => ['new','show','create','update','edit','destroy','index']
  before_filter :staff_only  
  layout :get_user_layout

  def new
    @application = VacApplication.find_by_id(params[:id], :include=>[:vacancy,:undpuser])
    
    if @application.blank? && !params[:undpuser_id].nil?
      flash[:error] = " This recruitment is not listed as an entry in the applications module"
      @vacancies = Vacancy.select("id,name,deadline,location")
      @undpuser = Undpuser.find_by_id(params[:undpuser_id],:include=>[:vendor_type])
       
      if @undpuser.nil?
        flash[:error] = "No entry found"
        redirect_to :back and return  if !request.env["HTTP_REFERER"].nil? 
      redirect_to vac_applications_url
   return  false
      end
    elsif (@application.blank? && params[:undpuser_id].blank?)
      flash[:error] = "No entry found"
      redirect_to :back and return  if !request.env["HTTP_REFERER"].nil? 
      redirect_to vac_applications_url
   return  false
    end
    @recruit = Recruitment.new
  end

  def index
    @recruitments = Recruitment.includes([:undpuser,:status]).paginate(:per_page=>50, :page =>params[:page])
    
    if !params[:recruitment].blank?
      @recruitments = Recruitment.where(:vacancy_id=>params[:recruitment][:vacancy_id]).includes([:undpuser,:status]).paginate(:per_page=>50, :page =>params[:page])
    
    end
  end

  def create
    if request.post?
      @recruit =  Recruitment.new params[:recruitment]
       vac_name= Vacancy.find(:first,:conditions=>{:id=>@recruit.vacancy_id}) 
       @recruit.job_title = vac_name.name
      if @recruit.save 
        flash[:notice] = "Saved recruitment status successfully"
        session[:new_model] = nil
        redirect_to vac_applications_url
        return
      else
        flash[:error] = "unable to save, Duplicated entry!"
        session[:new_model] = @recruit
        redirect_to :back 
        return
      end
    end
  end


def export_to_excel
   

if params[:recruits].nil?
     @clients = Recruitment.find(:all,:include=>[:undpuser,:status]) 
      
  elsif params[:recruits] == 'selected' && !session[:recruits].nil?
  @clients = Recruitment.find(:all,:conditions=>{:id=>session[:recruits]},:include=>[:undpuser,:status]) 
      
   else
     flash[:error] = "No  selection(s) detected"
     redirect_to :back
     return
  end

 



  require 'spreadsheet'
  respond_to do |format|
      format.pdf
      format.xls {
        clients = Spreadsheet::Workbook.new
        list = clients.create_worksheet :name => "recruited" + '_Applicants'
        list.row(0).concat %w[Applicant  Email  Vacancy  Recruited-by Date Status Remarks   ]
        
          
 
        
        list.row(0).height = 23 
        list.column(0).width =  @clients.first.undpuser.firstname.length + @clients.first.undpuser.lastname.length + 5
        list.column(1).width = @clients.first.undpuser.email.length + 5
        list.column(2).width = @clients.first.job_title.length + 5
        list.column(3).width = 18
       list.column(4).width = 18
        list.column(5).width = 18
        list.column(6).width = 100
           
        
                
        @clients.each_with_index { |client, i|
          if !client.nil? 
        
          # Contract-type Ad-Category Required-working-language   ]
            
           
          list.row(i+1).push("#{client.undpuser.firstname} #{client.undpuser.lastname.to_s.upcase}", client.undpuser.email,client.job_title, 
          client.recruitment_by ,client.created_at.to_date,  client.status.name,client.remark )
         
            
            
 
    
        
           
       end
        }
        
        header_format = Spreadsheet::Format.new :color => :blue, :weight => :bold ,:align =>:center
        list.row(0).default_format = header_format
        #output to blob object
        session[:recruits] = session[:user_ids] = nil
        blob = StringIO.new("")
        clients.write blob
        #respond with blob object as a file
        send_data blob.string, :type => :xls, :filename => "recruited_applicants_" +  "_exported_" + "#{Time.now}"+".xls"
      }
end
    


  end
  def edit
  end

  def update
  end

  def destroy
  end

  def show
  end
end
