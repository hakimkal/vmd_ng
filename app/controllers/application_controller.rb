class ApplicationController < ActionController::Base
  include SessionsHelper
  include AcoArosHelper
  include PublicActivity::StoreController 
  protect_from_forgery
  
  before_filter :disable_p_a_gem
  
  before_filter    {|c|
                    #session[:action] = c.params[:controller]
                    if c.signed_in?  and !c.request.xhr? 
                    check_vpa_id_and_vss_id(current_user,c.params[:action])  
                    
                    end  
                        
                    }
  
  
  before_filter    {|c|
                    #session[:action] = c.params[:controller]
                    c.is_allowed(current_user,c.params[:action], c.params[:controller])  if c.signed_in?  
                        
                    }
  
  
  
  def login_required
     if signed_in?
      return true
    else
    flash[:warning]='Please login to continue'
    session[:return_to]=request.url
    #redirect_to :controller => "users", :action => "login"
    redirect_to "/login"
    return false 
    end
  end
  
  def is_logged_in_user_admin 
    
   if !is_logged_in_user_admin?(current_user).nil?
     return true
   else
     flash[:error] = "Insufficient access rights!!!"
     redirect_to :back
     #return false
   end
    
  end
   
   def is_allowed(user,action,controller) 
     
     if signed_in? and is_allowed?(user,action,controller)
      return true
     else
       flash[:error] = "Insufficient access rights, you have been restricted and redirected!!!"
       redirect_to :back
       return false
     end
     
   end
  def check_admin(rpa)
    if is_admin?(rpa)
      flash[:error] = "Administrators have Super Usage rights, To modify access for admin users, you will have to assign the user to a different group!!"
      redirect_to :back
      return false
      #undpusers_url(:type=>"staff") and return
    else
      return true
    end
  end
  def logout_required
    if signed_in?
      sign_out
    end
  end
  
  
  def check_user_group
    
    if (signed_in? && (current_user.group_id !=4))
      
      return true
    else 
      flash[:error] = 'An attempt to access a restricted area has been detected and denied for this account'
  
       verify_user_section
    end
  end
  
  def consultants_only
    
    if ((signed_in? and (current_user.group_id ==4) and (current_user.vendor_id == 1)) or (signed_in? and (current_user.group_id !=4)))
      
      return true
     
    
    else 
      flash[:error] = 'An attempt to access a restricted area has been detected and denied for this account'
  
       verify_user_section
    end
  end
  
  def check_consultant
     if current_user.group_id == 4  
      
         cons=  Consultant.find_by_undpuser_id(current_user.id)
        session[:cons_id] = cons.id
        if cons.id == params[:consultant_id].to_i 
          return true
        elsif cons.id == params[:id].to_i
        return true
        elsif current_user.group_id == 4 && params[:consultant_id].nil? && params[:id].nil?
          params[:consultant_id] = cons.id
          return true
          
        elsif current_user.group_id == 4 && params[:consultant_id].nil? && !params[:id].nil?
          params[:consultant_id] = cons.id
          return true
        
        else
          flash[:error] = "Suspicious request, impersonation detected" 
          verify_user_section
       end
     else
       return true
     end
   
  end
  
  
  def ngos_only
    
    if ((signed_in? && (current_user.group_id ==4) && (current_user.vendor_id == 2)) || (signed_in? && (current_user.group_id !=4)))
      
      return true
     
    
    else 
      flash[:error] = 'An attempt to access a restricted area has been detected and denied for this account'
  
       verify_user_section
    end
  end
  
  
  def suppliers_only
    
    if ((signed_in? && (current_user.group_id ==4) && (current_user.vendor_id == 3)) || (signed_in? && (current_user.group_id !=4)))
      
      return true
     
    
    else 
      flash[:error] = 'An attempt to access a restricted area has been detected and denied for this account'
  
       verify_user_section
    end
  end
  
  def staff_only
    
    if ((signed_in? && (current_user.group_id !=4)))
      
      return true
     
    
    else 
      flash[:error] = 'An attempt to access a restricted area has been detected and denied for this account'
  
       verify_user_section
    end
  end
  
  def disable_p_a_gem
    
    ## Disable p_a for Article class
    #  Article.public_activity_off

    # p_a will not do anything here:
    #@article = Article.create(:title => "New article")
    
    # But will be enabled for other classes:
    # (creation of the comment will be recorded if you are tracking the Comment class)
    #@article.comments.create(:body => "some comment!") 
    
    # Enable it again for Article:
    # Article.public_activity_on
    if !signed_in?
      #Article.public_activity_off
      PublicActivity.enabled = false
    else
       PublicActivity.enabled = true
      
    end
  end
  
  def check_vpa_id_and_vss_id(user,action_name)
    
    if((user.group_id == 4) and (user.vpa_id.nil? or user.vss_id.nil?) and action_name !='vendor_select' and action_name != 'listed')
      flash[:error] = "Please Select Vendor Programme Area and Vendor Special Area"
      redirect_to vendor_select_undpuser_url user
    end
  end
end
