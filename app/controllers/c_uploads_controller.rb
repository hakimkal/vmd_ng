class CUploadsController < ApplicationController
  
  before_filter :login_required, :only=>['destroy','index','show','new','edit','create','update']
  before_filter :consultants_only , :only=>['destroy','index','show','new','edit','create','update']
  before_filter :check_consultant  ,:except => ['create','add','remove']
  
  
  layout :get_user_layout
  def new
  end

  def index
  end

  def create
    if request.post?
      @cupload = CUpload.new(params[:c_upload])
       
      
      if @cupload.save
        flash[:notice] = "Successfully saved upload(s)"
        redirect_to :back
        return false
      else
        flash[:error] = "unable to save uploads...Please make sure to upload an image not more than 1Mb. Duplicates are not acceptable. <br/> You may edit your upload(s) at any time!".html_safe
           
         if current_user.group_id !=4
         redirect_to  :back
         
         else
           redirect_to c_upload_url(current_user)
           
          end
         
      end
    end
    
    return false
  end

  def edit
      if current_user.group_id == 4
      @c_upload = CUpload.find(:first, :conditions => {:consultant_id => params[:consultant_id]})      
      if @c_upload.nil?
        flash[:error] = "No record found"
        verify_user_section
      end
     else
       
       @c_upload = CUpload.find(:first, :conditions => {:consultant_id => params[:consultant_id]})      
    if @c_upload.nil?
        flash[:error] = "No record found"
        verify_user_section
      end
     end
  
  end

  def update
    if request.put?
    @cupload = CUpload.find(:first,:conditions=>{:id=>params[:c_upload][:id]})
    #session[:c_upload] = @cupload;
     if params[:c_upload][:photo] && !params[:c_upload][:resume]
       @cupload.photo = nil
       @cupload.save
     elsif !params[:c_upload][:photo] && params[:c_upload][:resume]
     @cupload.resume = nil
     @cupload.save
      elsif params[:c_upload][:photo] && params[:c_upload][:resume]
      @cupload.photo  = nil
      @cupload.resume = nil
      @cupload.save
     end
       
              
        
       if @cupload.update_attributes(params[:c_upload])
       flash[:notice] = "Successfully updated uploads"
       if identify_vendor.nil?
         if request.env["HTTP_REFERER"].nil?
                verify_user_section
                return false
                else
                 redirect_to request.env["HTTP_REFERER"]
                 return false
                 end
       else
         verify_user_section
       end
      else
        
         flash[:error] = "Unable to update. Remember your image cannot be more than 1mb."
         redirect_to request.env["HTTP_REFERER"]
       end
  end
end
  def destroy
  end

  def show
   if current_user.group_id == 4
      @consultant = Consultant.find_by_undpuser_id(current_user.id,:include => [:c_upload])      
      if @consultant.c_upload.nil?
        flash[:error] = "No record found!!!"
        verify_user_section
      end
   else
        @consultant = Consultant.find_by_id(params[:consultant_id],:include => [:c_upload])
        
        if !@consultant || @consultant.nil? || @consultant.c_upload.nil?
        flash[:error] = "no record found"
        if request.env["HTTP_REFERER"].nil?
        redirect_to undpusers_url(:type => 'vendors')
        return false
        else
         redirect_to request.env["HTTP_REFERER"]
         return false
      end
     else 
       @user_title = "For #{@consultant.firstname}  #{@consultant.lastname} :: Consultant"
      end
     end
  end
end
