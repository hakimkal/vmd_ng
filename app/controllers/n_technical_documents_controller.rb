class NTechnicalDocumentsController < ApplicationController
  
  before_filter :login_required, :only=>['destroy','edit','create','index','show','update','destroy']
   before_filter :ngos_only
  
  layout :get_user_layout 
   
  
  def new
  end

  def index
  end

  def create
    if request.post?
         
      @nupload = NTechnicalDocument.new params[:n_technical_document]
      if @nupload.save
        session[:new_model] = nil
        flash[:notice] = "Successfully saved upload(s)"
       redirect_to new_ngo_url(:id => params[:n_technical_document][:ngo_id])
       return false
      else
        flash[:error] = "unable to save uploads...Please make sure to upload each resource not more than 2.9Mb. Duplicates are not acceptable. <br/> You may edit your upload(s) at any time!".html_safe
           session[:new_model] = @nil
         redirect_to new_ngo_url(:id => params[:n_technical_document][:ngo_id])
         return false
      end
    
    end
     
  end

  def edit
  end

  def update
    
    if request.put?
         
      @nupload = NTechnicalDocument.find_by_id(params[:id])
       if params[:n_technical_document][:written] && !params[:n_technical_document][:job_evidence] && !params[:n_technical_document][:financial]
        @nupload.written = nil
        @nupload.save
        
       elsif !params[:n_technical_document][:written] && !params[:n_technical_document][:job_evidence] && params[:n_technical_document][:financial]
        @nupload.job_evidence = nil
        @nupload.save
       elsif !params[:n_technical_document][:written] && !params[:n_technical_document][:job_evidence] && params[:n_technical_document][:financial]
        @nupload.financial = nil
        @nupload.save
       elsif  params[:n_technical_document][:written] && params[:n_technical_document][:job_evidence] && params[:n_technical_document][:financial]
        @nupload.written = @nupload.job_evidence = @nupload.financial = nil
        @nupload.save
       end
       
      if @nupload.update_attributes params[:n_technical_document]
         
        flash[:notice] = "Successfully saved upload(s)"
       redirect_to new_ngo_url(:id => params[:n_technical_document][:ngo_id])
       return false
  else
        flash[:error] = "unable to update upload(s)...Please make sure to upload each resource not more than 2.9Mb. Duplicates are not acceptable. <br/> You may edit your upload(s) at any time!".html_safe
           
         redirect_to new_ngo_url(:id => params[:n_technical_document][:ngo_id])
         return false
      end
    
    
    end
  end

  def show
  end

  def destroy
  end
end
