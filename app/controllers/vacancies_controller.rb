class VacanciesController < ApplicationController
  
  before_filter :login_required , :only => ['new','create','update','edit','destroy','index']
  before_filter :staff_only ,:except => ['show','view','adv_search']
  #before_filter :check_consultant  ,:except => ['create','add','remove']
  layout :get_user_layout
  
  def new
    @vendor_types = VendorType.select("id,name")
    @vacancy =  Vacancy.new
    
  end

  def view
    # @vacancies = Vacancy.where("status = 0 and deadline >= ?", Time.now)
    @vacancies = Vacancy.where("status = 0 and deadline::DATE >= ?",Time.now.strftime('%m-%d-%Y')).order('name asc').paginate(:page=>params[:page],:per_page=>40)
    
     if params[:vacancy]
     if !params[:vacancy][:vendor_id].blank?
     return @vacancies = Vacancy.where("status =0 and vendor_id = ? and deadline::DATE >= ? " , params[:vacancy][:vendor_id],Time.now.strftime('%m-%d-%Y')).paginate(:page=>params[:page],:per_page=>40)
      
     end
     end
  end
  
  def adv_search
    if request.get?
    
    @vacancy = Vacancy.new 
    
    elsif request.post?
       
   @vacancy = Vacancy.new
   
     if params[:vacancy][:vendor_id].blank? && params[:vacancy][:vpa_id].blank? && params[:vacancy][:vss_id].blank?
       @vacancies = Vacancy.where(:status => 0).order('name asc').paginate(:page=>params[:page],:per_page=>40)
       return false
      
     elsif !params[:vacancy][:vendor_id].blank? && params[:vacancy][:vpa_id].blank? && params[:vacancy][:vss_id].blank?
       @vacancies = Vacancy.where(:status => 0,:vendor_id=>params[:vacancy][:vendor_id]).order('name asc').paginate(:page=>params[:page],:per_page=>40)
       return false
     elsif !params[:vacancy][:vendor_id].blank? && !params[:vacancy][:vpa_id].blank? && params[:vacancy][:vss_id].blank?
       @vacancies = Vacancy.where(:status => 0,:vendor_id=>params[:vacancy][:vendor_id],:vpa_id=>params[:vacancy][:vpa_id]).order('name asc').paginate(:page=>params[:page],:per_page=>40)
       return false
     elsif !params[:vacancy][:vendor_id].blank? && !params[:vacancy][:vpa_id].blank? && !params[:vacancy][:vss_id].blank?
       @vacancies = Vacancy.where(:status => 0,:vendor_id=>params[:vacancy][:vendor_id],:vpa_id=>params[:vacancy][:vpa_id],:vss_id=>params[:vacancy][:vss_id]).order('name asc').paginate(:page=>params[:page],:per_page=>40)
       return false
    
     end   
      
        
        
   end
  end
  def show
    @back = request.env["HTTP_REFERER"]
    @vacancy = Vacancy.find_by_id(params[:id])
  end

  def create
    if request.post?
     @vacancy = Vacancy.new params[:vacancy]
     if @vacancy.save
       session[:new_model] = nil
       flash[:notice] = "Successfully created a vacancy"
       redirect_to vacancies_url
     else
       flash[:error] = "The following error(s) prevented saving to the database<br/>".html_safe
       
       
       
       @vacancy.errors.full_messages.each do |e|
        flash[:error]+= e.html_safe + "<br/>".html_safe
        
      end
       redirect_to :back
     end
    end
  end
  
  
  def index
    
   if !params[:filter].nil?
   filter = params[:filter]
   else
   filter = "name is not null"
   end
   if params[:sort].nil?
   sort = "name asc"
   else
   sort = params[:sort]
   end
   
    @back = request.env["HTTP_REFERER"]
    @vacancies = Vacancy.where(filter).paginate(:page=>params[:page],:per_page=>40).order(sort)
  end

  def edit
    
     @back = request.env["HTTP_REFERER"]
    @vendor_types = VendorType.select("id,name")
    
    
    @vacancy = Vacancy.find_by_id(params[:id])
    
    if @vacancy.blank?
      flash[:error] = "Invalid vacancy selected"
      redirect_to :back
    end
    if @vacancy.status == 0 && VacApplication.count(:conditions=>{:vacancy_id=>params[:id]}) > 0
      flash[:error] = "This vacancy is currently OPEN and have received applications, you can no longer edit this postion until you delist all applications!!!"
     redirect_to :back
    end
  end

  def destroy
    if request.delete?
    @vacancy = Vacancy.find_by_id(params[:id])
    
    if @vacancy.destroy
      flash[:notice] = "Successfully deleted vacancy"
      redirect_to vacancies_url and return
    else
      flash[:error] = "unable to delete vacancy"
      redirect_to vacancies_url and return
    end
  end
end
  def update
    if request.put?
      
      @vacancy = Vacancy.find_by_id(params[:id])
       
       
       
      if params[:vacancy] 
      if !params[:vacancy][:file_1].blank?
        @vacancy.file_1 = nil
        @vacancy.save
        
      end
      
      if !params[:vacancy][:file_2].blank?
        @vacancy.file_2 = nil
        @vacancy.save
        
      end
       if !params[:vacancy][:file_3].blank?
        @vacancy.file_3 = nil
        @vacancy.save
        
      end
      
       if !params[:vacancy][:file_4].blank?
        @vacancy.file_4 = nil
        @vacancy.save
        
      end
       if !params[:vacancy][:file_5].blank?
        @vacancy.file_5 = nil
        @vacancy.save
        
      end
     
      if @vacancy.update_attributes(params[:vacancy])
        flash[:notice] = "Successfully updated vacancy"
                      
      else
         
        flash[:error] = "The following error(s) prevented saving to the database<br/>".html_safe
       
       
       
       @vacancy.errors.full_messages.each do |e|
        flash[:error]+= e.html_safe + "<br/>".html_safe
        
      end
         redirect_to :back and return
               
      end
      else
        if params[:status] == "0"
        @vacancy.status = 0
        if  @vacancy.save
        flash[:notice]= "opened vacancy"
        end
      elsif  params[:status] == "1"
        @vacancy.status = 1
       if @vacancy.save
        flash[:notice]= "closed vacancy"
        end
      end
       end
      respond_to do |f|
       f.html{
       
      
        redirect_to vacancies_url and return
      
          }
      
       f.js
       
        
      
      end
    end
  end

end
