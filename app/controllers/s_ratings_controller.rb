class SRatingsController < ApplicationController
  
  before_filter :login_required , :only => ['index','new','select_cons','create','update','edit','destroy','show']
  before_filter :staff_only 
  #before_filter :check_consultant  ,:except => ['create','add','remove']
  layout :get_user_layout
  
  def select_sups
    if params[:cancel] == "1"
      session[:new_model] = nil
    end
    @s_rating = SRating.new
  end
  def new
    if params[:cancel] == "1"
      session[:new_model] = nil
    end
     if request.post?
       session[:new_model] = nil
      if !params[:s_rating][:name].blank?
      @suppliers = Supplier.where("name ilike '#{params[:s_rating][:name]}%'")
       end
     if @suppliers.blank?
       flash[:error] = "No entry found"
       redirect_to :back
       return false    
    end
    end
    @s_rating = SRating.new
    
    
  end

  def create
    
     @s_rating = SRating.new params[:s_rating]
   c  = Supplier.find_by_id(@s_rating.supplier_id) if  !@s_rating.supplier_id.blank?
   @s_rating.supplier_name = "#{c.name}" if !c.blank?
   if @s_rating.save
      session[:new_model] = nil
     flash[:notice] = " Sucessfully saved entry"
     redirect_to select_sups_s_ratings_url
     return false 
     
   else
     flash[:error] = "Unable to save entry"
     session[:new_model] = @s_rating
    if @s_rating.supplier_id.blank?
       redirect_to select_sups_s_ratings_url
       return false
     end
     @suppliers = Supplier.find(:all,:conditions=>{:id=>c.id})
     
     render :new
   end
  end

  def edit
    
     @s_rating = SRating.find(params[:id])
     #@consultants = Consultant.find(@c_rating.consultant_id)
     @suppliers = Supplier.where("id in(?)",@s_rating.supplier_id)
    
   
 
   
  end

  def update
    
    @reference = SRating.find(:first,:conditions=>{:id => params[:id]})
      #debugger
      if @reference && @reference.update_attributes(params[:s_rating])
        flash[:notice] = "Updated rating successfully"
       else
         flash[:error] = "Unable to update rating, try again later.<br/> **Blank fields are unacceptable!".html_safe
         session[:model] = nil
      end
      
      redirect_to sp_rating_url(@reference) and return
  end

   

  def index
  end

  def show
  end

  def destroy
    if @reference = SRating.find(:first,:conditions=>{:id => params[:id]}).destroy()
      flash[:notice] = "Successfully deleted rating"
    else
      flash[:error] = "unable to delete rating"
    end
     respond_to do |format|
        format.html { 
                       verify_user_section
                        return false
                         
                     }
                 end
  end
end
