class VpasController < ApplicationController
 before_filter :login_required , :only => ['index','create','update','edit','destroy']

  before_filter :check_user_group ,  :except => [:listed]
  layout :get_user_layout ,:except  => [:listed]
  respond_to :json , :only=>['listed']
  
 def listed
  if (request.get? and !params[:vendor_type_id].nil?)
   
  list = Vpa.select("id, name").where("vendor_type_id" => params[:vendor_type_id]).order(:name)
    
  respond_with list 
  end
 end
 
  def new
  end

  def create
    if request.post?
      vpa = Vpa.new(params[:vpas])
      if vpa.save 
        session[:new_model] = nil
        flash[:notice] = "Successfully addded Programme Area"
        redirect_to  new_vendor_type_url
       else
         session[:new_model] = vpa
         redirect_to  new_vendor_type_url
      end
    end
  end

 
 
  def index
    @title = 'Vendor Programme Areas [Thematic Area]'
    @vpas =  Vpa.includes(:vendor_type).where("vendor_type_id is not null").paginate(:page=>params[:page],:per_page=>50).order(:vendor_type_id)
    
  end

  def show
  end

  def edit
    @vendors = VendorType.select("id , name")
    @vpa = Vpa.find_by_id(params[:id])
    
    redirect_to request.env['HTTP_REFERER'] if @vpa.nil?
  end

  def update
    if request.put?
      @vpa = Vpa.find_by_id(params[:id])
      if @vpa && @vpa.update_attributes(params[:vpa])
        flash[:notice] = "Successfully updated Programme: #{params[:vpa][:name]}"
       
       else
         flash[:error] = "Failed to update Programme: #{params[:vpa][:name]} , Try again later."
         
    end
    redirect_to vpas_url
  end
  end
  def destroy
    if Vpa.find_by_id(params[:id]).destroy
      flash[:notice] = "Successfully deleted programme"
      
    else
      flash[:error] = "Unable to delete programme area"
    
    end
     redirect_to request.env['HTTP_REFERER']
     return
  end
end
