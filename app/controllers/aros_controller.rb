class ArosController < ApplicationController
  before_filter :login_required , :only => ['index','new','create','update','edit','destroy','show']
  before_filter :staff_only 
  #before_filter :check_consultant  ,:except => ['create','add','remove']
  layout :get_user_layout
  
   
   
   def new
    @sys_module = @fund = Aro.new
      
    
  end

  def create
    @sys_module  = Aro.new params[:aco]
    
    if @sys_module.save
      flash[:notice] = "Saved Aro!"
      
    else
      flash[:error]= "Failed to create"
      
    end
    
     redirect_to aros_url
    return false
  end

  def index
   @aros = @sys_modules = @funds = Aro.paginate(:page =>params[:page], :per_page => params[:per_page])
  end

  def edit
      
     @sys_module  = Aro.find_by_id(params[:id])
   
  end
  
  
  def setup_aros
   acos =  Aro.auto_populate_aros
   
   success = []
   failed = []
   acos.each do |a|
   if a[2]  == "s"
     success << a[0]
     
   elsif a[2] == "f"
     failed << a[0]
     
   end
   
   end
   failed =failed.uniq
   success = success.uniq
   suc =""
   fail = ""
   if !success.blank?
     success.each do |s|
       suc += s + " | " 
       
     end
   end
   
    if !failed.blank?
     failed.each do |s|
       fail += s + " | " 
       
     end
   end
     flash[:notice] = "#{success.size} Aros were successfully added for #{suc} "
     
     flash[:error] = "#{failed.size} Aros were not created for #{fail} "
     
     redirect_to aros_url
     return false
  end
  
  def destroy
    
     @sys_module  = Aro.find_by_id(params[:id])
      if @sys_module.destroy
        flash[:notice]  = "Aros  deleted"
      end
      redirect_to aros_url
    return false
  end

  def show
     redirect_to aros_url
    return false
  end

  def update
      @sys_module  = Aro.find_by_id(params[:id])
      if @sys_module.update_attributes(params[:aco])
        flash[:notice]  = "Action updated"
      end
       redirect_to aros_url
    return false
  end
end
