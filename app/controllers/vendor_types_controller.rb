class VendorTypesController < ApplicationController
  
   before_filter :login_required, :only=>['destroy','index','t_view_mgr','show','new','con_sup_service']
  before_filter :check_user_group , :only=>['con_sup_service','t_view_mgr'] 
  
  layout :get_user_layout
 
  def t_view_mgr
    session[:new_model] = nil
    
  end
  
  def cons_sup_service
    if request.post?
      if params[:con_sup_service][:vendor_type].nil?
      flash[:error] = "Please select vendor type..."
      redirect_to new_vendor_type_url 
       
      end
      case params[:con_sup_service][:vendor_type].upcase
      when 'CONSULTANT'
      if Conservice.create({:name=>params[:con_sup_service][:name],:description=>params[:con_sup_service][:description]})
      flash[:notice] = 'Successfully added consultant service title'
      else 
        flash[:error] = 'Unable to add service, Could be you are attempting to duplicate service titles'
      end
      when 'SUPPLIER'
       if Supservice.create({:name=>params[:con_sup_service][:name],:description=>params[:con_sup_service][:description]})
     flash[:notice] = 'Successfully added supplier service title'
    
      else
          flash[:error] = 'Unable to add service, Could be you are attempting to duplicate service titles'
     
      end
    end
    end
    redirect_to new_vendor_type_url
  end
  
  
  def new
    @vendor = @model = VendorType.new
    vendors = VendorType.select("id , name") 
    if vendors.nil?
      nil
    else
          @vendors = vendors 
    end
  end

  def index
  end

  def create
    if request.post?
    @vendor = @model = VendorType.new(params[:vendor_type])
   if  @vendor.save
     flash[:notice] = "Successfully created a vendor type #{@vendor.name}"
     redirect_to new_vendor_type_url
   end
    
     end
  end

  def vendor_manager
    
  end

  def edit
  end

  def update
  end

  def show
  end

  def destroy
  end
end
