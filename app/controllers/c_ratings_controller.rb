class CRatingsController < ApplicationController
  before_filter :login_required , :only => ['index','new','select_cons','search_vendor','create','update','edit','destroy','show']
  before_filter :staff_only 
  #before_filter :check_consultant  ,:except => ['create','add','remove']
  layout :get_user_layout
  
  def select_cons
    if params[:cancel] == "1"
      session[:new_model] = nil
    end
    @c_rating = CRating.new
  end
  
  def search_vendor
     
     
   
   if request.put?
     if params[:vendor_type].blank?
       flash[:error] = "Search parameter requires vendor type, please select vendor type"
       redirect_to :back
       return false
     
     else
       case params[:vendor_type]
         
       when "1"
         
         redirect_to cons_ratings_url(:name=>params[:name])
       
       when "2"
        redirect_to ng_ratings_url(:name=>params[:name])
         
       when "3"
         redirect_to sp_ratings_url(:name=>params[:name])
       
       end  
          
     end
     
     
   end 
   
  end
  
  def new
    if params[:cancel] == "1"
      session[:new_model] = nil
    end
    if request.post?
      session[:new_model] = nil
      if !params[:c_rating][:firstname].blank?
      @consultants = Consultant.where("firstname ilike '#{params[:c_rating][:firstname]}%'")
      elsif !params[:c_rating][:lastname].blank?
    @consultants = Consultant.where("lastname ilike '#{params[:c_rating][:lastname]}%'")
      end
     if @consultants.blank?
       flash[:error] = "No entry found"
       redirect_to :back
       return false    
    end
    end
    @c_rating = CRating.new
    
  #  @consultants = Consultant.select("id, firstname, lastname")
  end

  def create
   
   @c_rating = CRating.new params[:c_rating]
   c  = Consultant.find_by_id(@c_rating.consultant_id) if  !@c_rating.consultant_id.blank?
   @c_rating.consultant_name = "#{c.firstname} #{c.lastname}" if !c.blank?
   if @c_rating.save
      session[:new_model] = nil
     flash[:notice] = "Sucessfully saved entry"
     redirect_to select_cons_c_ratings_url
     return false 
     
   else
     flash[:error] = "Unable to save entry"
     session[:new_model] = @c_rating
     if @c_rating.consultant_id.blank?
       redirect_to select_cons_c_ratings_url
       return
     end
     @consultants = Consultant.find(:all,:conditions=>{:id=>c.id})
     
     render :new
      
   end
  end

  def edit
    
     @c_rating = CRating.find(params[:id])
     #@consultants = Consultant.find(@c_rating.consultant_id)
     @consultants = Consultant.where("id in(?)",@c_rating.consultant_id)
    
   
 
   
  end

  def update
    
    @reference = CRating.find(:first,:conditions=>{:id => params[:id]})
      #debugger
      if @reference && @reference.update_attributes(params[:c_rating])
        flash[:notice] = "Updated rating successfully"
       else
         flash[:error] = "Unable to update rating, try again later."
         session[:"model_error"] = @reference
      end
      
      redirect_to cons_rating_url(@reference) and return
  end

  def index
  end

  def show
  end

  def destroy
    if @reference = CRating.find(:first,:conditions=>{:id => params[:id]}).destroy()
      flash[:notice] = "Successfully deleted rating"
    else
      flash[:error] = "unable to delete rating"
    end
     respond_to do |format|
        format.html { 
                       verify_user_section
                        return false
                         
                     }
                 end
  end
end
