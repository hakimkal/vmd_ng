class CMiscsController < ApplicationController
  before_filter :login_required , :only => ['index','new','create','update','edit','destroy','show']
  before_filter :consultants_only 
  before_filter :check_consultant  ,:except => ['create','add','remove']
   
  
  layout :get_user_layout
  def new
  end

  def index
  end

  def create
#
#c_de_pendent[][dob]=
#c_de_pendent[][name]=
#c_de_pendent[]consultant_id]=1
#c_de_pendent[]relationship]=

if request.post?
  dependents = []
  params[:c_de_pendent].each do |c|
    dependents << {
                  :name => "#{c[:name]}" ,
                  :dob =>  "#{c[:dob]}" ,
                  :consultant_id => "#{c[:consultant_id]}",
                  :relationship => "#{c[:relationship]}"
                   }
         end
        dependents.delete_if {|c| c[:name].nil? || c[:name] == ""}
    dependents.delete_if {|c| CDePendent.count(:conditions => c) > 0 }
    if dependents.size > 0 
      CDePendent.create(dependents)
    end
         
       #c_ext_family_in_undp[][name]=
#c_ext_family_in_undp[][relationship]=
#c_ext_family_in_undp[]consultant_id]=1
#c_ext_family_in_undp[]duty_station]=
#c_ext_family_in_undp[]name_of_unit]=

    ext_family = []
    params[:c_ext_family_in_undp].each do |c|
      ext_family << {
                  
                  :name => "#{c[:name]}" ,
                  :duty_station =>  "#{c[:duty_station]}" ,
                  :consultant_id => "#{c[:consultant_id]}",
                  :relationship => "#{c[:relationship]}" ,
                  :name_of_unit => "#{c[:name_of_unit]}"
                    }
    end
    
    ext_family.delete_if {|c| c[:name].nil? || c[:name] == ""}
    ext_family.delete_if {|c| CExtFamilyInUndp.count(:conditions => c) > 0 }
    if ext_family.size > 0 
      CExtFamilyInUndp.create(ext_family)
    end
    #c_family_in_undp[][name]=
#c_family_in_undp[][relationship]=
#c_family_in_undp[]consultant_id]=1
#c_family_in_undp[]duty_station]=
#c_family_in_undp[]name_of_unit]=
    family = []
    params[:c_family_in_undp].each do |c|
      family << {
                  
                  :name => "#{c[:name]}" ,
                  :duty_station =>  "#{c[:duty_station]}" ,
                  :consultant_id => "#{c[:consultant_id]}",
                  :relationship => "#{c[:relationship]}" ,
                  :name_of_unit => "#{c[:name_of_unit]}"
                    }
    end
   family.delete_if { |c| c[:name].nil? || c[:name] == ""}
   family.delete_if {|c| CFamilyInUndp.count(:conditions => c) > 0}
   if family.size > 0
     CFamilyInUndp.create(family)
   end

#c_language[][language]=
#c_language[][read_status]=none
#c_language[][speak_status]=none
#c_language[][understand_status]=none
#c_language[][write_status]=none
#c_language[]consultant_id]=1

    languages = []
    params[:c_language].each do |c|
      languages << {
                  :language => "#{c[:language]}" ,
                  :read_status => "#{c[:read_status]}" ,
                  :speak_status => "#{c[:speak_status]}" ,
                  :write_status => "#{c[:write_status]}" ,
                  :understand_status => "#{c[:understand_status]}" ,
                  :consultant_id => "#{c[:consultant_id]}"
                   }
    end
    languages.delete_if {|c| c[:language].nil? || c[:language] == ""}
    languages.delete_if {|c| CLanguage.count(:conditions => c) > 0 }
      if languages.size > 0
        CLanguage.create(languages)
      end
#c_misc[asat_date_taken]=
#c_misc[consultant_id]=1
#c_misc[conviction_particulars]=
#c_misc[disciplinary_particulars]=
#c_misc[finance_assessment_date_taken]=
#c_misc[interviewed_posts]=
#c_misc[legal_permanent_residence_country]=
#c_misc[present_nationality_change_explain]=
#c_misc[separation_particulars]=
  
#c_misc[accept_employment_for_less_than_six]=on
#c_misc[any_dependent]=on
#c_misc[asat_date_taken]=
#c_misc[asat_test_status]=on
#c_misc[consultant_id]=1
#c_misc[conviction_particulars]=
#c_misc[disciplinary_measures]=on
#c_misc[disciplinary_particulars]=
#c_misc[ever_been_convicted_fined_imprisoned]=on
#c_misc[ext_family_in_undp_status]=on
#c_misc[family_in_undp_status]=on
#c_misc[finance_assessment_date_taken]=
#c_misc[finance_assessment_test_status]=on
#c_misc[interviewed_for_any_undp_job_in_last_twelve]=on
#c_misc[interviewed_posts]=
#c_misc[legal_permanent_residence_country]=
#c_misc[legal_permanent_residence_status]=on
#c_misc[present_nationality_change_explain]=
#c_misc[present_nationality_change_status]=on
#c_misc[separated_from_service]=on
#c_misc[separation_particulars]=
 
    
    @miscs = CMisc.new(params[:c_misc])
    
    if @miscs.save
      flash[:notice] = "Successfully saved Miscellaneous data"
    else
       flash[:error] = "unable to save data, please make sure you have answered every detail required.\\n Duplicates are not allowed".html_safe
    end
  
  end

  respond_to do |f|
    f.js
  end
end

   def edit
    @consultant =CMisc.find(:first, :conditions => {:consultant_id => params[:consultant_id],:id =>params[:id]})
    if !@consultant 
      flash[:error] = "invalid request"
      verify_user_section
    end
  end

  def update
    @consultant = CMisc.find(:first, :conditions => {:id =>params[:id]})
    if @consultant.update_attributes params[:c_misc]
      flash[:notice] = "Successfully updated"
    else
      flash[:error] = "Unable to update, try again later"
    end
    respond_to do |f|
      f.html {  if identify_vendor.nil? 
              
              if request.env["HTTP_REFERER"].nil?
                verify_user_section
                return false
                else
                 redirect_to request.env["HTTP_REFERER"]
                 return false
                 end
              else
                redirect_to c_misc_path :id => @consultant.id , :consultant_id => @consultant.consultant_id 
              end
                 }
      f.json
      f.js
    end
  end
  def destroy
  end

  def show
    
     if current_user.group_id == 4
      @consultant = Consultant.find_by_undpuser_id(current_user.id,:include => [:c_misc,:c_de_pendents,:c_family_in_undps,:c_ext_family_in_undps,:c_languages])      
      if @consultant.nil? || @consultant.c_misc.nil?
        flash[:error] = "No record found!"
        verify_user_section
      end
     else
        @consultant = Consultant.find_by_id(params[:consultant_id],:include => [:c_misc,:c_de_pendents,:c_family_in_undps,:c_ext_family_in_undps,:c_languages])
           
     if !@consultant || @consultant.nil? || @consultant.c_misc.nil?
        flash[:error] = "no record found"
        if request.env["HTTP_REFERER"].nil?
        redirect_to undpusers_url(:type => 'vendors')
        return false
        else
         redirect_to request.env["HTTP_REFERER"]
         return false
      end
     else 
       @user_title = "For #{@consultant.firstname}  #{@consultant.lastname} :: Consultant"
      end
    end
  end
end
