class CCertificationsController < ApplicationController
  before_filter :login_required, :only=>['destroy','index','show','new','create','update']
  before_filter :consultants_only , :only=>['destroy','index','show','new','create','update']
  
  layout :get_user_layout
 
  def index
  end

  def new
  end

  def create
    if params[:c_certification][:certify].to_i == 0
      flash[:error] = "We have received your submissions and noted you decline to certify all your information. \\n Your submissions will be cleared out in the next wipeout exercise. \\n Thank you"
   
   elsif params[:c_certification][:certify].to_i == 1
      @certify = CCertification.new(params[:c_certification])
      
      if @certify.save
        flash[:notice] ="We have received your certification!"
      else
        flash[:error] = "Unabale to process at the moment, try again later!"
      end
      
     
    end
    respond_to do |f|
      f.js
    
  end
end
  def edit
  end

  def update
    
    if params[:c_certification][:certify].to_i == 0
      flash[:error] = "We have received your submissions and noted you decline to certify all your information. \\n Your submissions will be cleared out in the next wipeout exercise. \\n Thank you"
      CCertification.find_by_id(params[:id]).update_attributes(params[:c_certification])
     
      
   elsif CCertification.find(:first, :conditions=> {:id =>params[:id]}).update_attributes(params[:c_certification])
     
      
         flash[:notice] ="We have received your certification!"
  else
        flash[:error] = "Unabale to process at the moment, try again later!"
      end
      
     if identify_vendor.nil?
       if request.env["HTTP_REFERER"].nil?
                verify_user_section
                return false
                else
                 redirect_to request.env["HTTP_REFERER"]
                 return false
                 end
     else
       verify_user_section
      end
  end

  def show
    if current_user.group_id == 4
      @consultant = Consultant.find_by_undpuser_id(current_user.id,:include => [:c_certification])      
      if @consultant.c_certification.nil? 
        flash[:error] = "No record found"
        verify_user_section
      end
     else
        @consultant = Consultant.find_by_id(params[:consultant_id],:include => [:c_certification])
           
      if !@consultant || @consultant.nil? || @consultant.c_certification.nil?
        flash[:error] = "no record found"
        if request.env["HTTP_REFERER"].nil?
        redirect_to undpusers_url(:type => 'vendors')
        return false
        else
         redirect_to request.env["HTTP_REFERER"]
         return false
      end
     else 
       @user_title = "For #{@consultant.firstname}  #{@consultant.lastname} :: Consultant"
      end
    end
  end
  

  def destroy
  end
end
