class NgouserController < ApplicationController
  before_filter :login_required, :only=>['destroy','edit','ngouser','index','show','update']
  before_filter :ngos_only
  layout :get_user_layout 
  
  def index
  end

  def ngouser
    
    if (Ngo.find_by_undpuser_id(current_user.id).nil? && current_user.group_id == 4 && current_user.vendor_id == 2)
       @ngo = Ngo.create({:undpuser_id => current_user.id})
         session[:nid]= @ngo.id
    elsif current_user.group_id != 4
    
     flash[:error] = 'you are attempting to access an NGOs only section, you have been redirected back your section'
     verify_user_section
    
    else
       @ngo = Ngo.find_by_undpuser_id(current_user.id)
     
      @ngo_id = @ngo.id
      
   
    
    end
  end
end
