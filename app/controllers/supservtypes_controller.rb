class SupservtypesController < ApplicationController
  
  def index
  
  end

  def new
  end

  def create
    if request.post?
        selected_conservtypes = params[:supservtype]
      if selected_conservtypes.nil? || selected_conservtypes.blank?
        verify_user_section
         return false
        
      end  
      s_id = nil
   selections =[]
   params[:supservtype].each{|sc| selections << {:supservice_id => sc[:supservice_id],:supplier_id=>sc[:supplier_id]} unless sc[:supservice_id].nil?}
   selections.delete_if {|scn| Supservtype.count(:conditions => scn) > 0}
    
      if selections.size <= 0
        flash[:error] = 'You have already selected all the submitted services'
        verify_user_section
         return false
      elsif Supservtype.create(selections)
        flash[:notice] = "Successfully selected Service(s)"
         
      else
        flash[:error] = 'Unable to add your selections '
        
        
      end
    end
    respond_to do |f|
      if identify_vendor.nil?
       f.html{ redirect_to :back;  return false
               }
      else
        f.html{ redirect_to new_supplier_url(:id => params[:supplier_id]) ;  return false
               }
         f.js
      end
    end
    return false
  end

  def destroy
  end

  def edit
  end

  def show
  end

  def update
  end
end
