class AutoPasswordLinkSender
  
  @queue = :auto_link_queue

  def self.perform(undpuser)
    users = Undpuser.select("id, firstname,lastname,email")
    
    users.each do |u|
      session[:u] = u
    params[:e_mail][:email] = u.email
    
    params[:e_mail][:firstname] = u.firstname
    MailMailer.send_email(params[:e_mail]).deliver
    end
  end
end