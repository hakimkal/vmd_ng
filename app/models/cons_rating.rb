# == Schema Information
#
# Table name: cons_ratings
#
#  prj_number                          :string(255)
#  consultant_name                     :string(255)
#  cons_id                             :integer
#  id                                  :integer
#  question_1                          :string(255)
#  question_2                          :string(255)
#  question_3                          :string(255)
#  question_4                          :string(255)
#  question_5                          :string(255)
#  question_6                          :string(255)
#  question_7                          :string(255)
#  consultant_id                       :integer
#  should_this_vendor_remain_on_roster :string(255)
#  evaluated_by                        :string(255)
#  extra_detail                        :text
#  thedate                             :string(255)
#  designation                         :string(255)
#  start_date                          :string(255)
#  end_date                            :string(255)
#  project_number                      :string(255)
#  project_title                       :string(255)
#  project_duty_station                :string(255)
#  project_description                 :text
#  countries_visited                   :string(255)
#  organization                        :string(255)
#  gender                              :string(255)
#  dob                                 :date
#  firstname                           :string(255)
#  lastname                            :string(255)
#  email                               :string(255)
#  created_at                          :datetime
#  updated_at                          :datetime
#

class ConsRating < ActiveRecord::Base
 table_name = "cons_ratings"
end
