# == Schema Information
#
# Table name: c_contact_details
#
#  id             :integer          not null, primary key
#  street_address :text
#  town           :string(255)
#  state          :string(255)
#  country        :string(255)
#  telephone      :string(255)
#  address_type   :string(255)
#  consultant_id  :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class CContactDetail < ActiveRecord::Base
  attr_accessor  :address_equal
  belongs_to :consultant ,:foreign_key => :consultant_id
  attr_accessible :address_type, :consultant_id, :country, :state, :street_address, :telephone, :town ,:address_equal
  
  validates :consultant_id , :presence =>true
  
  before_save :check_record
  
  private
  
  def check_record 
    if new_record?
      ex  = CContactDetail.count(:conditions =>{:consultant_id => consultant_id , :address_type =>address_type, :telephone =>telephone ,:town =>town, :country=>country})
      return false if ex > 0
    end
  end
end
