# == Schema Information
#
# Table name: suppliers
#
#  id                  :integer          not null, primary key
#  name                :string(255)
#  street_address      :string(255)
#  postal_code         :string(255)
#  city                :string(255)
#  country             :string(255)
#  mailing_address     :text
#  parent_company      :text
#  subsidiaries        :string(255)
#  nature_of_business  :string(255)
#  type_of_business    :string(255)
#  doe                 :string(255)
#  number_of_employees :string(255)
#  license_number      :string(255)
#  state_where_regd    :string(255)
#  tax_number          :string(255)
#  dor                 :string(255)
#  working_language    :string(255)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  undpuser_id         :integer
#  state               :string(255)
#

class Supplier < ActiveRecord::Base
  include PublicActivity::Model
  tracked :owner => proc { |controller, model| controller.current_user }
  
  belongs_to :undpuser , :foreign_key => :undpuser_id 
  has_many :supservtypes , :foreign_key => :supplier_id , :dependent => :destroy
  has_many :s_ratings , :foreign_key => :supplier_id , :dependent => :destroy
  has_one :stelephone , :foreign_key => :supplier_id  , :dependent => :destroy
  has_one :s_contact , :foreign_key => :supplier_id , :dependent => :destroy
  has_one :s_financial , :foreign_key => :supplier_id , :dependent => :destroy
  has_one :s_bank_info , :foreign_key => :supplier_id , :dependent => :destroy
  has_one :s_technical_document , :foreign_key => :supplier_id , :dependent => :destroy
  has_one :s_contract, :foreign_key => :supplier_id , :dependent => :destroy
  has_one :s_certification , :foreign_key => :supplier_id , :dependent => :destroy 
 
  attr_accessible :city,:state, :country, :doe,:undpuser_id, :dor, :license_number, :mailing_address, :name, :nature_of_business, :number_of_employees, :parent_company, :postal_code, :state_where_regd, :street_address, :subsidiaries, :tax_number, :type_of_business, :working_language
end
