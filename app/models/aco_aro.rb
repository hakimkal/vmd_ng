# == Schema Information
#
# Table name: aco_aros
#
#  id         :integer          not null, primary key
#  aco_id     :integer
#  aro_id     :integer
#  allow      :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class AcoAro < ActiveRecord::Base
  attr_accessible :aco_id, :allow, :aro_id
  belongs_to :aco
  belongs_to :aro
  validates :aco_id , :aro_id , :allow ,  :presence =>true
  
  
  before_save :check_record
  
  
  def self.get_aco_aro(ar_id,ac_id)
    
    
  ac = AcoAro.find(:first,:conditions => {:aro_id => ar_id,:aco_id =>ac_id})    
    
   if ac.blank?
     return "emp"
     
   else 
     return ac
   end
  end
  
  def check_record
    if new_record?
    if AcoAro.count(:conditions => {:aco_id => aco_id, :aro_id => aro_id }) > 0
      
      return false
    end
    end
  end
end
