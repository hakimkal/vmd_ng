# == Schema Information
#
# Table name: vac_applications
#
#  id          :integer          not null, primary key
#  undpuser_id :integer
#  vacancy_id  :integer
#  vendor_id   :integer
#  vpa_id      :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class VacApplication < ActiveRecord::Base
  include PublicActivity::Model
  tracked :owner => proc { |controller, model| controller.current_user }
  
  belongs_to :undpuser 
  belongs_to :vacancy 
  
  has_many :app_attachments , :dependent =>:destroy,:foreign_key => :application_id 
  has_many :recruitments , :foreign_key => :application_id,:dependent =>:destroy
  attr_accessor :requires_attachment ,:vac_name
  attr_accessible :undpuser_id, :vacancy_id, :vendor_id, :vpa_id,:requires_attachment ,:vac_name
  
  validates :vacancy_id , :presence=>true
  
 
end
