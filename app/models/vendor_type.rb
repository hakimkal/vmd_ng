# == Schema Information
#
# Table name: vendor_types
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class VendorType < ActiveRecord::Base
  has_many :vpas ,:dependent => :destroy 
  has_many :vsses , :dependent => :destroy
  has_many :undpuser
  has_many :vacancy , :foreign_key=> :vendor_id ,:dependent => :destroy
 
  attr_accessible :description, :name
end
