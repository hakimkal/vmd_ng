# == Schema Information
#
# Table name: c_ratings
#
#  id                                  :integer          not null, primary key
#  consultant_id                       :integer
#  project_number                      :string(255)
#  consultant_name                     :string(255)
#  project_title                       :string(255)
#  project_duty_station                :string(255)
#  project_description                 :text
#  start_date                          :string(255)
#  end_date                            :string(255)
#  countries_visited                   :string(255)
#  question_1                          :string(255)
#  question_2                          :string(255)
#  question_3                          :string(255)
#  question_4                          :string(255)
#  question_5                          :string(255)
#  question_6                          :string(255)
#  question_7                          :string(255)
#  extra_detail                        :text
#  should_this_vendor_remain_on_roster :string(255)
#  evaluated_by                        :string(255)
#  organization                        :string(255)
#  designation                         :string(255)
#  thedate                             :string(255)
#  created_at                          :datetime         not null
#  updated_at                          :datetime         not null
#

class CRating < ActiveRecord::Base
  include PublicActivity::Model
  tracked :owner => proc { |controller, model| controller.current_user }
 
  belongs_to :consultant
  attr_accessible :consultant_id, :consultant_name, :countries_visited, :thedate, :designation, :end_date, :evaluated_by, :extra_detail, :organization, :project_description, :project_duty_station, :project_number, 
  :project_title, :question_1, :question_2, :question_3, :question_4, :question_5, :question_6, :question_7,
   :should_this_vendor_remain_on_roster, :start_date 
   
   validates :consultant_id, :consultant_name, :countries_visited, :thedate, :designation, :end_date, :presence =>true
   validates :evaluated_by, :extra_detail, :organization, :project_description, :project_duty_station  ,:presence => true
   validates :project_number,:project_title, :question_1, :question_2, :question_3, :question_4, :question_5 ,:presence =>true
   validates  :question_6, :question_7,:should_this_vendor_remain_on_roster, :start_date , :presence => true
end
