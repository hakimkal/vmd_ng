# == Schema Information
#
# Table name: e_mails
#
#  id                      :integer          not null, primary key
#  subject                 :string(255)
#  message                 :text
#  sender                  :string(255)
#  sender_id               :integer
#  sent_to                 :text
#  cc                      :text
#  bcc                     :text
#  sent_to_ids             :text
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  category                :string(255)
#  severity                :string(255)
#  screenshot_file_name    :string(255)
#  screenshot_content_type :string(255)
#  screenshot_file_size    :integer
#  screenshot_updated_at   :datetime
#  ftype                   :string(255)
#

class EMail < ActiveRecord::Base
  attr_accessor :feedback
  attr_accessible  :ftype, :bcc, :cc,:feedback,:screenshot,:category,:severity, :message, :sender, :sender_id, :sent_to, :sent_to_ids, :subject
has_attached_file :screenshot ,:url => "/assets/feedbacks/screenshot/:id/:style/:basename.:extension",
                             :path =>":rails_root/public/assets/feedbacks/screenshot/:id/:style/:basename.:extension"
  
  validates_attachment_size :screenshot , :less_than => 2.megabytes
 


end
