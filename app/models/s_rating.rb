# == Schema Information
#
# Table name: s_ratings
#
#  id             :integer          not null, primary key
#  supplier_id    :integer
#  supplier_name  :string(255)
#  project_number :string(255)
#  goods_services :text
#  question_1     :string(255)
#  question_2     :string(255)
#  question_3     :string(255)
#  question_4     :string(255)
#  question_5     :string(255)
#  question_6     :text
#  evaluated_by   :string(255)
#  organization   :string(255)
#  designation    :string(255)
#  thedate        :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  project_title  :string(255)
#  start_date     :string(255)
#  end_date       :string(255)
#

class SRating < ActiveRecord::Base
  include PublicActivity::Model
  tracked :owner => proc { |controller, model| controller.current_user }
  belongs_to :supplier
  attr_accessible :designation,:project_title,:start_date,:end_date ,:evaluated_by, :goods_services, :organization, :project_number, :question_1, :question_2, :question_3, :question_4, :question_5, :question_6, :supplier_id, :supplier_name, :thedate
  
  validates  :designation,:start_date,:end_date ,:evaluated_by, :goods_services, :organization, :project_number, :question_1, :question_2, :question_3, :question_4, :question_5,  :supplier_id, :supplier_name, :thedate,:project_title , :presence =>true
  

end
