# == Schema Information
#
# Table name: agency_departments
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text
#  category    :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class AgencyDepartment < ActiveRecord::Base
  has_many :funds ,:foreign_key =>:agency_department_id ,:dependent => :restrict 
  has_many :projects ,:foreign_key =>:agency_department_id ,:dependent => :restrict
  has_many :un_contracts ,:foreign_key =>:agency_department_id ,:dependent => :restrict
 
  attr_accessible :category, :description, :name
  validates :name ,:category ,:presence => :true
  validates :name , :uniqueness => {:case_sensitive=>false}
end
