# == Schema Information
#
# Table name: conservices
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Conservice < ActiveRecord::Base
  attr_accessible :description, :name
  has_many :conservtypes , :dependent => :destroy
  
  validates :name , :presence => true ,
                    :uniqueness => {:case_sensitive=>false}
end
