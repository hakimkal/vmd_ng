# == Schema Information
#
# Table name: n_contacts
#
#  id         :integer          not null, primary key
#  firstname  :string(255)
#  lastname   :string(255)
#  title      :string(255)
#  ngo_id     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class NContact < ActiveRecord::Base
   belongs_to :ngo , :foreign_key => :ngo_id
  attr_accessible :firstname, :lastname, :ngo_id, :title
  validates :ngo_id , :presence => true 
  validates :firstname , :presence => true
  validates :lastname , :presence => true
  validates :title , :presence => true
  
  
   before_save :check_record
  
  def check_record
    
    if new_record?
      ex  = NContact.count(:conditions =>{:ngo_id => ngo_id , :firstname =>firstname , :lastname => lastname , :title =>title})
      return false if ex > 0
    end
    
  end
end
