# == Schema Information
#
# Table name: c_employment_records
#
#  id                                 :integer          not null, primary key
#  from                               :date
#  to                                 :string(255)
#  starting_gross_salary              :string(255)
#  final_gross_salary                 :string(255)
#  functional_title                   :string(255)
#  un_grade                           :string(255)
#  last_un_step                       :string(255)
#  name_of_employer                   :string(255)
#  address_of_employer                :text
#  description_of_duties              :text
#  reason_for_leaving                 :text
#  type_of_business                   :string(255)
#  employment_type                    :string(255)
#  type_of_contract                   :string(255)
#  name_of_supervisor                 :string(255)
#  email_of_supervisor                :string(255)
#  telephone_of_supervisor            :string(255)
#  did_you_supervise_staff            :string(255)
#  number_of_staff_supervised         :string(255)
#  number_of_support_staff_supervised :string(255)
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  consultant_id                      :integer
#  c_employment_detail_id             :integer
#

class CEmploymentRecord < ActiveRecord::Base
  belongs_to :consultant ,:foreign_key => :consultant_id
 # belongs_to :c_employment_detail
  attr_accessible :address_of_employer,:consultant_id, :description_of_duties, :did_you_supervise_staff, :email_of_supervisor, :employment_type, :final_gross_salary
  attr_accessible :from, :functional_title, :last_un_step,:number_of_staff_supervised, :number_of_support_staff_supervised, :reason_for_leaving
  attr_accessible :name_of_employer, :name_of_supervisor,  :starting_gross_salary
  attr_accessible :telephone_of_supervisor,    :to, :type_of_business,    :type_of_contract, :un_grade
  validates :consultant_id ,:presence => true
  #  name_of_employer                   :string(255)
#  address_of_employer                :text
  #validates :address_of_employer ,:presence =>{:message => "Employer Address cannot be blank"}
  validates :name_of_employer ,:presence =>  {:message => " cannot be blank"}
  validates :from ,:presence => {:message => "Missing Employment Date"}
  validates :to ,:presence =>{:message => "Missing Employment Date"}
  
 
  
  def self.calculate_work_experience(consultant_id)
    from_ar = []
    to_ar = []
    cons = CEmploymentRecord.find(:all,:conditions=>{:consultant_id =>consultant_id})
    if cons.blank?
      return false
    end
    cons.each do |c|
    #  from_ar << c.from.strftime("%Y").to_i
     from_ar << Time.parse(c.from.to_s)
      to_ar << c.to
    end
    from_ar = from_ar.uniq
    to_ar = to_ar.uniq
    if to_ar.include?("Till Date")
      
   # to = Time.now.strftime("%Y").to_i  
    to = Time.now.strftime("%d-%m-%Y").to_s
    
    e_d = CEmploymentDetail.find_by_consultant_id(consultant_id)
    if e_d.blank?
      return false
    end
    e_d.experience = (Time.parse(to) - from_ar.uniq.min)/1.day 
    e_d.save  
    
    else
      new_to_ar = []
      to_ar.each do  |t|
        new_to_ar << Time.parse(t)
         
    
      end 
      
      new_to_ar = new_to_ar.uniq
        e_d = CEmploymentDetail.find_by_consultant_id(consultant_id)
       e_d.experience = (new_to_ar.uniq.max - from_ar.uniq.min)/1.day
      e_d.save 
  end
  end
end
