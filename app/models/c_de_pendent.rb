# == Schema Information
#
# Table name: c_de_pendents
#
#  id            :integer          not null, primary key
#  name          :string(255)
#  dob           :date
#  relationship  :string(255)
#  consultant_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class CDePendent < ActiveRecord::Base
   belongs_to :consultant ,:foreign_key=>:consultant_id 

  attr_accessible :consultant_id, :dob, :name, :relationship
end
