# == Schema Information
#
# Table name: s_technical_documents
#
#  id                               :integer          not null, primary key
#  written_statement_policy_status  :string(255)
#  edi_status                       :string(255)
#  supplier_id                      :integer
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  fin_report_file_name             :string(255)
#  fin_report_content_type          :string(255)
#  fin_report_file_size             :integer
#  fin_report_updated_at            :datetime
#  quality_assurance_file_name      :string(255)
#  quality_assurance_content_type   :string(255)
#  quality_assurance_file_size      :integer
#  quality_assurance_updated_at     :datetime
#  intl_representation_file_name    :string(255)
#  intl_representation_content_type :string(255)
#  intl_representation_file_size    :integer
#  intl_representation_updated_at   :datetime
#  goods_services_file_name         :string(255)
#  goods_services_content_type      :string(255)
#  goods_services_file_size         :integer
#  goods_services_updated_at        :datetime
#  written_file_name                :string(255)
#  written_content_type             :string(255)
#  written_file_size                :integer
#  written_updated_at               :datetime
#

class STechnicalDocument < ActiveRecord::Base
  belongs_to :supplier , :foreign_key => :supplier_id
  
  attr_accessible :edi_status, :supplier_id, :written_statement_policy_status ,:fin_report,:quality_assurance, :intl_representation ,:written ,:goods_services
  
  
  has_attached_file :written ,:url => "/assets/suploads/written/:id/:style/:basename.:extension",
                             :path =>":rails_root/public/assets/suploads/written/:id/:style/:basename.:extension"
                             
  has_attached_file :fin_report , :url => "/assets/suploads/fin_report/:id/:style/:basename.:extension",
                              :path =>":rails_root/public/assets/suploads/fin_report/:id/:style/:basename.:extension"
  
  has_attached_file :goods_services , :url => "/assets/suploads/goods_services/:id/:style/:basename.:extension",
                              :path =>":rails_root/public/assets/suploads/goods_services/:id/:style/:basename.:extension"
  
  has_attached_file :quality_assurance , :url => "/assets/suploads/quality_assurance/:id/:style/:basename.:extension",
                              :path =>":rails_root/public/assets/suploads/quality_assurance/:id/:style/:basename.:extension"
  
  has_attached_file :intl_representation , :url => "/assets/suploads/intl_representation/:id/:style/:basename.:extension",
                              :path =>":rails_root/public/assets/suploads/intl_representation/:id/:style/:basename.:extension"
  
   
  validates :supplier_id , :presence => true,
                      :uniqueness => true
                      
  validates_attachment_size :written , :less_than => 4.megabytes
  validates_attachment_size :fin_report , :less_than => 4.megabytes
  validates_attachment_size :goods_services , :less_than => 4.megabytes
  validates_attachment_size :quality_assurance , :less_than => 4.megabytes
  validates_attachment_size :intl_representation , :less_than => 4.megabytes
end
