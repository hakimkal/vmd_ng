# == Schema Information
#
# Table name: funds
#
#  id                   :integer          not null, primary key
#  account_number       :string(255)
#  available_fund_usd   :decimal(, )
#  financial_year_start :string(255)
#  financial_year_end   :string(255)
#  operating_unit       :string(255)
#  approving_officer    :string(255)
#  date_created         :string(255)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  balance              :decimal(, )
#  agency_department_id :integer
#

class Fund < ActiveRecord::Base
  belongs_to :agency_department
  has_many :un_contracts ,:foreign_key =>:fund_id ,:dependent =>:restrict
   has_many :projects ,:foreign_key =>:fund_id ,:dependent =>:restrict
  attr_accessible  :balance,:agency_department_id , :account_number, :approving_officer, :available_fund_usd, :date_created, :financial_year_end, :financial_year_start

  validates :account_number ,:agency_department_id , :available_fund_usd ,:approving_officer, :presence => true
                               
  validates :account_number , :uniqueness => {:case_sensitive=>false}
  
  before_save :set_balance
  
  
  def set_balance
    if new_record?
      
      self.balance = available_fund_usd
     
    elsif !new_record? && self.balance.blank?
      # self.available_fund_usd = self.available_fund_usd + available_fund_usd
      self.balance = available_fund_usd  
      
    elsif !new_record? && !self.balance.blank?
     
    # self.available_fund_usd = self.available_fund_usd + available_fund_usd
     if self.balance > 0
     self.balance = balance + (available_fund_usd - self.balance)
     elsif self.balance < 0
        self.balance = balance + (available_fund_usd  + self.balance)
    
     end
    end
  end
  
       
end
