# == Schema Information
#
# Table name: c_employment_details
#
#  id                                      :integer          not null, primary key
#  object_to_enquiries_of_present_employer :string(255)
#  object_to_enquiries_of_past_employer    :string(255)
#  civil_servant                           :string(255)
#  civil_service_date_from                 :date
#  civil_service_date_to                   :date
#  civil_service_functions_text            :text
#  civil_service_country                   :string(255)
#  created_at                              :datetime         not null
#  updated_at                              :datetime         not null
#  unv                                     :string(255)
#  roster_no                               :string(255)
#  consultant_id                           :integer
#  un_staff                                :string(255)
#  un_staff_index_no                       :string(255)
#  experience                              :decimal(, )
#

class CEmploymentDetail < ActiveRecord::Base
  belongs_to :consultant ,:foreign_key=>:consultant_id  
  #has_many :c_employment_records ,:foreign_key =>:c_id ,:dependent => :destroy
  attr_accessible :experience , :un_staff,:un_staff_index_no, :civil_servant,:roster_no, :consultant_id,:civil_service_country, :civil_service_date_from, :civil_service_date_to, :civil_service_functions_text, :object_to_enquiries_of_past_employer,:unv,:consultant_id, :object_to_enquiries_of_present_employer
  
  validates :consultant_id ,:presence => true,:uniqueness =>{:message => "Duplicates are not allowed, You may attempt to edit the records."}
  validates :object_to_enquiries_of_past_employer,  :presence =>{ :message => " cannot be blank" }
 # validates :unv,  :presence =>{ :message => " cannot be blank" }

end
