# == Schema Information
#
# Table name: statuses
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Status < ActiveRecord::Base
  include PublicActivity::Model
  tracked :owner => proc { |controller, model| controller.current_user }
  has_many :recruitments
  
  attr_accessible :description, :name
  validates :name , :presence => true,
                     :uniqueness => {:case_sensitive=>false}
   
end
