# == Schema Information
#
# Table name: supservtypes
#
#  id            :integer          not null, primary key
#  supplier_id   :integer
#  supservice_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Supservtype < ActiveRecord::Base
  include PublicActivity::Model
  tracked :owner => proc { |controller, model| controller.current_user }
  
  belongs_to :supservice , :foreign_key => :supservice_id
  belongs_to :supplier , :foreign_key => :supplier_id
  attr_accessible :supplier_id, :supservice_id, :undpuser_id
  validates :supservice_id , :presence => true
  validates :supplier_id , :presence => true
end
