# == Schema Information
#
# Table name: n_bank_infos
#
#  id                  :integer          not null, primary key
#  bank_name           :string(255)
#  bank_address        :text
#  bank_swift_code     :text
#  bank_account_number :string(255)
#  bank_account_name   :string(255)
#  ngo_id              :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class NBankInfo < ActiveRecord::Base
   belongs_to :ngo , :foreign_key => :ngo_id
 
  attr_accessible :bank_account_name, :bank_account_number, :bank_address, :bank_name, :bank_swift_code, :ngo_id
  validates :bank_name , :presence => true
  validates :bank_account_name ,:presence =>true 
  validates :bank_swift_code , :presence => true
  
  before_save :check_record
  
  def check_record
    
    if new_record?
      ex  = NBankInfo.count(:conditions =>{:ngo_id => ngo_id })
      return false if ex > 0
    end
    
  end
  
end
