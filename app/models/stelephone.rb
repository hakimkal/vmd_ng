# == Schema Information
#
# Table name: stelephones
#
#  id          :integer          not null, primary key
#  mobile      :string(255)
#  home        :string(255)
#  office      :string(255)
#  fax         :string(255)
#  email       :string(255)
#  website     :string(255)
#  supplier_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Stelephone < ActiveRecord::Base
   belongs_to :supplier , :foreign_key => :supplier_id
 
  attr_accessible :email, :fax, :home, :mobile, :office, :supplier_id, :website
  
  email_format = /\A[\w+\-\d.]+@[a-z\d\-.]+\.[a-z]+\z/i
  
  validates :supplier_id , :presence =>  true
  validates :mobile , :presence => true
  validates   :email ,   :format => {:with => email_format },
                         :uniqueness => {:case_sensitive=>false}
                         
   before_save :check_record
  
  def check_record
    
    if new_record?
      ex  = Stelephone.count(:conditions =>{:supplier_id =>supplier_id , :mobile =>mobile , :email => email , :fax => fax })
      return false if ex > 0
    end
    
  end
             
  
  
end
