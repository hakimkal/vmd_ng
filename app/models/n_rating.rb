# == Schema Information
#
# Table name: n_ratings
#
#  id                     :integer          not null, primary key
#  ngo_name               :string(255)
#  organizational_unit    :string(255)
#  contract_sum           :string(255)
#  project_title          :string(255)
#  contract_number        :string(255)
#  start_date             :string(255)
#  end_date               :string(255)
#  additional_information :text
#  question_1             :text
#  question_2             :text
#  question_3             :text
#  question_4             :text
#  question_5             :text
#  question_6             :text
#  question_7             :string(255)
#  question_8             :string(255)
#  question_9             :string(255)
#  question_10            :string(255)
#  question_11            :string(255)
#  question_12            :text
#  evaluated_by           :string(255)
#  organization           :string(255)
#  designation            :string(255)
#  thedate                :string(255)
#  ngo_id                 :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class NRating < ActiveRecord::Base
  include PublicActivity::Model
  tracked :owner => proc { |controller, model| controller.current_user }
  
  belongs_to :ngo
  attr_accessible :additional_information, :contract_number, :contract_sum, :designation, :end_date, :evaluated_by, :ngo_id, :ngo_name, :organization, :organizational_unit, :project_title, :question_1, :question_10, :question_11, :question_12, :question_2, :question_3, :question_4, :question_5, :question_6, :question_7, :question_8, :question_9, :start_date, :thedate
  validates :additional_information, :contract_number, :contract_sum, :designation, :end_date, :evaluated_by, :ngo_id, :ngo_name, :organization, :organizational_unit, :project_title, :question_1, :question_10, :question_11, :question_12, :question_2, :question_3, :question_4, :question_5, :question_6, :question_7, :question_8, :question_9, :start_date, :thedate , :presence =>true

end
