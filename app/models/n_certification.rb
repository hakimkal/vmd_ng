# == Schema Information
#
# Table name: n_certifications
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  function   :text
#  title      :string(255)
#  certify    :string(255)
#  ngo_id     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class NCertification < ActiveRecord::Base
   belongs_to :ngo , :foreign_key => :ngo_id
  attr_accessible :certify, :function, :name, :ngo_id, :title
  validates :ngo_id , :presence => true ,
                      :uniqueness => true
                      
   validates :name , :presence => true
   validates :certify , :presence =>true
   validates :function , :presence => true
   validates :title , :presence => true

  
  before_save :check_record
  
  def check_record
    
    if new_record?
      ex  = NCertification.count(:conditions =>{:ngo_id => ngo_id})
      return false if ex > 0
    end
    
  end
end
