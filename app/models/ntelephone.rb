# == Schema Information
#
# Table name: ntelephones
#
#  id         :integer          not null, primary key
#  mobile     :string(255)
#  home       :string(255)
#  office     :string(255)
#  fax        :string(255)
#  email      :string(255)
#  website    :string(255)
#  ngo_id     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Ntelephone < ActiveRecord::Base
  belongs_to :ngo , :foreign_key => :ngo_id
  attr_accessible :email, :fax, :home, :mobile, :ngo_id, :office, :website
  email_format = /\A[\w+\-\d.]+@[a-z\d\-.]+\.[a-z]+\z/i
 
  validates :ngo_id , :presence =>  true
  validates :mobile , :presence => true
  validates   :email ,   :presence => true,
                         :format => {:with => email_format },
                         :uniqueness => {:case_sensitive=>false}
                         
   before_save :check_record
  
  def check_record
    
    if new_record?
      ex  = Ntelephone.count(:conditions =>{:ngo_id => ngo_id , :mobile =>mobile , :email => email , :fax => fax })
      return false if ex > 0
    end
    
  end
                 
end
