# == Schema Information
#
# Table name: ctelephones
#
#  id            :integer          not null, primary key
#  home          :string(255)
#  mobile        :string(255)
#  work          :string(255)
#  consultant_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  fax           :string(255)
#

class Ctelephone < ActiveRecord::Base
    belongs_to :consultant ,:foreign_key => :consultant_id
  attr_accessible :consultant_id, :home, :mobile, :work ,:fax
  
  validates :consultant_id , :presence =>true,
                             :uniqueness => true
end
