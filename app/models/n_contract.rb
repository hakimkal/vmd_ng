# == Schema Information
#
# Table name: n_contracts
#
#  id                :integer          not null, primary key
#  organization      :string(255)
#  value             :string(255)
#  year              :string(255)
#  nature_of_service :text
#  contact_details   :text
#  disputes          :text
#  membership_of     :text
#  ngo_id            :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class NContract < ActiveRecord::Base
   belongs_to :ngo , :foreign_key => :ngo_id
  attr_accessible :contact_details, :disputes, :membership_of, :nature_of_service, :ngo_id, :organization, :value, :year
  validates :ngo_id , :presence => true ,
                      :uniqueness => true

 before_save :check_record
  
  def check_record
    
    if new_record?
      ex  = NContract.count(:conditions =>{:ngo_id => ngo_id})
      return false if ex > 0
    end
    
  end

end
