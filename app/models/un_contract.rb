# == Schema Information
#
# Table name: un_contracts
#
#  id                               :integer          not null, primary key
#  project_name                     :string(255)
#  project_ref                      :string(255)
#  department_ref                   :string(255)
#  thedate                          :string(255)
#  contract_type                    :string(255)
#  contract_title                   :string(255)
#  contract_un_reference            :string(255)
#  contract_value_in_usd            :decimal(, )
#  contract_value_in_co_currency    :decimal(, )
#  description_of_goods_or_services :text
#  start_date                       :string(255)
#  end_date                         :string(255)
#  duration                         :decimal(, )
#  organization_awarded             :string(255)
#  location                         :string(255)
#  appointing_agency                :string(255)
#  appointing_agency_contact_person :string(255)
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  fund_id                          :integer
#  project_id                       :integer
#  agency_department_id             :integer
#

class UnContract < ActiveRecord::Base
  belongs_to :project 
  belongs_to :agency_department
  attr_accessible :project_id, :agency_department_id , :appointing_agency_contact_person, :contract_title, :contract_type, :contract_un_reference, :contract_value_in_co_currency, :contract_value_in_usd, :description_of_goods_or_services, :duration, :end_date, :location, :organization_awarded, :start_date, :thedate

 validates :project_id, :agency_department_id , :appointing_agency_contact_person, :contract_title, :contract_type, :contract_un_reference, :contract_value_in_co_currency, :contract_value_in_usd, :description_of_goods_or_services, :duration, :end_date, :location, :organization_awarded, :start_date , :presence =>true
 
 before_save :check_record
 after_save :set_balance

def set_balance
   
   project = Project.find_by_id(self.project_id)
    
    fund = Fund.find_by_id(project.fund_id)
    if fund.balance > 0
    balance = fund.balance - self.contract_value_in_usd 
    fund.balance = balance
    
    elsif fund.balance < 0
      
     balance = fund.balance + self.contract_value_in_usd 
    fund.balance = balance
   
      
   end
    if  fund.save
      return true
     
    end
      
end

def self.check_available_fund(contract)
  
    project = Project.find_by_id(contract[:project_id].to_i)
    
    fund = Fund.find_by_id(project.fund_id)
    if fund.balance > 0 
    balance = fund.balance - contract[:contract_value_in_usd].to_f
    
    else 
      balance = contract[:contract_value_in_usd].to_f * (-1)
    end
    if balance < 0
      return balance
     else
        return  0
    end
      
end
def check_record
  if new_record?
  ex = UnContract.count(:conditions=>
  {
       :project_id => project_id,
       :agency_department_id  => agency_department_id,
       :appointing_agency_contact_person => appointing_agency_contact_person,
       :contract_title => contract_title, 
       :contract_type => contract_type , 
       :contract_un_reference => contract_un_reference, 
       :contract_value_in_co_currency => contract_value_in_co_currency, 
       :contract_value_in_usd =>contract_value_in_usd, 
       :description_of_goods_or_services => description_of_goods_or_services, 
       :duration => duration,
       :end_date => end_date,
       :location => location,
       :organization_awarded => organization_awarded,
       :start_date  => start_date              
      })
      
        return false if ex > 0 
        
      end  
end


def self.get_awarded_id(the_id)
    the_id = the_id.to_i
    @vendor = ConsultantFullname.find_by_id(the_id)
    @vendor_type = 'ConsultantFullname'
    
    if @vendor.blank?
     @vendor = Supplier.find_by_id(the_id)
     @vendor_type = 'Supplier'
      if @vendor.blank?
       @vendor = Ngo.find_by_id(the_id) 
       @vendor_type = 'Ngo'
      end
    end
    
    if @vendor.blank?
      return nil
    else
      @vessel = [@vendor, @vendor_type]
       return  @vessel
    end
  end
end
