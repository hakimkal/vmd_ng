# == Schema Information
#
# Table name: sys_modules
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  alias      :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SysModule < ActiveRecord::Base
  attr_accessible :alias, :name
  has_many :acos
  
  validates :name , :presence => true , :uniqueness => {:case_sensitive => false}
end
