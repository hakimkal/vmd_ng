# == Schema Information
#
# Table name: s_bank_infos
#
#  id                  :integer          not null, primary key
#  bank_name           :string(255)
#  bank_address        :text
#  bank_swift_code     :string(255)
#  bank_account_number :string(255)
#  bank_account_name   :string(255)
#  supplier_id         :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class SBankInfo < ActiveRecord::Base
  attr_accessible :bank_account_name, :bank_account_number, :bank_address, :bank_name, :bank_swift_code, :supplier_id


 validates :supplier_id ,:presence => true ,:uniqueness=>true
  
  
  before_save :check_record
  
  def check_record
    
    if new_record?
      ex  = SBankInfo.count(:conditions =>{:supplier_id => supplier_id })
      return false if ex > 0
    end
    
  end


end
