# == Schema Information
#
# Table name: consultant_reports
#
#  c_fname           :string(255)
#  c_lname           :string(255)
#  consultant_id     :integer
#  id                :integer
#  dob               :date
#  gender            :string(255)
#  birthday          :date
#  c_age             :integer
#  marital_status    :string(255)
#  vendor_id         :integer
#  email             :string(255)
#  firstname         :string(255)
#  lastname          :string(255)
#  undpuser_id       :integer
#  vpa_id            :integer
#  vpa_name          :string(255)
#  vss_id            :integer
#  vss_name          :string(255)
#  service_id        :integer
#  service           :string(255)
#  home              :string(255)
#  mobile            :string(255)
#  contact_id        :integer
#  country           :string(255)
#  resume_file_name  :string(255)
#  roster_no         :string(255)
#  un_staff_index_no :string(255)
#  experience        :decimal(, )
#  qualification     :string(255)
#  c_qual_id         :integer
#  c_post_qual       :string(255)
#  c_post_qual_id    :integer
#

 
class ConsultantReport< ActiveRecord::Base
 table_name = "consultant_reports"
end
