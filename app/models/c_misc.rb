# == Schema Information
#
# Table name: c_miscs
#
#  id                                          :integer          not null, primary key
#  any_dependent                               :string(255)
#  legal_permanent_residence_status            :string(255)
#  legal_permanent_residence_country           :string(255)
#  present_nationality_change_status           :string(255)
#  present_nationality_change_explain          :text
#  family_in_undp_status                       :string(255)
#  ext_family_in_undp_status                   :string(255)
#  accept_employment_for_less_than_six         :string(255)
#  interviewed_for_any_undp_job_in_last_twelve :string(255)
#  interviewed_posts                           :text
#  asat_test_status                            :string(255)
#  asat_date_taken                             :date
#  finance_assessment_test_status              :string(255)
#  finance_assessment_date_taken               :date
#  ever_been_convicted_fined_imprisoned        :string(255)
#  conviction_particulars                      :text
#  disciplinary_measures                       :string(255)
#  disciplinary_particulars                    :text
#  separated_from_service                      :string(255)
#  separation_particulars                      :text
#  consultant_id                               :integer
#  created_at                                  :datetime         not null
#  updated_at                                  :datetime         not null
#

class CMisc < ActiveRecord::Base
  belongs_to :consultant , :foreign_key => :consultant_id

  attr_accessible :accept_employment_for_less_than_six, :any_dependent, :asat_date_taken, :asat_test_status, :consultant_id, :conviction_particulars, :disciplinary_measures, :disciplinary_particulars, :ever_been_convicted_fined_imprisoned, :ext_family_in_undp_status, :family_in_undp_status, :finance_assessment_date_taken, :finance_assessment_test_status, :interviewed_for_any_undp_job_in_last_twelve, :interviewed_posts, :legal_permanent_residence_country, :legal_permanent_residence_status, :present_nationality_change_explain, :present_nationality_change_status, :separated_from_service, :separation_particulars
  validates :consultant_id , :presence => true ,
                             :uniqueness => true
end
