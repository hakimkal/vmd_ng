# == Schema Information
#
# Table name: recruitments
#
#  id                     :integer          not null, primary key
#  vacancy_id             :integer
#  status_id              :integer
#  undpuser_id            :integer
#  job_title              :text
#  vendor_type_id         :integer
#  recruitment_by         :text
#  recruiting_undpuser_id :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  application_id         :integer
#  remark                 :text
#

class Recruitment < ActiveRecord::Base
  include PublicActivity::Model
  tracked :owner => proc { |controller, model| controller.current_user }
  
  belongs_to :undpuser
  belongs_to :status
  belongs_to :vacancy
  belongs_to :vac_application, :foreign_key => :application_id
  attr_accessible :job_title,:application_id, :recruiting_undpuser_id,:remark, :recruitment_by, :status_id, :undpuser_id, :vacancy_id, :vendor_type_id
 validates :undpuser_id , :presence =>true
 validates :vacancy_id , :presence => true
 validates :status_id , :presence => true
 validates :remark , :presence => true
 before_save :check_record
  
  def check_record
    
    if new_record?
      ex  = Recruitment.count(:conditions =>{:undpuser_id => undpuser_id, :vacancy_id => vacancy_id ,
         :status_id => status_id,:application_id => application_id,:vendor_type_id => vendor_type_id , :remark => remark})
      return false if ex > 0
    end
    
  end

end
