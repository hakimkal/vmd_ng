# == Schema Information
#
# Table name: projects
#
#  id                   :integer          not null, primary key
#  name                 :string(255)
#  agency_department_id :integer
#  thedate              :string(255)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  fund_id              :integer
#

class Project < ActiveRecord::Base
  belongs_to :agency_department
  belongs_to :fund
  attr_accessible  :fund_id , :agency_department_id, :name, :thedate
  
  validates :agency_department_id ,:name ,:fund_id, :presence =>true
  validates :name , :uniqueness => {:case_sensitive=>false}
end
