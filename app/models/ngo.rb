# == Schema Information
#
# Table name: ngos
#
#  id                      :integer          not null, primary key
#  name                    :string(255)
#  street_address          :string(255)
#  city                    :string(255)
#  state                   :string(255)
#  postal_code             :string(255)
#  country                 :string(255)
#  mailing_address         :text
#  parent_ngo              :text
#  subsidiaries            :text
#  objectives              :text
#  doe                     :string(255)
#  num_of_employees        :string(255)
#  license_number          :string(255)
#  state_where_regd        :string(255)
#  dor                     :string(255)
#  working_languages       :text
#  undpuser_id             :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  registration_type       :string(255)
#  registration_type_other :string(255)
#

class Ngo < ActiveRecord::Base
  include PublicActivity::Model
  tracked :owner => proc { |controller, model| controller.current_user }
  
  belongs_to :undpuser ,:foreign_key => :undpuser_id
  has_one :ntelephone , :foreign_key => :ngo_id , :dependent =>:destroy
  has_one :n_contact , :foreign_key => :ngo_id , :dependent => :destroy
  
  has_one :n_financial , :foreign_key => :ngo_id , :dependent => :destroy
  has_one :n_bank_info , :foreign_key => :ngo_id , :dependent => :destroy
  has_one :n_technical_document , :foreign_key => :ngo_id , :dependent => :destroy
  has_one :n_contract , :foreign_key => :ngo_id , :dependent => :destroy
  has_one :n_certification , :foreign_key => :ngo_id , :dependent => :destroy
  has_many :ngo_services , :foreign_key => :ngo_id , :dependent =>:destroy
  has_many :n_ratings , :foreign_key => :ngo_id , :dependent =>:destroy
  attr_accessible :city, :country, :doe,:registration_type_other, :registration_type,:dor, :license_number, :mailing_address, :name, :num_of_employees, :objectives, :parent_ngo, :postal_code, :state, :state_where_regd, :street_address, :subsidiaries, :undpuser_id, :working_languages
  validates :undpuser_id , :presence =>true ,
                           :uniqueness => true
                           
                          
end
