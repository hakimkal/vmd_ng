# == Schema Information
#
# Table name: aros
#
#  id          :integer          not null, primary key
#  group_id    :integer
#  undpuser_id :integer
#  alias       :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Aro < ActiveRecord::Base
  belongs_to :undpuser 
  has_many  :aco_aro 
  attr_accessible :alias, :group_id, :undpuser_id
  
   validates :alias ,:presence =>true ,:uniqueness => {:case_sensitive=>false}
  
  def self.build_aro(user)
     u = Undpuser.find(user.id)
     ar = []
     aro =  Aro.new
     aro.group_id = u.group_id 
     aro.undpuser_id = u.id
     aro.alias = u.username
     
     if aro.save
       ar << [aro.alias ,aro.undpuser_id, "s"]
       
     else
       ar << [aro.alias ,aro.undpuser_id, "f"]
       
     end
     return ar
  end
  
  def self.update_aro(user)
     
       
     aro = Aro.find_by_undpuser_id(user.id)
     
 
     if !aro or aro.blank?
       Aro.build_aro(user)
     end
     u = Undpuser.find(user.id)
    ar =  []
     aro.group_id = u.group_id 
     aro.undpuser_id = u.id
     aro.alias = u.username
     
    if aro.save
       ar << [aro.alias ,aro.undpuser_id, "s"]
       
     else
       ar << [aro.alias ,aro.undpuser_id, "f"]
       
     end
     return ar
     
  end
  
  def self.auto_populate_aros
    
    #setup groups
    
    groups = Group.select("id,name")
    
    groups.each do |g|
     aro  = Aro.new
     aro.group_id = g.id
     aro.alias = g.name.downcase
    if aro.save 
     # return aro
    end
    
    users = Undpuser.find(:all)
    
    users.each do |u|
      
      Aro.build_aro(u) unless Aro.count(:conditions=>{:undpuser_id => u.id}) > 0
    end
   end
   
   
  end
end
