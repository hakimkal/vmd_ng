# == Schema Information
#
# Table name: c_certifications
#
#  id            :integer          not null, primary key
#  certify       :integer
#  consultant_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class CCertification < ActiveRecord::Base
  belongs_to :consultant ,:foreign_key=>:consultant_id 
  attr_accessible :certify, :consultant_id
  validates :consultant_id , :presence =>true,
                             :uniqueness => true
  validates :certify , :presence => true
                       #:numericality =>{:equal_to => 1 ,:only_integer =>true}
                        
end
