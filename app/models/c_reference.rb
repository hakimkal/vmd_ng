# == Schema Information
#
# Table name: c_references
#
#  id            :integer          not null, primary key
#  firstname     :string(255)
#  lastname      :string(255)
#  street        :text
#  town          :string(255)
#  state         :string(255)
#  country       :string(255)
#  email         :string(255)
#  mobile        :string(255)
#  home          :string(255)
#  fax           :string(255)
#  consultant_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  work          :string(255)
#

class CReference < ActiveRecord::Base
  belongs_to :consultant  , :foreign_key => :consultant_id
  attr_accessible :country, :email, :fax,:consultant_id, :work,:firstname, :home, :lastname, :mobile, :state, :street, :town
  email_format = /\A[\w+\-\d.]+@[a-z\d\-.]+\.[a-z]+\z/i
 
   validates   :email ,  :presence=>true, :format => {:with => email_format }
   validates  :consultant_id , :presence =>true
                        


  before_save :check_record
  
  private
  
   
 


  def check_record 
    if new_record?
      ex  = CReference.count(:conditions =>{
        :consultant_id => consultant_id , 
        :firstname => firstname ,
        :lastname => lastname ,
        :home=> home , 
        :state => state ,
        :street => street ,
        :work=>work,:country=>country, 
        :email=>email ,
        :mobile=>mobile,
        :town => town ,
        :fax =>fax ,
        :state =>state ,
        :country => country
        })
      return false if ex > 0 
    end
  end
end
