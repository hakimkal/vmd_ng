# == Schema Information
#
# Table name: n_financials
#
#  id            :integer          not null, primary key
#  year_1        :string(255)
#  year_1_amount :string(255)
#  year_2        :string(255)
#  year_2_amount :string(255)
#  year_3        :string(255)
#  year_3_amount :string(255)
#  ngo_id        :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class NFinancial < ActiveRecord::Base
  belongs_to :ngo , :foreign_key => :ngo_id
  attr_accessible :ngo_id, :year_1, :year_1_amount, :year_2, :year_2_amount, :year_3, :year_3_amount
  validates :ngo_id ,:presence => true ,:uniqueness=>true
  
  
  before_save :check_record
  
  def check_record
    
    if new_record?
      ex  = NFinancial.count(:conditions =>{:ngo_id => ngo_id })
      return false if ex > 0
    end
    
  end
end
