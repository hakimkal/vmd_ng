# == Schema Information
#
# Table name: acos
#
#  id            :integer          not null, primary key
#  action        :string(255)
#  alias         :string(255)
#  sys_module_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Aco < ActiveRecord::Base
   has_many  :aco_aro ,:dependent => :destroy
  attr_accessible :action, :alias, :sys_module_id
  
  belongs_to :sys_module 
 
  before_save :check_record
  
  after_save :insert_newest
  
  
  def self.auto_populate_acos
    ac = []
    @modules = SysModule.select("id,name")
    
    @modules.each do |m|
    aco = Aco.new
    
    aco.action = 'new'
    aco.alias = 'create'
    aco.sys_module_id = m.id
     
     if aco.save
       ac <<   [m.name,m.id,"s"] 
       
     else
        ac <<   [m.name,m.id,"f"] 
    end
    aco = Aco.new
    
     aco.action = 'edit'
    aco.alias = 'edit'
    aco.sys_module_id = m.id
    
     if aco.save
       ac <<   [m.name,m.id,"s"] 
       
     else
        ac <<   [m.name,m.id,"f"] 
    end
    aco = Aco.new
    
     aco.action = 'destroy'
    aco.alias = 'delete'
    aco.sys_module_id = m.id
    
    
     if aco.save
       ac <<   [m.name,m.id,"s"] 
       
     else
        ac <<   [m.name,m.id,"f"] 
    end
    
    aco = Aco.new
    
     aco.action = 'index'
    aco.alias = 'read'
    aco.sys_module_id = m.id
     if aco.save
       ac <<   [m.name,m.id,"s"] 
       
     else
        ac <<   [m.name,m.id,"f"] 
    end
    #aco = Aco.new
    
    #aco.action = 'show'
    #aco.alias = 'view'
    #aco.sys_module_id = m.id
    # if aco.save
    #   ac <<   [m.name,m.id,"s"] 
       
    # else
    #    ac <<   [m.name,m.id,"f"] 
    #end
   
    end
    
    return ac
  end
  
  def insert_newest
    
    aco_aros = AcoAro.select("Distinct aro_id ")
    aco_aros.each do |aro|
     aco_aro = AcoAro.new({:aco_id => self.id,  :aro_id => aro.aro_id ,   :allow => "No"  })
      
      aco_aro.save
    end
    
  end
  def check_record 
    if new_record?
    ex = Aco.count(:conditions => {:action => action, :sys_module_id =>sys_module_id}) 
    return false if ex > 0
    end
      
      
  end
end
