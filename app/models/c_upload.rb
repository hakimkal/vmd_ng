# == Schema Information
#
# Table name: c_uploads
#
#  id                  :integer          not null, primary key
#  consultant_id       :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  photo_file_name     :string(255)
#  photo_content_type  :string(255)
#  photo_file_size     :integer
#  photo_updated_at    :datetime
#  resume_file_name    :string(255)
#  resume_content_type :string(255)
#  resume_file_size    :integer
#  resume_updated_at   :datetime
#

class CUpload < ActiveRecord::Base
  belongs_to :consultant ,:foreign_key => :consultant_id 
  attr_accessible :consultant_id ,:photo , :resume
  has_attached_file :photo ,:styles => { :medium => "300x300>", :thumb => "100x100>" } ,
                             :url => "/assets/cuploads/photos/:id/:style/:basename.:extension",
                             :path =>":rails_root/public/assets/cuploads/photos/:id/:style/:basename.:extension"
  has_attached_file :resume , :url => "/assets/cuploads/resumes/:id/:style/:basename.:extension",
                              :path =>":rails_root/public/assets/cuploads/resumes/:id/:style/:basename.:extension"
   
 validates :consultant_id , :presence =>true , :uniqueness =>true
 #validates_attachment_presence :photo
 validates_attachment_size :photo , :less_than => 1.megabytes
  validates_attachment_size :resume , :less_than => 4.megabytes
 validates_attachment_content_type  :photo ,:content_type => ['image/jpeg','image/jpg','image/png']
end
