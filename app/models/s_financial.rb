# == Schema Information
#
# Table name: s_financials
#
#  id              :integer          not null, primary key
#  year1_ti        :string(255)
#  year1_ti_amount :string(255)
#  year2_ti        :string(255)
#  year2_ti_amount :string(255)
#  year3_ti        :string(255)
#  year3_ti_amount :string(255)
#  year1_es        :string(255)
#  year1_es_value  :string(255)
#  year2_es        :string(255)
#  year2_es_value  :string(255)
#  year3_es        :string(255)
#  year3_es_value  :string(255)
#  supplier_id     :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class SFinancial < ActiveRecord::Base
  belongs_to :supplier , :foreign_key => :supplier_id
  
  attr_accessible :supplier_id, :year1_es, :year1_es_value, :year1_ti, :year1_ti_amount, :year2_es, :year2_es_value, :year2_ti, :year2_ti_amount, :year3_ti, :year3_es, :year3_es_value, :year3_ti_amount
  validates :supplier_id ,:presence => true ,:uniqueness=>true
  
  
  before_save :check_record
  
  def check_record
    
    if new_record?
      ex  = SFinancial.count(:conditions =>{:supplier_id => supplier_id })
      return false if ex > 0
    end
    
end
end
