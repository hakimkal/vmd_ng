# == Schema Information
#
# Table name: s_contracts
#
#  id                :integer          not null, primary key
#  organization      :string(255)
#  contract_amount   :string(255)
#  year              :string(255)
#  goods_or_services :text
#  destination       :string(255)
#  contact_details   :text
#  export_countries  :text
#  disputes          :string(255)
#  membership_of     :string(255)
#  supplier_id       :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class SContract < ActiveRecord::Base
   belongs_to :supplier , :foreign_key => :supplier_id
  attr_accessible :contact_details, :contract_amount, :destination, :disputes, :export_countries, :goods_or_services, :membership_of, :organization, :supplier_id, :year

 validates :supplier_id , :presence => true ,
                      :uniqueness => true

 before_save :check_record
  
  def check_record
    
    if new_record?
      ex  = SContract.count(:conditions =>{:supplier_id => supplier_id})
      return false if ex > 0
    end
    
  end


end
