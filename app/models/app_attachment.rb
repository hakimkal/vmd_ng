# == Schema Information
#
# Table name: app_attachments
#
#  id                  :integer          not null, primary key
#  application_id      :integer
#  undpuser_id         :integer
#  file_1_desc         :text
#  file_2_desc         :text
#  file_3_desc         :text
#  file_4_desc         :text
#  file_5_desc         :text
#  vacancy_id          :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  file_1_file_name    :string(255)
#  file_1_content_type :string(255)
#  file_1_file_size    :integer
#  file_1_updated_at   :datetime
#  file_2_file_name    :string(255)
#  file_2_content_type :string(255)
#  file_2_file_size    :integer
#  file_2_updated_at   :datetime
#  file_3_file_name    :string(255)
#  file_3_content_type :string(255)
#  file_3_file_size    :integer
#  file_3_updated_at   :datetime
#  file_4_file_name    :string(255)
#  file_4_content_type :string(255)
#  file_4_file_size    :integer
#  file_4_updated_at   :datetime
#  file_5_file_name    :string(255)
#  file_5_content_type :string(255)
#  file_5_file_size    :integer
#  file_5_updated_at   :datetime
#

class AppAttachment < ActiveRecord::Base
  belongs_to :vac_application ,:foreign_key => :application_id
  attr_accessible :application_id, :file_1,:file_2,:file_3,:file_4,:file_5, :file_1_desc, :file_2_desc, :file_3_desc, :file_4_desc, :file_5_desc, :undpuser_id, :vacancy_id
 
 has_attached_file :file_1 , :url => "/assets/app_attachments/attachments/:id/:style/:basename.:extension",
                              :path =>":rails_root/public/assets/app_attachments/attachments/:id/:style/:basename.:extension"
   
 has_attached_file :file_2 , :url => "/assets/app_attachments/attachments/:id/:style/:basename.:extension",
                              :path =>":rails_root/public/assets/app_attachments/attachments/:id/:style/:basename.:extension"
 
 has_attached_file :file_3 , :url => "/assets/app_attachments/attachments/:id/:style/:basename.:extension",
                              :path =>":rails_root/public/assets/app_attachments/attachments/:id/:style/:basename.:extension"
 
 has_attached_file :file_4 , :url => "/assets/app_attachments/attachments/:id/:style/:basename.:extension",
                              :path =>":rails_root/public/assets/app_attachments/attachments/:id/:style/:basename.:extension"
  
 has_attached_file :file_5 , :url => "/assets/app_attachments/attachments/:id/:style/:basename.:extension",
                              :path =>":rails_root/public/assets/app_attachments/attachments/:id/:style/:basename.:extension"
 
 
 
 validates_attachment_size :file_1 , :less_than => 3.megabytes
 validates_attachment_size :file_2 , :less_than => 3.megabytes
 validates_attachment_size :file_3 , :less_than => 3.megabytes
 validates_attachment_size :file_4 , :less_than => 3.megabytes
 validates_attachment_size :file_5 , :less_than => 3.megabytes



end
