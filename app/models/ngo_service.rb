# == Schema Information
#
# Table name: ngo_services
#
#  id         :integer          not null, primary key
#  ngo_id     :integer
#  vss_id     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class NgoService < ActiveRecord::Base
  belongs_to  :ngo , :foreign_key => :ngo_id
  belongs_to :vss , :foreign_key => :vss_id
  attr_accessible :ngo_id, :vss_id
  validates :ngo_id , :presence =>true
  validates :vss_id , :presence => true
  
  before_save :check_record
  
  def check_record
    
    if new_record?
      ex  = NgoService.count(:conditions =>{:ngo_id => ngo_id , :vss_id =>vss_id})
      return false if ex > 0
    end
    
  end
end
