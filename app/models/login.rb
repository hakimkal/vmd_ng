# == Schema Information
#
# Table name: logins
#
#  id               :integer          not null, primary key
#  user_id          :integer
#  time_in          :datetime
#  time_out         :datetime
#  session_duration :integer
#  username         :string(255)
#  fullname         :string(255)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Login < ActiveRecord::Base
   
  belongs_to :undpuser ,:foreign_key => :user_id 
  attr_accessible :session_duration, :time_in, :time_out, :user_id ,:username , :fullname
  
  validates :time_in , :presence => true 
  validates :user_id , :presence => true 
  validates :username , :presence => true 
  validates :fullname , :presence => true
end
