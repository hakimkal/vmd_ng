# == Schema Information
#
# Table name: n_technical_documents
#
#  id                              :integer          not null, primary key
#  written_statement_policy_status :string(255)
#  ngo_id                          :integer
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#  financial_file_name             :string(255)
#  financial_content_type          :string(255)
#  financial_file_size             :integer
#  financial_updated_at            :datetime
#  job_evidence_file_name          :string(255)
#  job_evidence_content_type       :string(255)
#  job_evidence_file_size          :integer
#  job_evidence_updated_at         :datetime
#  written_file_name               :string(255)
#  written_content_type            :string(255)
#  written_file_size               :integer
#  written_updated_at              :datetime
#

class NTechnicalDocument < ActiveRecord::Base
   belongs_to :ngo , :foreign_key => :ngo_id
 
  attr_accessible :ngo_id, :written_statement_policy_status , :written , :job_evidence , :financial
  
  has_attached_file :written ,:url => "/assets/nuploads/written/:id/:style/:basename.:extension",
                             :path =>":rails_root/public/assets/nuploads/written/:id/:style/:basename.:extension"
  has_attached_file :job_evidence , :url => "/assets/nuploads/job_evidence/:id/:style/:basename.:extension",
                              :path =>":rails_root/public/assets/nuploads/job_evidence/:id/:style/:basename.:extension"
  
  has_attached_file :financial , :url => "/assets/nuploads/financial/:id/:style/:basename.:extension",
                              :path =>":rails_root/public/assets/nuploads/financial/:id/:style/:basename.:extension"
   
  validates :ngo_id , :presence => true,
                      :uniqueness => true
                      
  validates_attachment_size :written , :less_than => 4.megabytes
  validates_attachment_size :job_evidence , :less_than => 4.megabytes
  validates_attachment_size :financial , :less_than => 4.megabytes
end
