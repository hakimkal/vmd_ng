# == Schema Information
#
# Table name: c_languages
#
#  id                :integer          not null, primary key
#  language          :string(255)
#  read_status       :string(255)
#  write_status      :string(255)
#  speak_status      :string(255)
#  understand_status :string(255)
#  consultant_id     :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class CLanguage < ActiveRecord::Base
   belongs_to :consultant ,:foreign_key=>:consultant_id 

  attr_accessible :consultant_id, :language, :read_status, :speak_status, :understand_status, :write_status
end
