# == Schema Information
#
# Table name: consultants
#
#  id                         :integer          not null, primary key
#  undpuser_id                :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  other_name                 :string(255)
#  dob                        :date
#  place_of_birth             :string(255)
#  nationality_at_birth       :string(255)
#  gender                     :string(255)
#  marital_status             :string(255)
#  disability_from_air_travel :string(255)
#  firstname                  :string(255)
#  lastname                   :string(255)
#  disability_describe        :string(255)
#  nationalities              :string(255)
#

class Consultant < ActiveRecord::Base
  include PublicActivity::Model
  tracked :owner => proc { |controller, model| controller.current_user }
  
  belongs_to :undpuser ,:foreign_key => :undpuser_id
  has_many :c_ratings ,:foreign_key =>:consultant_id ,:dependent =>:destroy
  has_many :c_contact_details ,:foreign_key => :consultant_id ,:dependent =>:destroy
  has_many :c_references ,:foreign_key => :consultant_id ,:dependent =>:destroy
  has_many :cun_languages ,:foreign_key => :consultant_id ,:dependent =>:destroy
  has_many :c_publications ,:foreign_key => :consultant_id ,:dependent =>:destroy
  has_many :cundp_certifications ,:foreign_key => :consultant_id ,:dependent =>:destroy
  has_many :c_educations ,:foreign_key => :consultant_id ,:dependent =>:destroy
  has_many :c_post_qualifications , :foreign_key => :consultant_id , :dependent => :destroy
  has_many :c_pro_societies , :foreign_key => :consultant_id , :dependent => :destroy
  has_many :c_employment_records ,:foreign_key => :consultant_id, :dependent => :destroy
 
  has_many :c_family_in_undps , :foreign_key=> :consultant_id ,:dependent => :destroy
  has_many :c_languages , :foreign_key => :consultant_id , :dependent => :destroy
  has_many :c_ext_family_in_undps , :foreign_key => :consultant_id , :dependent => :destroy
  has_many :c_de_pendents , :foreign_key => :consultant_id , :dependent => :destroy
  has_one :c_certification ,:foreign_key => :consultant_id ,:dependent =>:destroy
  has_one :c_employment_detail , :dependent =>:destroy
  has_one :c_misc , :foreign_key=>:consultant_id ,:dependent =>  :destroy 
  has_one :ctelephone , :foreign_key => :consultant_id ,:dependent =>:destroy
  has_one :c_upload ,:foreign_key => :consultant_id , :dependent => :destroy
  attr_accessible :undpuser_id ,:other_name ,:dob ,:place_of_birth ,:disability_describe,:nationalities,:nationality_at_birth,:gender ,:marital_status,:disability_from_air_travel,:firstname,:lastname
  
  validates :undpuser_id , :presence =>true ,
                           :uniqueness=>true
end
