# == Schema Information
#
# Table name: cun_languages
#
#  id              :integer          not null, primary key
#  institution     :string(255)
#  place           :string(255)
#  country         :string(255)
#  qualification   :string(255)
#  course_of_study :string(255)
#  study_type      :string(255)
#  consultant_id   :integer
#  attended_from   :string(255)
#  attended_to     :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class CunLanguage < ActiveRecord::Base
  belongs_to :consultant ,:foreign_key=>:consultant_id 
  attr_accessible :attended_from, :attended_to, :consultant_id, :country, :course_of_study, :institution, :place, :qualification, :study_type
end
