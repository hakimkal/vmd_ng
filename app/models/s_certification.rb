# == Schema Information
#
# Table name: s_certifications
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  function    :string(255)
#  title       :string(255)
#  certify     :string(255)
#  supplier_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class SCertification < ActiveRecord::Base
  belongs_to :supplier , :foreign_key => :supplier_id
  attr_accessible :certify, :function, :name, :supplier_id, :title
  
  
   validates :supplier_id , :presence => true ,
                      :uniqueness => true
                      
   validates :name , :presence => true
   validates :certify , :presence =>true
   validates :function , :presence => true
   validates :title , :presence => true

  
  before_save :check_record
  
  def check_record
    
    if new_record?
      ex  = SCertification.count(:conditions =>{:supplier_id => supplier_id})
      return false if ex > 0
    end
    
  end
  
end
