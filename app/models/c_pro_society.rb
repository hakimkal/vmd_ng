# == Schema Information
#
# Table name: c_pro_societies
#
#  id            :integer          not null, primary key
#  name          :string(255)
#  details       :text
#  consultant_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class CProSociety < ActiveRecord::Base
  belongs_to :consultant ,:foreign_key=>:consultant_id  
  attr_accessible :consultant_id, :details, :name
end
