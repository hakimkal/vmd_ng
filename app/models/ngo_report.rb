# == Schema Information
#
# Table name: ngo_reports
#
#  name                :string(255)
#  id                  :integer
#  ngo_id              :integer
#  city                :string(255)
#  country             :string(255)
#  vendor_id           :integer
#  undpuser_id         :integer
#  username            :string(255)
#  firstname           :string(255)
#  lastname            :string(255)
#  email               :string(255)
#  vpa_id              :integer
#  vpa_name            :string(255)
#  vss_id              :integer
#  vss_name            :string(255)
#  ngo_financial_id    :integer
#  ngo_contract_id     :integer
#  ngo_document_id     :integer
#  financial_file_name :string(255)
#  ngo_evidence        :string(255)
#  written_file_name   :string(255)
#

class NgoReport< ActiveRecord::Base
 table_name = "ngo_reports"
end
