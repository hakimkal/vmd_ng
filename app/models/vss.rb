# == Schema Information
#
# Table name: vsses
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  desc           :string(255)
#  vendor_type_id :integer
#  vpa_id         :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Vss < ActiveRecord::Base
  include PublicActivity::Model
  tracked :owner => proc { |controller, model| controller.current_user }
  
  belongs_to :vendor_type  ,:foreign_key => :vendor_type_id 
  belongs_to :vpa , :foreign_key => :vpa_id
   has_one :vacancy ,:dependent => :nullify 
  has_one :undpuser
  has_many :ngo_services , :foreign_key => :vss_id
  attr_accessible :desc, :name, :vendor_type_id, :vpa_id
  validates :name , :presence =>true
  validates :vendor_type_id , :presence => true
  validate :vpa_id  , :presence => true
  
  
  before_save :check_record
  
  def check_record
    
    if new_record?
      ex  = Vss.count(:conditions =>{:name =>name , :vendor_type_id =>vendor_type_id , :vpa_id => vpa_id  })
      return false if ex > 0
    end
    
  end
end
