# == Schema Information
#
# Table name: c_ext_family_in_undps
#
#  id            :integer          not null, primary key
#  name          :string(255)
#  relationship  :string(255)
#  name_of_unit  :string(255)
#  duty_station  :string(255)
#  consultant_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class CExtFamilyInUndp < ActiveRecord::Base
   belongs_to :consultant ,:foreign_key=>:consultant_id 

  attr_accessible :consultant_id, :duty_station, :name, :name_of_unit, :relationship
end
