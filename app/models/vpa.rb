# == Schema Information
#
# Table name: vpas
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  desc           :string(255)
#  vendor_type_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Vpa < ActiveRecord::Base
  include PublicActivity::Model
  tracked :owner => proc { |controller, model| controller.current_user }
  
  belongs_to :vendor_type ,:foreign_key => :vendor_type_id 
  has_many :vsses ,:dependent => :nullify , :foreign_key => :vpa_id
  has_one :undpuser
   has_one :vacancy ,:dependent => :nullify 
   attr_accessible :desc, :name, :vendor_type_id
   validates :name , :presence =>true
   validates :vendor_type_id , :presence => true
end
