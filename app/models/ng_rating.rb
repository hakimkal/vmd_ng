# == Schema Information
#
# Table name: ng_ratings
#
#  id                     :integer
#  question_1             :text
#  question_2             :text
#  question_3             :text
#  question_4             :text
#  question_5             :text
#  question_6             :text
#  question_7             :string(255)
#  question_8             :string(255)
#  question_9             :string(255)
#  question_10            :string(255)
#  question_11            :string(255)
#  question_12            :text
#  ngo_id                 :integer
#  organizational_unit    :string(255)
#  name                   :string(255)
#  evaluated_by           :string(255)
#  thedate                :string(255)
#  designation            :string(255)
#  contract_number        :string(255)
#  start_date             :string(255)
#  end_date               :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#  organization           :string(255)
#  contract_sum           :string(255)
#  ngo_name               :string(255)
#  project_title          :string(255)
#  additional_information :text
#

class NgRating < ActiveRecord::Base
 table_name = "ng_ratings"
end
