# == Schema Information
#
# Table name: sp_ratings
#
#  id             :integer
#  question_1     :string(255)
#  question_2     :string(255)
#  question_3     :string(255)
#  question_4     :string(255)
#  question_5     :string(255)
#  question_6     :text
#  supplier_id    :integer
#  supplier_name  :string(255)
#  name           :string(255)
#  project_number :string(255)
#  evaluated_by   :string(255)
#  thedate        :string(255)
#  designation    :string(255)
#  start_date     :string(255)
#  end_date       :string(255)
#  goods_services :text
#  organization   :string(255)
#  project_title  :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#

class SpRating < ActiveRecord::Base
 table_name = "sp_ratings"
end
