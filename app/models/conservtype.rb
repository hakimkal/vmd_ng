# == Schema Information
#
# Table name: conservtypes
#
#  id            :integer          not null, primary key
#  consultant_id :integer
#  conservice_id :integer
#  undpuser_id   :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Conservtype < ActiveRecord::Base
  belongs_to :conservice , :foreign_key =>  :conservice_id
  validates :conservice_id , :presence => true
  attr_accessible :conservice_id, :consultant_id, :undpuser_id
end
