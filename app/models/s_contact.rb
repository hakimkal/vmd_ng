# == Schema Information
#
# Table name: s_contacts
#
#  id          :integer          not null, primary key
#  firstname   :string(255)
#  lastname    :string(255)
#  title       :string(255)
#  supplier_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class SContact < ActiveRecord::Base
  belongs_to :supplier , :foreign_key => :supplier_id
 
  attr_accessible :firstname, :lastname, :supplier_id, :title
  
  
  validates :supplier_id , :presence => true 
  validates :firstname , :presence => true
  validates :lastname , :presence => true
  validates :title , :presence => true
  
  
   before_save :check_record
  
  def check_record
    
    if new_record?
      ex  = SContact.count(:conditions =>{:supplier_id => supplier_id , :firstname =>firstname , :lastname => lastname , :title =>title})
      return false if ex > 0
    end
    
  end
end
