# == Schema Information
#
# Table name: supplier_reports
#
#  name                          :string(255)
#  supplier_id                   :integer
#  id                            :integer
#  city                          :string(255)
#  country                       :string(255)
#  vendor_id                     :integer
#  email                         :string(255)
#  firstname                     :string(255)
#  lastname                      :string(255)
#  undpuser_id                   :integer
#  vpa_id                        :integer
#  vpa_name                      :string(255)
#  vss_id                        :integer
#  vss_name                      :string(255)
#  sfinancial_id                 :integer
#  s_contract_id                 :integer
#  s_document_id                 :integer
#  fin_report_file_name          :string(255)
#  quality_assurance_file_name   :string(255)
#  intl_representation_file_name :string(255)
#  goods_services_file_name      :string(255)
#  written_file_name             :string(255)
#  supservtype_id                :integer
#  service                       :string(255)
#

 

class SupplierReport< ActiveRecord::Base
 table_name = "supplier_reports"
end
