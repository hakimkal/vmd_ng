module UnContractsHelper
  
  def get_awarded(the_id)
    the_id = the_id.to_i
    @vendor = ConsultantFullname.find_by_id(the_id)
    
    if @vendor.blank?
     @vendor = Supplier.find_by_id(the_id)
      if @vendor.blank?
       @vendor = Ngo.find_by_id(the_id) 
      end
    end
    
    if @vendor.blank?
      return nil
    else
       return @vendor.name
    end
  end
  
  def get_id(option = {})
    contract = nil
    if option[:contract_title]
    
    contract = UnContract.find_by_contract_title(option[:contract_title]) 
    
    
    end
    return contract.id
  end
end
