require 'open-uri'
module SessionsHelper
  
  def sign_in user 
    cookies.permanent.signed[:remember_token] = [user.id,user.salt] 
    
    current_user = user 
    
     
     
    
   
  end
  
  def current_user=(user)
    @current_user
  end
  
  def current_user
    @current_user ||= user_from_session
  end
  
  def identify_vendor=(get_vendor_id)
    @identify_vendor
 end
 
 def identify_vendor
   @identify_vendor ||= get_vendor_id
 end
  def signed_in?
    !current_user.nil?
  end
  
  def sign_out(login_id) 
    
    logout_audit(login_id)
    cookies.delete(:remember_token)
    cookies.delete(:login_id)
    self.current_user =nil
    reset_session
  end
  
  def login_audit user
    
    #user_id          :integer
    #  time_in          :datetime
    #  time_out         :datetime
   #  session_duration :string(255)
   @t = Login.new
    @t.user_id = user.id
    @t.time_in = Time.zone.now
    @t.time_out = nil
    @t.session_duration = 0
    @t.username = user.username
    @t.fullname = user.firstname + " "+ user.lastname
    @t.save
    return @t.id
    
    
  end
  
  def logout_audit(login_id)
    if signed_in?
    #
   @audit_created = Login.find_by_id(login_id) 
   return if @audit_created.blank?
   @audit_created.time_out = Time.zone.now 
   #a = DateTime.new(2009, 10, 13, 12, 0, 0) - DateTime.new(2009, 10, 11, 0, 0, 0)
   @audit_created.session_duration = (((@audit_created.time_out - @audit_created.time_in)))
   if @audit_created.save
    session.delete(:login_id) 
   end
   end
  end
  
  private
  
  def user_from_session
   user_from_remember_from_token
  end
  def user_from_remember_from_token
    Undpuser.authenticate_with_salt(*remember_token)
  end
  
  def remember_token
    cookies.signed[:remember_token] ||= [nil,nil]
  end
  
  
  def internet_connection?
  begin
    true if
    open("http://www.google.com/")
   rescue
    flash[:warning] = "We are unable to connect to our twitter page at this time."  
    false
  end
 end
  
 def verify_user_section 
   
  case current_user.group_id
  when 1..3
      
     redirect_to dashboard_path
     
   
   
   when 4
     if current_user.vendor_id == 1
      
      redirect_to chome_path
      
      elsif current_user.vendor_id == 2
      redirect_to nhome_path
      
      elsif current_user.vendor_id == 3
       redirect_to shome_path
      elsif current_user.vendor_id.blank?
        flash[:notice] = "Select vendor category"
      redirect_to vendor_select_undpuser_url current_user
    end 
  end  
  end
  
  def vendors_only
    if signed_in?
      if current_user.group_id == 4
        return true
      else
        flash[:error] = "That section is restricted to Vendors Only!!!"
        verify_user_section
        return false
      end
    end
  end
  def is_vendor?(user_id = nil)
    if user_id.nil?
      return session[:vendor_user]  
    end
    user = Undpuser.find(user_id)
    if user.group_id == 4
        case user.vendor_id
        when 1
          @vendor = Consultant.find_by_undpuser_id(user_id)
         
        when 2
           @vendor = Ngo.find_by_undpuser_id(user_id)
        when 3
           @vendor = Supplier.find_by_undpuser_id(user_id)
        end
    
      
      if @vendor.blank? || @vendor.nil? 
         case user.vendor_id 
           when 1
            @vendor = Consultant.new({:undpuser_id => user_id})
            @vendor.save
           when 2
            @vendor = Ngo.new({:undpuser_id => user_id})
            @vendor.save 
           when 3
              @vendor = Supplier.new({:undpuser_id => user_id})
              @vendor.save
             
          when nil
            
           @vendor = nil
      end
     end
      if @vendor.nil?
        return 0
         
      else
         session[:vendor_user] = @vendor.id
        return @vendor.id
      end
  end
  end
  def get_vendor_id
    
    case current_user.vendor_id.to_i 
      when 1
      @vendor = Consultant.find_by_undpuser_id(current_user.id)
       
      when 2 
      @vendor = Ngo.find_by_undpuser_id(current_user.id)
       
      when 3
      @vendor = Supplier.find_by_undpuser_id(current_user.id)
      
      when nil
       
        
      
        @vendor = nil 
        
    end
   
     if !@vendor.nil?
      
       return @vendor.id 
    else
      
      return nil
    end
   
 end
 
 def username_is?(undpuser= nil)
   if !undpuser.to_i.nil?
   @user = Undpuser.find_by_id(undpuser.to_i)
   end
   if !@user
     return "Unidentified User"
   end
   if !@user.username.nil? && !@user.usertype.nil?
    return @user.username + "::" + @user.usertype.upcase
    else
      return "Unidentified User"
    end
 end
 
def get_user_layout 
   if !signed_in?
     return 'application'
   end
 case current_user.group_id
  when 1..3
      
      return 'dashboard'
     
   
     
  when 4
     if current_user.vendor_id == 1
      
     return    'usershome'
      
     elsif current_user.vendor_id == 2
     return 'ngouser'
      
      elsif current_user.vendor_id == 3
      return  'supplieruser'
      
      elsif current_user.vendor_id.nil?
        return 'application' 
    end
    
     
  end  
 end
 
 def time_length seconds
   days = (seconds/1.day).floor
   seconds -= days.days
   hours = (seconds/1.hour).floor
   seconds -= hours.hours
   minutes = (seconds/1.minute).floor
   seconds -= minutes.minutes
   
    if (minutes <= 0 && hours <=0 && days <=0 )
      seconds.to_s + " secs"
      ## problem noticed when time_in ad time_out are within same hours so temporatrily i removed the 23 hour
    elsif (minutes > 0  && hours == 23 && days <= 0)
   minutes.to_s + " mins " + seconds.to_s + " secs"
   elsif(minutes > 0 && hours > 0 && hours <23  && days <= 0)
     hours.to_s + " hrs " + minutes.to_s + " mins " + seconds.to_s + " secs"
   elsif(minutes > 0  && hours >=24 && days > 0)
     days.to_s + " days"
   end 
 end
 
 def generate_password
   alphabet_s = ["a","b","c","d","e","f","g","h","j","k","m","n","p","q","r","s","t","u","v","w","x","y","z"]
   alphabet_c = ["A","B","C","D","E","F","G","H","J","K","L","M","N","P","Q","R","S","T","U","V","W","X","Y","Z"]
   n_mbers = ["0","1","3","4","5","6","7","8","9"]
    v= r = Random.new
     
   c_i = r.rand(0..23)
   c_i_i = v.rand(1..24)
   c_i_n = r.rand(0..7)
   
   pwod = ""
   3.times do
   pwod += alphabet_s[r.rand(0..22)] + alphabet_c[v.rand(1..22)] + n_mbers[c_i_n] 
  
    end
     
     
    pwod =pwod.chop
   session[:p_wod] = pwod
   return pwod
 end
 
 def get_time_in_string? tint = 0 
   
     tint = tint.to_i
     years = 0
     month = 0
     m = 0
     days = 0
     twords = "" 
    
   
   years = (tint / 365)
   
   month = (tint % 365) 
     
   if month >= 30
   m = month / 30
   days = month % 30 
   else
     days  = month
   end
   
   if(years > 0 && m > 0 && days  > 0)
     twords +=  "#{years} years , #{m} months and #{days} days."
     
  elsif years > 0 && m > 0 && days  <= 0
   
   twords +=  "#{years} years  and #{m} months."
  
   elsif years > 0 && m <= 0 && days  > 0
   
   twords +=  "#{years} years  and #{days} days."
  
  elsif years > 0 && m <= 0 && days  <=0
   
   twords +=  "#{years} years."
    
   elsif years == 0 && m > 0 && days  > 0
   
   twords +=  "#{m} months  and #{days} days."
   
   elsif years ==  0 && m == 0 && days  > 0
   
   twords +=  "#{days} days."
   
   end
    
   return twords
   
 end
end