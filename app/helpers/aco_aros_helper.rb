module AcoArosHelper
  
  def is_admin?(params)
    
     if  params[:group_id].to_i == 1
        
        return true 
        
    elsif params[:undpuser_id]
       user = Undpuser.find_by_id params[:undpuser_id]
        if user.group_id == 1
      return true
      end
    
    elsif params[:aro_id]
     aro = Aro.find_by_id(params[:aro_id].to_i)
     user = Undpuser.find_by_id(aro.undpuser_id)
    
     if !user.blank?  
      if user.group_id == 1
      return true
      end
   end
      
   
     
  
      
    else
      return nil
     
    end
  end
  
  def is_logged_in_user_admin?(user)
    
    if user.group_id.to_i == 1
      return true
    else
      return nil
    end
    
  end
  
  def is_allowed?(user,action,controller)
    
    if user.group_id.to_i == 4
      return true
      
    elsif user.group_id.to_i == 1
     
     return true
     
    elsif user.group_id.to_i == 2 or user.group_id.to_i == 3
          #check Aco
          aco = is_aco?(user,action,controller)
          
          if aco
               aro = Aro.find_by_undpuser_id(user.id)
               #check for user
              aco_Aro = AcoAro.find_by_aro_id(aro.id)
              if aco_Aro.blank?
                #empty user, check for group
             aro = Aro.find(:first,:conditions => {:group_id => user.group_id, "undpuser_id" => nil  })
            
              end
               
                
              

           
                 if user_has_rights?(aro.id,aco.id)
                   
                 return true
                
                else
                
                return false
                
                end
          else
            return true
          end  
    end
    
  end
   
    def is_aco?(user,action,controller)
      
      mod = SysModule.find_by_name(controller.capitalize)
      
      if mod.blank?
        return   false
       
       else
         aco = Aco.find(:first ,:conditions => {:sys_module_id =>mod.id, :action => action})
        
            if aco
              return aco
             
             else
             return false
          
             end  
      end
    end
    
    def user_has_rights?(aro_id,aco_id)
      
         aco_aro = AcoAro.find(:first , :conditions => {:aro_id=>aro_id,:aco_id => aco_id})
         
        if aco_aro
             if aco_aro.allow == "Yes"
              return true
             else 
              return false
            end
      
       else
        return true
    end
end
end