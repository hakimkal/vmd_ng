require 'spec_helper'

describe SupplieruserController do

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'supplieruser'" do
    it "returns http success" do
      get 'supplieruser'
      response.should be_success
    end
  end

end
