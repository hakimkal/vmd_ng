# == Schema Information
#
# Table name: vacancies
#
#  id                         :integer          not null, primary key
#  name                       :string(255)
#  vendor_id                  :integer
#  vpa_id                     :string(255)
#  vss_id                     :string(255)
#  location                   :string(255)
#  deadline                   :string(255)
#  contract_type              :string(255)
#  language_required          :string(255)
#  starting_date              :string(255)
#  end_date                   :string(255)
#  duration_of_contract       :string(255)
#  job_background             :text
#  job_duties                 :text
#  job_deliverables           :text
#  technical_competence       :text
#  corporate_competence       :text
#  functional_competence      :text
#  management                 :text
#  required_skills_experience :text
#  file_1_description         :string(255)
#  file_2_description         :string(255)
#  file_3_description         :string(255)
#  file_4_description         :string(255)
#  file_5_description         :string(255)
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  file_1_file_name           :string(255)
#  file_1_content_type        :string(255)
#  file_1_file_size           :integer
#  file_1_updated_at          :datetime
#  file_2_file_name           :string(255)
#  file_2_content_type        :string(255)
#  file_2_file_size           :integer
#  file_2_updated_at          :datetime
#  file_3_file_name           :string(255)
#  file_3_content_type        :string(255)
#  file_3_file_size           :integer
#  file_3_updated_at          :datetime
#  file_4_file_name           :string(255)
#  file_4_content_type        :string(255)
#  file_4_file_size           :integer
#  file_4_updated_at          :datetime
#  file_5_file_name           :string(255)
#  file_5_content_type        :string(255)
#  file_5_file_size           :integer
#  file_5_updated_at          :datetime
#  status                     :integer          default(0)
#  requires_attachment        :integer
#  minimum_attachment         :integer
#

require 'spec_helper'

describe Vacancy do
  pending "add some examples to (or delete) #{__FILE__}"
end
