# == Schema Information
#
# Table name: suppliers
#
#  id                  :integer          not null, primary key
#  name                :string(255)
#  street_address      :string(255)
#  postal_code         :string(255)
#  city                :string(255)
#  country             :string(255)
#  mailing_address     :text
#  parent_company      :text
#  subsidiaries        :string(255)
#  nature_of_business  :string(255)
#  type_of_business    :string(255)
#  doe                 :string(255)
#  number_of_employees :string(255)
#  license_number      :string(255)
#  state_where_regd    :string(255)
#  tax_number          :string(255)
#  dor                 :string(255)
#  working_language    :string(255)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  undpuser_id         :integer
#  state               :string(255)
#

require 'spec_helper'

describe Supplier do
  pending "add some examples to (or delete) #{__FILE__}"
end
