# == Schema Information
#
# Table name: c_employment_records
#
#  id                                 :integer          not null, primary key
#  from                               :date
#  to                                 :string(255)
#  starting_gross_salary              :string(255)
#  final_gross_salary                 :string(255)
#  functional_title                   :string(255)
#  un_grade                           :string(255)
#  last_un_step                       :string(255)
#  name_of_employer                   :string(255)
#  address_of_employer                :text
#  description_of_duties              :text
#  reason_for_leaving                 :text
#  type_of_business                   :string(255)
#  employment_type                    :string(255)
#  type_of_contract                   :string(255)
#  name_of_supervisor                 :string(255)
#  email_of_supervisor                :string(255)
#  telephone_of_supervisor            :string(255)
#  did_you_supervise_staff            :string(255)
#  number_of_staff_supervised         :string(255)
#  number_of_support_staff_supervised :string(255)
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  consultant_id                      :integer
#  c_employment_detail_id             :integer
#

require 'spec_helper'

describe CEmploymentRecord do
  pending "add some examples to (or delete) #{__FILE__}"
end
