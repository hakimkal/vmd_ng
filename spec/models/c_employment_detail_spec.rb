# == Schema Information
#
# Table name: c_employment_details
#
#  id                                      :integer          not null, primary key
#  object_to_enquiries_of_present_employer :string(255)
#  object_to_enquiries_of_past_employer    :string(255)
#  civil_servant                           :string(255)
#  civil_service_date_from                 :date
#  civil_service_date_to                   :date
#  civil_service_functions_text            :text
#  civil_service_country                   :string(255)
#  created_at                              :datetime         not null
#  updated_at                              :datetime         not null
#  unv                                     :string(255)
#  roster_no                               :string(255)
#  consultant_id                           :integer
#  un_staff                                :string(255)
#  un_staff_index_no                       :string(255)
#  experience                              :decimal(, )
#

require 'spec_helper'

describe CEmploymentDetail do
  pending "add some examples to (or delete) #{__FILE__}"
end
