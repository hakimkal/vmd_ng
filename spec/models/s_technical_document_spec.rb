# == Schema Information
#
# Table name: s_technical_documents
#
#  id                               :integer          not null, primary key
#  written_statement_policy_status  :string(255)
#  edi_status                       :string(255)
#  supplier_id                      :integer
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  fin_report_file_name             :string(255)
#  fin_report_content_type          :string(255)
#  fin_report_file_size             :integer
#  fin_report_updated_at            :datetime
#  quality_assurance_file_name      :string(255)
#  quality_assurance_content_type   :string(255)
#  quality_assurance_file_size      :integer
#  quality_assurance_updated_at     :datetime
#  intl_representation_file_name    :string(255)
#  intl_representation_content_type :string(255)
#  intl_representation_file_size    :integer
#  intl_representation_updated_at   :datetime
#  goods_services_file_name         :string(255)
#  goods_services_content_type      :string(255)
#  goods_services_file_size         :integer
#  goods_services_updated_at        :datetime
#  written_file_name                :string(255)
#  written_content_type             :string(255)
#  written_file_size                :integer
#  written_updated_at               :datetime
#

require 'spec_helper'

describe STechnicalDocument do
  pending "add some examples to (or delete) #{__FILE__}"
end
