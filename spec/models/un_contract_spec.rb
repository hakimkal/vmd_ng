# == Schema Information
#
# Table name: un_contracts
#
#  id                               :integer          not null, primary key
#  project_name                     :string(255)
#  project_ref                      :string(255)
#  department_ref                   :string(255)
#  thedate                          :string(255)
#  contract_type                    :string(255)
#  contract_title                   :string(255)
#  contract_un_reference            :string(255)
#  contract_value_in_usd            :decimal(, )
#  contract_value_in_co_currency    :decimal(, )
#  description_of_goods_or_services :text
#  start_date                       :string(255)
#  end_date                         :string(255)
#  duration                         :decimal(, )
#  organization_awarded             :string(255)
#  location                         :string(255)
#  appointing_agency                :string(255)
#  appointing_agency_contact_person :string(255)
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  fund_id                          :integer
#  project_id                       :integer
#  agency_department_id             :integer
#

require 'spec_helper'

describe UnContract do
  pending "add some examples to (or delete) #{__FILE__}"
end
