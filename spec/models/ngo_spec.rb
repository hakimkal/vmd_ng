# == Schema Information
#
# Table name: ngos
#
#  id                      :integer          not null, primary key
#  name                    :string(255)
#  street_address          :string(255)
#  city                    :string(255)
#  state                   :string(255)
#  postal_code             :string(255)
#  country                 :string(255)
#  mailing_address         :text
#  parent_ngo              :text
#  subsidiaries            :text
#  objectives              :text
#  doe                     :string(255)
#  num_of_employees        :string(255)
#  license_number          :string(255)
#  state_where_regd        :string(255)
#  dor                     :string(255)
#  working_languages       :text
#  undpuser_id             :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  registration_type       :string(255)
#  registration_type_other :string(255)
#

require 'spec_helper'

describe Ngo do
  pending "add some examples to (or delete) #{__FILE__}"
end
