class CreateVacApplications < ActiveRecord::Migration
  def change
    create_table :vac_applications do |t|
      t.integer :undpuser_id
      t.integer :vacancy_id
      t.integer :vendor_id
      t.integer :vpa_id

      t.timestamps
    end
  end
end
