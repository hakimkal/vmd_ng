class AddAttachmentFile1File2File3File4File5ToAppAttachments < ActiveRecord::Migration
  def self.up
    change_table :app_attachments do |t|
      t.has_attached_file :file_1
      t.has_attached_file :file_2
      t.has_attached_file :file_3
      t.has_attached_file :file_4
      t.has_attached_file :file_5
    end
  end

  def self.down
    drop_attached_file :app_attachments, :file_1
    drop_attached_file :app_attachments, :file_2
    drop_attached_file :app_attachments, :file_3
    drop_attached_file :app_attachments, :file_4
    drop_attached_file :app_attachments, :file_5
  end
end
