class CreateVpas < ActiveRecord::Migration
  def change
    create_table :vpas do |t|
      t.string :name
      t.string :desc
      t.integer :vendor_type_id

      t.timestamps
    end
  end
end
