class CreateSCertifications < ActiveRecord::Migration
  def change
    create_table :s_certifications do |t|
      t.string :name
      t.string :function
      t.string :title
      t.string :certify
      t.integer :supplier_id

      t.timestamps
    end
  end
end
