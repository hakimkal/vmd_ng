class RenameFieldTypeFtypeEMails < ActiveRecord::Migration
  def up
    rename_column :e_mails, :type, :ftype 
  end

  def down
  end
end
