class CreateCEmploymentDetails < ActiveRecord::Migration
  def change
    create_table :c_employment_details do |t|
      t.string :object_to_enquiries_of_present_employer
      t.string :object_to_enquiries_of_past_employer
      t.string :civil_servant
      t.date :civil_service_date_from
      t.date :civil_service_date_to
      t.string :civil_service_functions_text
      t.string :civil_service_country

      t.timestamps
    end
  end
end
