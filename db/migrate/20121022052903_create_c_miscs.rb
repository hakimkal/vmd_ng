class CreateCMiscs < ActiveRecord::Migration
  def change
    create_table :c_miscs do |t|
      t.string :any_dependent
      t.string :legal_permanent_residence_status
      t.string :legal_permanent_residence_country
      t.string :present_nationality_change_status
      t.text :present_nationality_change_explain
      t.string :family_in_undp_status
      t.string :ext_family_in_undp_status
      t.string :accept_employment_for_less_than_six
      t.string :interviewed_for_any_undp_job_in_last_twelve
      t.text :interviewed_posts
      t.string :asat_test_status
      t.date :asat_date_taken
      t.string :finance_assessment_test_status
      t.date :finance_assessment_date_taken
      t.string :ever_been_convicted_fined_imprisoned
      t.text :conviction_particulars
      t.string :disciplinary_measures
      t.text :disciplinary_particulars
      t.string :separated_from_service
      t.text :separation_particulars
      t.integer :consultant_id

      t.timestamps
    end
  end
end
