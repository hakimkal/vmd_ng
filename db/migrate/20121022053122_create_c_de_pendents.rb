class CreateCDePendents < ActiveRecord::Migration
  def change
    create_table :c_de_pendents do |t|
      t.string :name
      t.date :dob
      t.string :relationship
      t.integer :consultant_id

      t.timestamps
    end
  end
end
