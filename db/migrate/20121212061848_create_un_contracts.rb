class CreateUnContracts < ActiveRecord::Migration
  def change
    create_table :un_contracts do |t|
      t.string :project_name
      t.string :project_ref
      t.string :department_ref
      t.string :thedate
      t.string :contract_type
      t.string :contract_title
      t.string :contract_un_reference
      t.decimal :contract_value_in_usd
      t.decimal :contract_value_in_co_currency
      t.text :description_of_goods_or_services
      t.string :start_date
      t.string :end_date
      t.decimal :duration
      t.string :organization_awarded
      t.string :location
      t.string :appointing_agency
      t.string :appointing_agency_contact_person

      t.timestamps
    end
  end
end
