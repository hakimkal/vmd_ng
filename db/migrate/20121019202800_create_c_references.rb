class CreateCReferences < ActiveRecord::Migration
  def change
    create_table :c_references do |t|
      t.string :firstname
      t.string :lastname
      t.text :street
      t.string :town
      t.string :state
      t.string :country
      t.string :email
      t.string :mobile
      t.string :home
      t.string :fax
      t.integer :consultant_id

      t.timestamps
    end
  end
end
