class ChangeColumnTypeForCEmploymentDetails < ActiveRecord::Migration
  def up
     change_column :c_employment_details ,:civil_service_functions_text , :text
  end

  def down
  end
end
