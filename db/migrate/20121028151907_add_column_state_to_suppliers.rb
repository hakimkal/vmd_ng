class AddColumnStateToSuppliers < ActiveRecord::Migration
  def change
    add_column :suppliers, :state, :string
  end
end
