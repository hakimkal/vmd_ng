class AddColumnsToVacancies < ActiveRecord::Migration
  def change
    add_column :vacancies, :requires_attachment, :integer
    add_column :vacancies, :minimum_attachment, :integer
  end
end
