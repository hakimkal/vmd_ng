class AddDateColumnsToSRatings < ActiveRecord::Migration
  def change
    add_column :s_ratings, :start_date, :string
    add_column :s_ratings, :end_date, :string
  end
end
