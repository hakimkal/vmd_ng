class AddColumnsToCEmploymentRecords < ActiveRecord::Migration
  def change
    add_column :c_employment_records, :consultant_id, :integer
    add_column :c_employment_records, :c_employment_detail_id, :integer
  end
end
