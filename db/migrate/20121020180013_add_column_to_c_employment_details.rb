class AddColumnToCEmploymentDetails < ActiveRecord::Migration
  def change
    add_column :c_employment_details, :unv, :string
    add_column :c_employment_details, :roster_no, :string
  end
end
