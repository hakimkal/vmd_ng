class CreateAcoAros < ActiveRecord::Migration
  def change
    create_table :aco_aros do |t|
      t.integer :aco_id
      t.integer :aro_id
      t.string :allow

      t.timestamps
    end
  end
end
