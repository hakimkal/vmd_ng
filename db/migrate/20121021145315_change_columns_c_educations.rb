class ChangeColumnsCEducations < ActiveRecord::Migration
  def up
    change_column :c_educations ,:attended_from , :string
    change_column :c_educations,:attended_to , :string
  end

  def down
  end
end
