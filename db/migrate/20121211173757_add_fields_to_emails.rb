class AddFieldsToEmails < ActiveRecord::Migration
  def change
    add_column :e_mails, :category, :string
    add_column :e_mails, :severity, :string
  end
end
