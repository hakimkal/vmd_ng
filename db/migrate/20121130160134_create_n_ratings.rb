class CreateNRatings < ActiveRecord::Migration
  def change
    create_table :n_ratings do |t|
      t.string :ngo_name
      t.string :organizational_unit
      t.string :contract_sum
      t.string :project_title
      t.string :contract_number
      t.string :start_date
      t.string :end_date
      t.text :additional_information
      t.text :question_1
      t.text :question_2
      t.text :question_3
      t.text :question_4
      t.text :question_5
      t.text :question_6
      t.string :question_7
      t.string :question_8
      t.string :question_9
      t.string :question_10
      t.string :question_11
      t.text :question_12
      t.string :evaluated_by
      t.string :organization
      t.string :designation
      t.string :thedate
      t.integer :ngo_id

      t.timestamps
    end
  end
end
