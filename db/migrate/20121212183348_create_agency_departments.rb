class CreateAgencyDepartments < ActiveRecord::Migration
  def change
    create_table :agency_departments do |t|
      t.string :name
      t.text :description
      t.string :category

      t.timestamps
    end
  end
end
