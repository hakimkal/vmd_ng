class CreateLogins < ActiveRecord::Migration
  def change
   create_table :logins do |t|
    t.integer :user_id
      t.timestamp :time_in
      t.timestamp :time_out
      t.integer :session_duration
      t.string :username 
      t.string :fullname
      
      t.timestamps
     
   end

  end
 
end
