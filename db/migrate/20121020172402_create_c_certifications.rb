class CreateCCertifications < ActiveRecord::Migration
  def change
    create_table :c_certifications do |t|
      t.integer :certify
      t.integer :consultant_id

      t.timestamps
    end
  end
end
