class AddAttachmentFinReportQualityAssuranceIntlRepresentationGoodsServicesWrittenToSTechnicalDocuments < ActiveRecord::Migration
  def self.up
    change_table :s_technical_documents do |t|
      t.has_attached_file :fin_report
      t.has_attached_file :quality_assurance
      t.has_attached_file :intl_representation
      t.has_attached_file :goods_services
      t.has_attached_file :written
    end
  end

  def self.down
    drop_attached_file :s_technical_documents, :fin_report
    drop_attached_file :s_technical_documents, :quality_assurance
    drop_attached_file :s_technical_documents, :intl_representation
    drop_attached_file :s_technical_documents, :goods_services
    drop_attached_file :s_technical_documents, :written
  end
end
