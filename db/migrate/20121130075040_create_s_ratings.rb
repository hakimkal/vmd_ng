class CreateSRatings < ActiveRecord::Migration
  def change
    create_table :s_ratings do |t|
      t.integer :supplier_id
      t.string :supplier_name
      t.string :project_number
      t.text :goods_services
      t.string :question_1
      t.string :question_2
      t.string :question_3
      t.string :question_4
      t.string :question_5
      t.text :question_6
      t.string :evaluated_by
      t.string :organization
      t.string :designation
      t.string :thedate

      t.timestamps
    end
  end
end
