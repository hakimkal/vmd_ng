class AddAttachmentFile1File2File3File4File5ToVacancies < ActiveRecord::Migration
  def self.up
    change_table :vacancies do |t|
      t.has_attached_file :file_1
      t.has_attached_file :file_2
      t.has_attached_file :file_3
      t.has_attached_file :file_4
      t.has_attached_file :file_5
    end
  end

  def self.down
    drop_attached_file :vacancies, :file_1
    drop_attached_file :vacancies, :file_2
    drop_attached_file :vacancies, :file_3
    drop_attached_file :vacancies, :file_4
    drop_attached_file :vacancies, :file_5
  end
end
