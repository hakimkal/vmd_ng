class CreateSContacts < ActiveRecord::Migration
  def change
    create_table :s_contacts do |t|
      t.string :firstname
      t.string :lastname
      t.string :title
      t.integer :supplier_id

      t.timestamps
    end
  end
end
