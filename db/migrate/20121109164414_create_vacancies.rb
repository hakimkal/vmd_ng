class CreateVacancies < ActiveRecord::Migration
  def change
    create_table :vacancies do |t|
      t.string :name
      t.integer :vendor_id
      t.string :vpa_id
      t.string :vss_id
      t.string :location
      t.string :deadline
      t.string :contract_type
      t.string :language_required
      t.string :starting_date
      t.string :end_date
      t.string :duration_of_contract
      t.text :job_background
      t.text :job_duties
      t.text :job_deliverables
      t.text :technical_competence
      t.text :corporate_competence
      t.text :functional_competence
      t.text :management
      t.text :required_skills_experience
      t.string :file_1_description
      t.string :file_2_description
      t.string :file_3_description
      t.string :file_4_description
      t.string :file_5_description

      t.timestamps
    end
  end
end
