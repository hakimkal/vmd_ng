class CreateCExtFamilyInUndps < ActiveRecord::Migration
  def change
    create_table :c_ext_family_in_undps do |t|
      t.string :name
      t.string :relationship
      t.string :name_of_unit
      t.string :duty_station
      t.integer :consultant_id

      t.timestamps
    end
  end
end
