class CreateConsultants < ActiveRecord::Migration
  def change
    create_table :consultants do |t|
      t.integer :undpuser_id

      t.timestamps
    end
  end
end
