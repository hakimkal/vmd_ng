class CreateCProSocieties < ActiveRecord::Migration
  def change
    create_table :c_pro_societies do |t|
      t.string :name
      t.text :details
      t.integer :consultant_id

      t.timestamps
    end
  end
end
