class AddAttachmentPhotoResumeToCUploads < ActiveRecord::Migration
  def self.up
    change_table :c_uploads do |t|
      t.has_attached_file :photo
      t.has_attached_file :resume
    end
  end

  def self.down
    drop_attached_file :c_uploads, :photo
    drop_attached_file :c_uploads, :resume
  end
end
