class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :name
      t.integer :agency_department_id
      t.string :thedate

      t.timestamps
    end
  end
end
