class AddAttachmentFinancialJobEvidenceWrittenToNTechnicalDocuments < ActiveRecord::Migration
  def self.up
    change_table :n_technical_documents do |t|
      t.has_attached_file :financial
      t.has_attached_file :job_evidence
      t.has_attached_file :written
    end
  end

  def self.down
    drop_attached_file :n_technical_documents, :financial
    drop_attached_file :n_technical_documents, :job_evidence
    drop_attached_file :n_technical_documents, :written
  end
end
