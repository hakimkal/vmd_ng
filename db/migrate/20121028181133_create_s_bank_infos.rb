class CreateSBankInfos < ActiveRecord::Migration
  def change
    create_table :s_bank_infos do |t|
      t.string :bank_name
      t.text :bank_address
      t.string :bank_swift_code
      t.string :bank_account_number
      t.string :bank_account_name
      t.integer :supplier_id

      t.timestamps
    end
  end
end
