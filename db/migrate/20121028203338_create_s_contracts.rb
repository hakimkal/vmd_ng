class CreateSContracts < ActiveRecord::Migration
  def change
    create_table :s_contracts do |t|
      t.string :organization
      t.string :contract_amount
      t.string :year
      t.text :goods_or_services
      t.string :destination
      t.text :contact_details
      t.text :export_countries
      t.string :disputes
      t.string :membership_of
      t.integer :supplier_id

      t.timestamps
    end
  end
end
