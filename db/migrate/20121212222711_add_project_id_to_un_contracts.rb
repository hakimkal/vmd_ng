class AddProjectIdToUnContracts < ActiveRecord::Migration
  def change
    add_column :un_contracts, :project_id, :integer
    add_column :un_contracts, :agency_department_id, :integer
  end
end
