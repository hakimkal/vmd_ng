class CreateCRatings < ActiveRecord::Migration
  def change
    create_table :c_ratings do |t|
      t.integer :consultant_id
      t.string :project_number
      t.string :consultant_name
      t.string :project_title
      t.string :project_duty_station
      t.text :project_description
      t.string :start_date
      t.string :end_date
      t.string :countries_visited
      t.string :question_1
      t.string :question_2
      t.string :question_3
      t.string :question_4
      t.string :question_5
      t.string :question_6
      t.string :question_7
      t.text :extra_detail
      t.string :should_this_vendor_remain_on_roster
      t.string :evaluated_by
      t.string :organization
      t.string :designation
      t.string :date

      t.timestamps
    end
  end
end
