class CreateCPostQualifications < ActiveRecord::Migration
  def change
    create_table :c_post_qualifications do |t|
      t.string :institution
      t.string :place
      t.string :country
      t.string :qualification
      t.string :course_of_study
      t.string :study_type
      t.integer :consultant_id
      t.string :attended_from
      t.string :attended_to

      t.timestamps
    end
  end
end
