class CreateAcos < ActiveRecord::Migration
  def change
    create_table :acos do |t|
      t.string :action
      t.string :alias
      t.integer :sys_module_id

      t.timestamps
    end
  end
end
