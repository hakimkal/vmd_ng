class CreateSupservtypes < ActiveRecord::Migration
  def change
    create_table :supservtypes do |t|
      t.integer :supplier_id
      t.integer :supservice_id
      t.integer :undpuser_id

      t.timestamps
    end
  end
end
