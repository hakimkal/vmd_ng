class AddColumnNationalitiesToConsultants < ActiveRecord::Migration
  def change
    add_column :consultants, :nationalities, :string
  end
end
