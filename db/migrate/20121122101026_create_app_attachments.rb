class CreateAppAttachments < ActiveRecord::Migration
  def change
    create_table :app_attachments do |t|
      t.integer :application_id
      t.integer :undpuser_id
      t.text :file_1_desc
      t.text :file_2_desc
      t.text :file_3_desc
      t.text :file_4_desc
      t.text :file_5_desc
      t.integer :vacancy_id

      t.timestamps
    end
  end
end
