class CreateAros < ActiveRecord::Migration
  def change
    create_table :aros do |t|
      t.integer :group_id
      t.integer :undpuser_id
      t.string :alias

      t.timestamps
    end
  end
end
