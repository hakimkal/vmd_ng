class CreateFunds < ActiveRecord::Migration
  def change
    create_table :funds do |t|
      t.string :account_number
      t.decimal :available_fund_usd
      t.string :financial_year_start
      t.string :financial_year_end
      t.string :operating_unit
      t.string :approving_officer
      t.string :date_created

      t.timestamps
    end
  end
end
