class RemoveColumnFromSupservtypes < ActiveRecord::Migration
  def up
    remove_column :supservtypes, :undpuser_id
  end

  def down
    add_column :supservtypes, :undpuser_id, :string
  end
end
