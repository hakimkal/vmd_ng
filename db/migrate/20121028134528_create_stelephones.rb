class CreateStelephones < ActiveRecord::Migration
  def change
    create_table :stelephones do |t|
      t.string :mobile
      t.string :home
      t.string :office
      t.string :fax
      t.string :email
      t.string :website
      t.integer :supplier_id

      t.timestamps
    end
  end
end
