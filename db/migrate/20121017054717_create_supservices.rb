class CreateSupservices < ActiveRecord::Migration
  def change
    create_table :supservices do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
