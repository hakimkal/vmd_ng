class AddAttachmentScreenshotToEMails < ActiveRecord::Migration
  def self.up
    change_table :e_mails do |t|
      t.has_attached_file :screenshot
    end
  end

  def self.down
    drop_attached_file :e_mails, :screenshot
  end
end
