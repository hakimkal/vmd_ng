class AddRegnTypeColumnToNgos < ActiveRecord::Migration
  def change
    add_column :ngos, :registration_type, :string
  end
end
