class CreateSFinancials < ActiveRecord::Migration
  def change
    create_table :s_financials do |t|
      t.string :year1_ti
      t.string :year1_ti_amount
      t.string :year2_ti
      t.string :year2_ti_amount
      t.string :year3
      t.string :year3_ti_amount
      t.string :year1_es
      t.string :year1_es_value
      t.string :year2_es
      t.string :year2_es_value
      t.string :year3_es
      t.string :year3_es_value
      t.integer :supplier_id

      t.timestamps
    end
  end
end
