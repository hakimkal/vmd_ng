class CreateCtelephones < ActiveRecord::Migration
  def change
    create_table :ctelephones do |t|
      t.string :home
      t.string :mobile
      t.string :work
      t.integer :consultant_id

      t.timestamps
    end
  end
end
