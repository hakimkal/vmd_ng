class CreateRecruitments < ActiveRecord::Migration
  def change
    create_table :recruitments do |t|
      t.integer :vacancy_id
      t.integer :status_id
      t.integer :undpuser_id
      t.text :job_title
      t.integer :vendor_type_id
      t.text :recruitment_by
      t.integer :recruiting_undpuser_id

      t.timestamps
    end
  end
end
