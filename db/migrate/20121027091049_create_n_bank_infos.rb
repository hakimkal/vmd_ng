class CreateNBankInfos < ActiveRecord::Migration
  def change
    create_table :n_bank_infos do |t|
      t.string :bank_name
      t.text :bank_address
      t.text :bank_swift_code
      t.string :bank_account_number
      t.string :bank_account_name
      t.integer :ngo_id

      t.timestamps
    end
  end
end
