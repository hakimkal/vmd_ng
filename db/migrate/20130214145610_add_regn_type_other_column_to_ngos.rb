class AddRegnTypeOtherColumnToNgos < ActiveRecord::Migration
  def change
    add_column :ngos, :registration_type_other, :string
  end
end
