class CreateVsses < ActiveRecord::Migration
  def change
    create_table :vsses do |t|
      t.string :name
      t.string :desc
      t.integer :vendor_type_id
      t.integer :vpa_id

      t.timestamps
    end
  end
end
