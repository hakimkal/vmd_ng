class CreateCUploads < ActiveRecord::Migration
  def change
    create_table :c_uploads do |t|
      t.integer :consultant_id

      t.timestamps
    end
  end
end
