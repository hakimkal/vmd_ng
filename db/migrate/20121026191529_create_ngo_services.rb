class CreateNgoServices < ActiveRecord::Migration
  def change
    create_table :ngo_services do |t|
      t.integer :ngo_id
      t.integer :vss_id

      t.timestamps
    end
  end
end
