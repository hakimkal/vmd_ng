class AddFaxColumnToCtelephones < ActiveRecord::Migration
  def change
    add_column :ctelephones, :fax, :string
  end
end
