class CreateEMails < ActiveRecord::Migration
  def change
    create_table :e_mails do |t|
      t.string :subject
      t.text :message
      t.string :sender
      t.integer :sender_id
      t.text :sent_to
      t.text :cc
      t.text :bcc
      t.text :sent_to_ids

      t.timestamps
    end
  end
end
