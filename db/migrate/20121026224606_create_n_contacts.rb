class CreateNContacts < ActiveRecord::Migration
  def change
    create_table :n_contacts do |t|
      t.string :firstname
      t.string :lastname
      t.string :title
      t.integer :ngo_id

      t.timestamps
    end
  end
end
