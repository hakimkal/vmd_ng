class AddColumnsToCEmploymentDetails < ActiveRecord::Migration
  def change
    add_column :c_employment_details, :un_staff, :string
    add_column :c_employment_details, :un_staff_index_no, :string
  end
end
