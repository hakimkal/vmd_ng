class AddColumnsToConsultants < ActiveRecord::Migration
  def change
    add_column :consultants, :other_name, :string
    add_column :consultants, :dob, :date
    add_column :consultants, :place_of_birth, :string
    add_column :consultants, :nationality_at_birth, :string
    add_column :consultants, :gender, :string
    add_column :consultants, :marital_status, :string
    add_column :consultants, :disability_from_air_travel, :string
  end
end
