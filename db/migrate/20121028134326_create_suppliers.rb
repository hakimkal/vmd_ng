class CreateSuppliers < ActiveRecord::Migration
  def change
    create_table :suppliers do |t|
      t.string :name
      t.string :street_address
      t.string :postal_code
      t.string :city
      t.string :country
      t.text :mailing_address
      t.text :parent_company
      t.string :subsidiaries
      t.string :nature_of_business
      t.string :type_of_business
      t.string :doe
      t.string :number_of_employees
      t.string :license_number
      t.string :state_where_regd
      t.string :tax_number
      t.string :dor
      t.string :working_language

      t.timestamps
    end
  end
end
