class AddFieldToFunds < ActiveRecord::Migration
  def change
    add_column :funds, :balance, :decimal
  end
end
