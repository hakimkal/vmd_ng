class CreateSTechnicalDocuments < ActiveRecord::Migration
  def change
    create_table :s_technical_documents do |t|
      t.string :written_statement_policy_status
      t.string :edi_status
      t.integer :supplier_id

      t.timestamps
    end
  end
end
