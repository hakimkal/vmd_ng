class CreateCLanguages < ActiveRecord::Migration
  def change
    create_table :c_languages do |t|
      t.string :language
      t.string :read_status
      t.string :write_status
      t.string :speak_status
      t.string :understand_status
      t.integer :consultant_id

      t.timestamps
    end
  end
end
