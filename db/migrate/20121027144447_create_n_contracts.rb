class CreateNContracts < ActiveRecord::Migration
  def change
    create_table :n_contracts do |t|
      t.string :orgainization
      t.string :value
      t.string :year
      t.text :nature_of_service
      t.text :contact_details
      t.text :disputes
      t.text :membership_of
      t.integer :ngo_id

      t.timestamps
    end
  end
end
