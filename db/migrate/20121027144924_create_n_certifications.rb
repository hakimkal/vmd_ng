class CreateNCertifications < ActiveRecord::Migration
  def change
    create_table :n_certifications do |t|
      t.string :name
      t.text :function
      t.string :title
      t.string :certify
      t.integer :ngo_id

      t.timestamps
    end
  end
end
