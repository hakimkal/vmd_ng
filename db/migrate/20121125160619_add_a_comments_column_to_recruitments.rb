class AddACommentsColumnToRecruitments < ActiveRecord::Migration
  def change
    add_column :recruitments, :remark, :text
  end
end
