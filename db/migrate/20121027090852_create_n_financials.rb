class CreateNFinancials < ActiveRecord::Migration
  def change
    create_table :n_financials do |t|
      t.string :year_1
      t.string :year_1_amount
      t.string :year_2
      t.string :year_2_amount
      t.string :year_3
      t.string :year_3_amount
      t.integer :ngo_id

      t.timestamps
    end
  end
end
