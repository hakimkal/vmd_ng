class CreateUndpusers < ActiveRecord::Migration
  def change
    create_table :undpusers do |t|
      t.string :username
      t.string :encrypted_password
      t.string :firstname
      t.string :lastname
      t.string :email
      t.integer :added_by
      t.string :usertype
      t.string :uni_id
      t.integer :vpa_id
      t.integer :vendor_id 
      t.integer :vss_id
      t.string :suspend
      t.integer :group_id

      t.timestamps
    end
  end
end
