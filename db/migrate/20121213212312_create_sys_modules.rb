class CreateSysModules < ActiveRecord::Migration
  def change
    create_table :sys_modules do |t|
      t.string :name
      t.string :alias

      t.timestamps
    end
  end
end
