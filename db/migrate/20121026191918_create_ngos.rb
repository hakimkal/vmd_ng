class CreateNgos < ActiveRecord::Migration
  def change
    create_table :ngos do |t|
      t.string :name
      t.string :street_address
      t.string :city
      t.string :state
      t.string :postal_code
      t.string :country
      t.text :mailing_address
      t.text :parent_ngo
      t.text :subsidiaries
      t.text :objectives
      t.string :doe
      t.string :num_of_employees
      t.string :license_number
      t.string :state_where_regd
      t.string :dor
      t.text :working_languages
      t.integer :undpuser_id

      t.timestamps
    end
  end
end
