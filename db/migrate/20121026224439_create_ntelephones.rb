class CreateNtelephones < ActiveRecord::Migration
  def change
    create_table :ntelephones do |t|
      t.string :mobile
      t.string :home
      t.string :office
      t.string :fax
      t.string :email
      t.string :website
      t.integer :ngo_id

      t.timestamps
    end
  end
end
