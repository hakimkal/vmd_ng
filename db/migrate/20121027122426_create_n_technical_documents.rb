class CreateNTechnicalDocuments < ActiveRecord::Migration
  def change
    create_table :n_technical_documents do |t|
      t.string :written_statement_policy_status
      t.integer :ngo_id

      t.timestamps
    end
  end
end
