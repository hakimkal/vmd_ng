class CreateCContactDetails < ActiveRecord::Migration
  def change
    create_table :c_contact_details do |t|
      t.text :street_address
      t.string :town
      t.string :state
      t.string :country
      t.string :telephone
      t.string :address_type
      t.integer :consultant_id

      t.timestamps
    end
  end
end
