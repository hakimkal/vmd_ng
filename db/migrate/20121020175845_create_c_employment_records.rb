class CreateCEmploymentRecords < ActiveRecord::Migration
  def change
    create_table :c_employment_records do |t|
      t.date :from
      t.date :to
      t.string :starting_gross_salary
      t.string :final_gross_salary
      t.string :functional_title
      t.string :un_grade
      t.string :last_un_step
      t.string :name_of_employer
      t.text :address_of_employer
      t.text :description_of_duties
      t.text :reason_for_leaving
      t.string :type_of_business
      t.string :employment_type
      t.string :type_of_contract
      t.string :name_of_supervisor
      t.string :email_of_supervisor
      t.string :telephone_of_supervisor
      t.string :did_you_supervise_staff
      t.string :number_of_staff_supervised
      t.string :number_of_support_staff_supervised

      t.timestamps
    end
  end
end
