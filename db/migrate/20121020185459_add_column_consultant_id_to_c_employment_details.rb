class AddColumnConsultantIdToCEmploymentDetails < ActiveRecord::Migration
  def change
    add_column :c_employment_details, :consultant_id, :integer
  end
end
