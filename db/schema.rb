# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130214145610) do

  create_table "sys_modules", :force => true do |t|
    t.string   "name"
    t.string   "alias"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "acos", :force => true do |t|
    t.string   "action"
    t.string   "alias"
    t.integer  "sys_module_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.index ["sys_module_id"], :name => "index_acos_on_sys_module_id"
    t.foreign_key ["sys_module_id"], "sys_modules", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "acos_sys_module_id_fkey"
  end

  create_table "groups", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "agency_departments", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "category"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "undpusers", :force => true do |t|
    t.string   "username"
    t.string   "encrypted_password"
    t.string   "firstname"
    t.string   "lastname"
    t.string   "email"
    t.integer  "added_by"
    t.string   "usertype"
    t.string   "uni_id"
    t.integer  "vpa_id"
    t.integer  "vendor_id"
    t.integer  "vss_id"
    t.string   "suspend"
    t.integer  "group_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.string   "salt"
    t.integer  "agency_department_id"
    t.index ["agency_department_id"], :name => "index_undpusers_on_agency_department_id"
    t.foreign_key ["agency_department_id"], "agency_departments", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "undpusers_agency_department_id_fkey"
  end

  create_table "aros", :force => true do |t|
    t.integer  "group_id"
    t.integer  "undpuser_id"
    t.string   "alias"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.index ["group_id"], :name => "index_aros_on_group_id"
    t.index ["undpuser_id"], :name => "index_aros_on_undpuser_id"
    t.foreign_key ["group_id"], "groups", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "aros_group_id_fkey"
    t.foreign_key ["undpuser_id"], "undpusers", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "aros_undpuser_id_fkey"
  end

  create_table "aco_aros", :force => true do |t|
    t.integer  "aco_id"
    t.integer  "aro_id"
    t.string   "allow"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.index ["aco_id"], :name => "index_aco_aros_on_aco_id"
    t.index ["aro_id"], :name => "index_aco_aros_on_aro_id"
    t.foreign_key ["aco_id"], "acos", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "aco_aros_aco_id_fkey"
    t.foreign_key ["aro_id"], "aros", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "aco_aros_aro_id_fkey"
  end

  create_table "activities", :force => true do |t|
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "key"
    t.text     "parameters"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "app_attachments", :force => true do |t|
    t.integer  "application_id"
    t.integer  "undpuser_id"
    t.text     "file_1_desc"
    t.text     "file_2_desc"
    t.text     "file_3_desc"
    t.text     "file_4_desc"
    t.text     "file_5_desc"
    t.integer  "vacancy_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "file_1_file_name"
    t.string   "file_1_content_type"
    t.integer  "file_1_file_size"
    t.datetime "file_1_updated_at"
    t.string   "file_2_file_name"
    t.string   "file_2_content_type"
    t.integer  "file_2_file_size"
    t.datetime "file_2_updated_at"
    t.string   "file_3_file_name"
    t.string   "file_3_content_type"
    t.integer  "file_3_file_size"
    t.datetime "file_3_updated_at"
    t.string   "file_4_file_name"
    t.string   "file_4_content_type"
    t.integer  "file_4_file_size"
    t.datetime "file_4_updated_at"
    t.string   "file_5_file_name"
    t.string   "file_5_content_type"
    t.integer  "file_5_file_size"
    t.datetime "file_5_updated_at"
  end

  create_table "c_certifications", :force => true do |t|
    t.integer  "certify"
    t.integer  "consultant_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "c_contact_details", :force => true do |t|
    t.text     "street_address"
    t.string   "town"
    t.string   "state"
    t.string   "country"
    t.string   "telephone"
    t.string   "address_type"
    t.integer  "consultant_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "c_de_pendents", :force => true do |t|
    t.string   "name"
    t.date     "dob"
    t.string   "relationship"
    t.integer  "consultant_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "c_educations", :force => true do |t|
    t.string   "institution"
    t.string   "place"
    t.string   "country"
    t.string   "qualification"
    t.string   "course_of_study"
    t.string   "study_type"
    t.integer  "consultant_id"
    t.string   "attended_from"
    t.string   "attended_to"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "c_employment_details", :force => true do |t|
    t.string   "object_to_enquiries_of_present_employer"
    t.string   "object_to_enquiries_of_past_employer"
    t.string   "civil_servant"
    t.date     "civil_service_date_from"
    t.date     "civil_service_date_to"
    t.text     "civil_service_functions_text"
    t.string   "civil_service_country"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.string   "unv"
    t.string   "roster_no"
    t.integer  "consultant_id"
    t.string   "un_staff"
    t.string   "un_staff_index_no"
    t.decimal  "experience"
  end

  create_table "c_employment_records", :force => true do |t|
    t.date     "from"
    t.string   "to"
    t.string   "starting_gross_salary"
    t.string   "final_gross_salary"
    t.string   "functional_title"
    t.string   "un_grade"
    t.string   "last_un_step"
    t.string   "name_of_employer"
    t.text     "address_of_employer"
    t.text     "description_of_duties"
    t.text     "reason_for_leaving"
    t.string   "type_of_business"
    t.string   "employment_type"
    t.string   "type_of_contract"
    t.string   "name_of_supervisor"
    t.string   "email_of_supervisor"
    t.string   "telephone_of_supervisor"
    t.string   "did_you_supervise_staff"
    t.string   "number_of_staff_supervised"
    t.string   "number_of_support_staff_supervised"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.integer  "consultant_id"
    t.integer  "c_employment_detail_id"
  end

  create_table "c_ext_family_in_undps", :force => true do |t|
    t.string   "name"
    t.string   "relationship"
    t.string   "name_of_unit"
    t.string   "duty_station"
    t.integer  "consultant_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "c_family_in_undps", :force => true do |t|
    t.string   "name"
    t.string   "relationship"
    t.string   "name_of_unit"
    t.string   "duty_station"
    t.integer  "consultant_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "c_languages", :force => true do |t|
    t.string   "language"
    t.string   "read_status"
    t.string   "write_status"
    t.string   "speak_status"
    t.string   "understand_status"
    t.integer  "consultant_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "c_miscs", :force => true do |t|
    t.string   "any_dependent"
    t.string   "legal_permanent_residence_status"
    t.string   "legal_permanent_residence_country"
    t.string   "present_nationality_change_status"
    t.text     "present_nationality_change_explain"
    t.string   "family_in_undp_status"
    t.string   "ext_family_in_undp_status"
    t.string   "accept_employment_for_less_than_six"
    t.string   "interviewed_for_any_undp_job_in_last_twelve"
    t.text     "interviewed_posts"
    t.string   "asat_test_status"
    t.date     "asat_date_taken"
    t.string   "finance_assessment_test_status"
    t.date     "finance_assessment_date_taken"
    t.string   "ever_been_convicted_fined_imprisoned"
    t.text     "conviction_particulars"
    t.string   "disciplinary_measures"
    t.text     "disciplinary_particulars"
    t.string   "separated_from_service"
    t.text     "separation_particulars"
    t.integer  "consultant_id"
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
  end

  create_table "c_post_qualifications", :force => true do |t|
    t.string   "institution"
    t.string   "place"
    t.string   "country"
    t.string   "qualification"
    t.string   "course_of_study"
    t.string   "study_type"
    t.integer  "consultant_id"
    t.string   "attended_from"
    t.string   "attended_to"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "c_pro_societies", :force => true do |t|
    t.string   "name"
    t.text     "details"
    t.integer  "consultant_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "c_publications", :force => true do |t|
    t.string   "name"
    t.text     "details"
    t.integer  "consultant_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "c_ratings", :force => true do |t|
    t.integer  "consultant_id"
    t.string   "project_number"
    t.string   "consultant_name"
    t.string   "project_title"
    t.string   "project_duty_station"
    t.text     "project_description"
    t.string   "start_date"
    t.string   "end_date"
    t.string   "countries_visited"
    t.string   "question_1"
    t.string   "question_2"
    t.string   "question_3"
    t.string   "question_4"
    t.string   "question_5"
    t.string   "question_6"
    t.string   "question_7"
    t.text     "extra_detail"
    t.string   "should_this_vendor_remain_on_roster"
    t.string   "evaluated_by"
    t.string   "organization"
    t.string   "designation"
    t.string   "thedate"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
  end

  create_table "c_references", :force => true do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.text     "street"
    t.string   "town"
    t.string   "state"
    t.string   "country"
    t.string   "email"
    t.string   "mobile"
    t.string   "home"
    t.string   "fax"
    t.integer  "consultant_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "work"
  end

  create_table "c_uploads", :force => true do |t|
    t.integer  "consultant_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.string   "resume_file_name"
    t.string   "resume_content_type"
    t.integer  "resume_file_size"
    t.datetime "resume_updated_at"
  end

  create_table "consultants", :force => true do |t|
    t.integer  "undpuser_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.string   "other_name"
    t.date     "dob"
    t.string   "place_of_birth"
    t.string   "nationality_at_birth"
    t.string   "gender"
    t.string   "marital_status"
    t.string   "disability_from_air_travel"
    t.string   "firstname"
    t.string   "lastname"
    t.string   "disability_describe"
    t.string   "nationalities"
  end

  create_view "cons_ratings", "SELECT DISTINCT ON (c_ratings.id, c_ratings.project_number, c_ratings.consultant_id) c_ratings.project_number AS prj_number, c_ratings.consultant_name, c_ratings.consultant_id AS cons_id, c_ratings.id, c_ratings.question_1, c_ratings.question_2, c_ratings.question_3, c_ratings.question_4, c_ratings.question_5, c_ratings.question_6, c_ratings.question_7, c_ratings.consultant_id, c_ratings.should_this_vendor_remain_on_roster, c_ratings.evaluated_by, c_ratings.extra_detail, c_ratings.thedate, c_ratings.designation, c_ratings.start_date, c_ratings.end_date, c_ratings.project_number, c_ratings.project_title, c_ratings.project_duty_station, c_ratings.project_description, c_ratings.countries_visited, c_ratings.organization, consultants.gender, consultants.dob, undpusers.firstname, undpusers.lastname, undpusers.email, c_ratings.created_at, c_ratings.updated_at FROM ((c_ratings LEFT JOIN consultants ON ((c_ratings.consultant_id = consultants.id))) LEFT JOIN undpusers ON ((consultants.undpuser_id = undpusers.id))) ORDER BY c_ratings.id, c_ratings.project_number, c_ratings.consultant_id", :force => true
  create_table "conservices", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "conservtypes", :force => true do |t|
    t.integer  "consultant_id"
    t.integer  "conservice_id"
    t.integer  "undpuser_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_view "consultant_fullnames", "SELECT (((consultants.firstname)::text || ' '::text) || (consultants.lastname)::text) AS name, consultants.id FROM consultants WHERE (consultants.firstname IS NOT NULL) ORDER BY consultants.firstname", :force => true
  create_table "ctelephones", :force => true do |t|
    t.string   "home"
    t.string   "mobile"
    t.string   "work"
    t.integer  "consultant_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "fax"
  end

  create_table "vpas", :force => true do |t|
    t.string   "name"
    t.string   "desc"
    t.integer  "vendor_type_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "vsses", :force => true do |t|
    t.string   "name"
    t.string   "desc"
    t.integer  "vendor_type_id"
    t.integer  "vpa_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_view "consultant_reports", "SELECT consultants.firstname AS c_fname, consultants.lastname AS c_lname, consultants.id AS consultant_id, consultants.id, consultants.dob, consultants.gender, consultants.dob AS birthday, (('now'::text)::date - consultants.dob) AS c_age, consultants.marital_status, undpusers.vendor_id, undpusers.email, undpusers.firstname, undpusers.lastname, undpusers.id AS undpuser_id, undpusers.vpa_id, vpas.name AS vpa_name, undpusers.vss_id, vsses.name AS vss_name, conservtypes.conservice_id AS service_id, conservices.name AS service, ctelephones.home, ctelephones.mobile, c_contact_details.id AS contact_id, c_contact_details.country, c_uploads.resume_file_name, c_employment_details.roster_no, c_employment_details.un_staff_index_no, c_employment_details.experience, c_educations.qualification, c_educations.id AS c_qual_id, c_post_qualifications.qualification AS c_post_qual, c_post_qualifications.id AS c_post_qual_id FROM (((((((((((consultants LEFT JOIN undpusers ON ((consultants.undpuser_id = undpusers.id))) LEFT JOIN vpas ON ((undpusers.vpa_id = vpas.id))) LEFT JOIN conservtypes ON ((conservtypes.consultant_id = consultants.id))) LEFT JOIN conservices ON ((conservtypes.conservice_id = conservices.id))) LEFT JOIN vsses ON ((undpusers.vss_id = vsses.id))) LEFT JOIN ctelephones ON ((consultants.id = ctelephones.consultant_id))) LEFT JOIN c_contact_details ON ((consultants.id = c_contact_details.consultant_id))) LEFT JOIN c_uploads ON ((consultants.id = c_uploads.consultant_id))) LEFT JOIN c_employment_details ON ((consultants.id = c_employment_details.consultant_id))) RIGHT JOIN c_educations ON ((consultants.id = c_educations.consultant_id))) FULL JOIN c_post_qualifications ON ((consultants.id = c_post_qualifications.consultant_id))) WHERE (consultants.firstname IS NOT NULL) ORDER BY consultants.firstname", :force => true
  create_table "cun_languages", :force => true do |t|
    t.string   "institution"
    t.string   "place"
    t.string   "country"
    t.string   "qualification"
    t.string   "course_of_study"
    t.string   "study_type"
    t.integer  "consultant_id"
    t.string   "attended_from"
    t.string   "attended_to"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "cundp_certifications", :force => true do |t|
    t.string   "institution"
    t.string   "place"
    t.string   "country"
    t.string   "qualification"
    t.string   "course_of_study"
    t.string   "study_type"
    t.integer  "consultant_id"
    t.string   "attended_from"
    t.string   "attended_to"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "e_mails", :force => true do |t|
    t.string   "subject"
    t.text     "message"
    t.string   "sender"
    t.integer  "sender_id"
    t.text     "sent_to"
    t.text     "cc"
    t.text     "bcc"
    t.text     "sent_to_ids"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.string   "category"
    t.string   "severity"
    t.string   "screenshot_file_name"
    t.string   "screenshot_content_type"
    t.integer  "screenshot_file_size"
    t.datetime "screenshot_updated_at"
    t.string   "ftype"
  end

  create_table "funds", :force => true do |t|
    t.string   "account_number"
    t.decimal  "available_fund_usd"
    t.string   "financial_year_start"
    t.string   "financial_year_end"
    t.string   "operating_unit"
    t.string   "approving_officer"
    t.string   "date_created"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.decimal  "balance"
    t.integer  "agency_department_id"
    t.index ["agency_department_id"], :name => "index_funds_on_agency_department_id"
    t.foreign_key ["agency_department_id"], "agency_departments", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "funds_agency_department_id_fkey"
  end

  create_table "logins", :force => true do |t|
    t.integer  "user_id"
    t.datetime "time_in"
    t.datetime "time_out"
    t.integer  "session_duration"
    t.string   "username"
    t.string   "fullname"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "mails", :force => true do |t|
    t.string   "subject"
    t.text     "message"
    t.string   "sender"
    t.integer  "sender_id"
    t.text     "sent_to"
    t.text     "cc"
    t.text     "bcc"
    t.text     "sent_to_ids"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "n_bank_infos", :force => true do |t|
    t.string   "bank_name"
    t.text     "bank_address"
    t.text     "bank_swift_code"
    t.string   "bank_account_number"
    t.string   "bank_account_name"
    t.integer  "ngo_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "n_certifications", :force => true do |t|
    t.string   "name"
    t.text     "function"
    t.string   "title"
    t.string   "certify"
    t.integer  "ngo_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "n_contacts", :force => true do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.string   "title"
    t.integer  "ngo_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "n_contracts", :force => true do |t|
    t.string   "organization"
    t.string   "value"
    t.string   "year"
    t.text     "nature_of_service"
    t.text     "contact_details"
    t.text     "disputes"
    t.text     "membership_of"
    t.integer  "ngo_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "n_financials", :force => true do |t|
    t.string   "year_1"
    t.string   "year_1_amount"
    t.string   "year_2"
    t.string   "year_2_amount"
    t.string   "year_3"
    t.string   "year_3_amount"
    t.integer  "ngo_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "n_ratings", :force => true do |t|
    t.string   "ngo_name"
    t.string   "organizational_unit"
    t.string   "contract_sum"
    t.string   "project_title"
    t.string   "contract_number"
    t.string   "start_date"
    t.string   "end_date"
    t.text     "additional_information"
    t.text     "question_1"
    t.text     "question_2"
    t.text     "question_3"
    t.text     "question_4"
    t.text     "question_5"
    t.text     "question_6"
    t.string   "question_7"
    t.string   "question_8"
    t.string   "question_9"
    t.string   "question_10"
    t.string   "question_11"
    t.text     "question_12"
    t.string   "evaluated_by"
    t.string   "organization"
    t.string   "designation"
    t.string   "thedate"
    t.integer  "ngo_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "n_technical_documents", :force => true do |t|
    t.string   "written_statement_policy_status"
    t.integer  "ngo_id"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.string   "financial_file_name"
    t.string   "financial_content_type"
    t.integer  "financial_file_size"
    t.datetime "financial_updated_at"
    t.string   "job_evidence_file_name"
    t.string   "job_evidence_content_type"
    t.integer  "job_evidence_file_size"
    t.datetime "job_evidence_updated_at"
    t.string   "written_file_name"
    t.string   "written_content_type"
    t.integer  "written_file_size"
    t.datetime "written_updated_at"
  end

  create_table "ngos", :force => true do |t|
    t.string   "name"
    t.string   "street_address"
    t.string   "city"
    t.string   "state"
    t.string   "postal_code"
    t.string   "country"
    t.text     "mailing_address"
    t.text     "parent_ngo"
    t.text     "subsidiaries"
    t.text     "objectives"
    t.string   "doe"
    t.string   "num_of_employees"
    t.string   "license_number"
    t.string   "state_where_regd"
    t.string   "dor"
    t.text     "working_languages"
    t.integer  "undpuser_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.string   "registration_type"
    t.string   "registration_type_other"
  end

  create_view "ng_ratings", "SELECT DISTINCT ON (n_ratings.contract_number, n_ratings.ngo_id) n_ratings.id, n_ratings.question_1, n_ratings.question_2, n_ratings.question_3, n_ratings.question_4, n_ratings.question_5, n_ratings.question_6, n_ratings.question_7, n_ratings.question_8, n_ratings.question_9, n_ratings.question_10, n_ratings.question_11, n_ratings.question_12, n_ratings.ngo_id, n_ratings.organizational_unit, ngos.name, n_ratings.evaluated_by, n_ratings.thedate, n_ratings.designation, n_ratings.contract_number, n_ratings.start_date, n_ratings.end_date, n_ratings.created_at, n_ratings.updated_at, n_ratings.organization, n_ratings.contract_sum, n_ratings.ngo_name, n_ratings.project_title, n_ratings.additional_information FROM (n_ratings LEFT JOIN ngos ON ((n_ratings.ngo_id = ngos.id)))", :force => true
  create_table "ngo_services", :force => true do |t|
    t.integer  "ngo_id"
    t.integer  "vss_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_view "ngo_reports", "SELECT ngos.name, ngos.id, ngos.id AS ngo_id, ngos.city, ngos.country, undpusers.vendor_id, undpusers.id AS undpuser_id, undpusers.username, undpusers.firstname, undpusers.lastname, undpusers.email, undpusers.vpa_id, vpas.name AS vpa_name, undpusers.vss_id, vsses.name AS vss_name, n_financials.id AS ngo_financial_id, n_contracts.id AS ngo_contract_id, n_technical_documents.id AS ngo_document_id, n_technical_documents.financial_file_name, n_technical_documents.job_evidence_file_name AS ngo_evidence, n_technical_documents.written_file_name FROM (((((((ngos LEFT JOIN undpusers ON ((ngos.undpuser_id = undpusers.id))) LEFT JOIN vpas ON ((undpusers.vpa_id = vpas.id))) LEFT JOIN vsses ON ((undpusers.vss_id = vsses.id))) LEFT JOIN ngo_services ON ((ngos.id = ngo_services.ngo_id))) LEFT JOIN n_financials ON ((ngos.id = n_financials.ngo_id))) LEFT JOIN n_contracts ON ((ngos.id = n_contracts.ngo_id))) LEFT JOIN n_technical_documents ON ((ngos.id = n_technical_documents.ngo_id))) WHERE (ngos.name IS NOT NULL) ORDER BY ngos.name", :force => true
  create_table "ntelephones", :force => true do |t|
    t.string   "mobile"
    t.string   "home"
    t.string   "office"
    t.string   "fax"
    t.string   "email"
    t.string   "website"
    t.integer  "ngo_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "projects", :force => true do |t|
    t.string   "name"
    t.integer  "agency_department_id"
    t.string   "thedate"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.integer  "fund_id"
    t.index ["agency_department_id"], :name => "index_projects_on_agency_department_id"
    t.index ["fund_id"], :name => "index_projects_on_fund_id"
    t.foreign_key ["agency_department_id"], "agency_departments", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "projects_agency_department_id_fkey"
    t.foreign_key ["fund_id"], "funds", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "projects_fund_id_fkey"
  end

  create_table "recruitments", :force => true do |t|
    t.integer  "vacancy_id"
    t.integer  "status_id"
    t.integer  "undpuser_id"
    t.text     "job_title"
    t.integer  "vendor_type_id"
    t.text     "recruitment_by"
    t.integer  "recruiting_undpuser_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.integer  "application_id"
    t.text     "remark"
  end

  create_table "s_bank_infos", :force => true do |t|
    t.string   "bank_name"
    t.text     "bank_address"
    t.string   "bank_swift_code"
    t.string   "bank_account_number"
    t.string   "bank_account_name"
    t.integer  "supplier_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "s_certifications", :force => true do |t|
    t.string   "name"
    t.string   "function"
    t.string   "title"
    t.string   "certify"
    t.integer  "supplier_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "s_contacts", :force => true do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.string   "title"
    t.integer  "supplier_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "s_contracts", :force => true do |t|
    t.string   "organization"
    t.string   "contract_amount"
    t.string   "year"
    t.text     "goods_or_services"
    t.string   "destination"
    t.text     "contact_details"
    t.text     "export_countries"
    t.string   "disputes"
    t.string   "membership_of"
    t.integer  "supplier_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "s_financials", :force => true do |t|
    t.string   "year1_ti"
    t.string   "year1_ti_amount"
    t.string   "year2_ti"
    t.string   "year2_ti_amount"
    t.string   "year3_ti"
    t.string   "year3_ti_amount"
    t.string   "year1_es"
    t.string   "year1_es_value"
    t.string   "year2_es"
    t.string   "year2_es_value"
    t.string   "year3_es"
    t.string   "year3_es_value"
    t.integer  "supplier_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "s_ratings", :force => true do |t|
    t.integer  "supplier_id"
    t.string   "supplier_name"
    t.string   "project_number"
    t.text     "goods_services"
    t.string   "question_1"
    t.string   "question_2"
    t.string   "question_3"
    t.string   "question_4"
    t.string   "question_5"
    t.text     "question_6"
    t.string   "evaluated_by"
    t.string   "organization"
    t.string   "designation"
    t.string   "thedate"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.string   "project_title"
    t.string   "start_date"
    t.string   "end_date"
  end

  create_table "s_technical_documents", :force => true do |t|
    t.string   "written_statement_policy_status"
    t.string   "edi_status"
    t.integer  "supplier_id"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
    t.string   "fin_report_file_name"
    t.string   "fin_report_content_type"
    t.integer  "fin_report_file_size"
    t.datetime "fin_report_updated_at"
    t.string   "quality_assurance_file_name"
    t.string   "quality_assurance_content_type"
    t.integer  "quality_assurance_file_size"
    t.datetime "quality_assurance_updated_at"
    t.string   "intl_representation_file_name"
    t.string   "intl_representation_content_type"
    t.integer  "intl_representation_file_size"
    t.datetime "intl_representation_updated_at"
    t.string   "goods_services_file_name"
    t.string   "goods_services_content_type"
    t.integer  "goods_services_file_size"
    t.datetime "goods_services_updated_at"
    t.string   "written_file_name"
    t.string   "written_content_type"
    t.integer  "written_file_size"
    t.datetime "written_updated_at"
  end

  create_table "suppliers", :force => true do |t|
    t.string   "name"
    t.string   "street_address"
    t.string   "postal_code"
    t.string   "city"
    t.string   "country"
    t.text     "mailing_address"
    t.text     "parent_company"
    t.string   "subsidiaries"
    t.string   "nature_of_business"
    t.string   "type_of_business"
    t.string   "doe"
    t.string   "number_of_employees"
    t.string   "license_number"
    t.string   "state_where_regd"
    t.string   "tax_number"
    t.string   "dor"
    t.string   "working_language"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.integer  "undpuser_id"
    t.string   "state"
  end

  create_view "sp_ratings", "SELECT DISTINCT ON (s_ratings.project_number, s_ratings.supplier_id) s_ratings.id, s_ratings.question_1, s_ratings.question_2, s_ratings.question_3, s_ratings.question_4, s_ratings.question_5, s_ratings.question_6, s_ratings.supplier_id, s_ratings.supplier_name, suppliers.name, s_ratings.project_number, s_ratings.evaluated_by, s_ratings.thedate, s_ratings.designation, s_ratings.start_date, s_ratings.end_date, s_ratings.goods_services, s_ratings.organization, s_ratings.project_title, s_ratings.created_at, s_ratings.updated_at FROM (s_ratings LEFT JOIN suppliers ON ((s_ratings.supplier_id = suppliers.id))) ORDER BY s_ratings.project_number, s_ratings.supplier_id", :force => true
  create_table "statuses", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "stelephones", :force => true do |t|
    t.string   "mobile"
    t.string   "home"
    t.string   "office"
    t.string   "fax"
    t.string   "email"
    t.string   "website"
    t.integer  "supplier_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "supservices", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "supservtypes", :force => true do |t|
    t.integer  "supplier_id"
    t.integer  "supservice_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_view "supplier_reports", "SELECT suppliers.name, suppliers.id AS supplier_id, suppliers.id, suppliers.city, suppliers.country, undpusers.vendor_id, undpusers.email, undpusers.firstname, undpusers.lastname, undpusers.id AS undpuser_id, undpusers.vpa_id, vpas.name AS vpa_name, undpusers.vss_id, vsses.name AS vss_name, s_financials.id AS sfinancial_id, s_contracts.id AS s_contract_id, s_technical_documents.id AS s_document_id, s_technical_documents.fin_report_file_name, s_technical_documents.quality_assurance_file_name, s_technical_documents.intl_representation_file_name, s_technical_documents.goods_services_file_name, s_technical_documents.written_file_name, supservtypes.supservice_id AS supservtype_id, supservices.name AS service FROM ((((((((suppliers LEFT JOIN undpusers ON ((suppliers.undpuser_id = undpusers.id))) LEFT JOIN vpas ON ((undpusers.vpa_id = vpas.id))) LEFT JOIN vsses ON ((undpusers.vss_id = vsses.id))) RIGHT JOIN supservtypes ON ((suppliers.id = supservtypes.supplier_id))) RIGHT JOIN supservices ON ((supservices.id = supservtypes.supservice_id))) LEFT JOIN s_financials ON ((suppliers.id = s_financials.supplier_id))) LEFT JOIN s_contracts ON ((suppliers.id = s_contracts.supplier_id))) LEFT JOIN s_technical_documents ON ((suppliers.id = s_technical_documents.supplier_id))) WHERE (suppliers.name IS NOT NULL) ORDER BY suppliers.name", :force => true
  create_table "un_contracts", :force => true do |t|
    t.string   "project_name"
    t.string   "project_ref"
    t.string   "department_ref"
    t.string   "thedate"
    t.string   "contract_type"
    t.string   "contract_title"
    t.string   "contract_un_reference"
    t.decimal  "contract_value_in_usd"
    t.decimal  "contract_value_in_co_currency"
    t.text     "description_of_goods_or_services"
    t.string   "start_date"
    t.string   "end_date"
    t.decimal  "duration"
    t.string   "organization_awarded"
    t.string   "location"
    t.string   "appointing_agency"
    t.string   "appointing_agency_contact_person"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
    t.integer  "fund_id"
    t.integer  "project_id"
    t.integer  "agency_department_id"
    t.index ["agency_department_id"], :name => "index_un_contracts_on_agency_department_id"
    t.index ["fund_id"], :name => "index_un_contracts_on_fund_id"
    t.index ["project_id"], :name => "index_un_contracts_on_project_id"
    t.foreign_key ["fund_id"], "funds", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "un_contracts_fund_id_fkey"
    t.foreign_key ["project_id"], "projects", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "un_contracts_project_id_fkey"
    t.foreign_key ["agency_department_id"], "agency_departments", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "un_contracts_agency_department_id_fkey"
  end

  create_table "vac_applications", :force => true do |t|
    t.integer  "undpuser_id"
    t.integer  "vacancy_id"
    t.integer  "vendor_id"
    t.integer  "vpa_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "vacancies", :force => true do |t|
    t.string   "name"
    t.integer  "vendor_id"
    t.string   "vpa_id"
    t.string   "vss_id"
    t.string   "location"
    t.string   "deadline"
    t.string   "contract_type"
    t.string   "language_required"
    t.string   "starting_date"
    t.string   "end_date"
    t.string   "duration_of_contract"
    t.text     "job_background"
    t.text     "job_duties"
    t.text     "job_deliverables"
    t.text     "technical_competence"
    t.text     "corporate_competence"
    t.text     "functional_competence"
    t.text     "management"
    t.text     "required_skills_experience"
    t.string   "file_1_description"
    t.string   "file_2_description"
    t.string   "file_3_description"
    t.string   "file_4_description"
    t.string   "file_5_description"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.string   "file_1_file_name"
    t.string   "file_1_content_type"
    t.integer  "file_1_file_size"
    t.datetime "file_1_updated_at"
    t.string   "file_2_file_name"
    t.string   "file_2_content_type"
    t.integer  "file_2_file_size"
    t.datetime "file_2_updated_at"
    t.string   "file_3_file_name"
    t.string   "file_3_content_type"
    t.integer  "file_3_file_size"
    t.datetime "file_3_updated_at"
    t.string   "file_4_file_name"
    t.string   "file_4_content_type"
    t.integer  "file_4_file_size"
    t.datetime "file_4_updated_at"
    t.string   "file_5_file_name"
    t.string   "file_5_content_type"
    t.integer  "file_5_file_size"
    t.datetime "file_5_updated_at"
    t.integer  "status",                     :default => 0
    t.integer  "requires_attachment"
    t.integer  "minimum_attachment"
  end

  create_table "vendor_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

end
