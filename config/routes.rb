Vmd::Application.routes.draw do
     
  resources :groups do
    
  end

  resources :aco_aros do
    
     member do
      post 'new'
    end
  end

  resources :aros do
   collection do 
      get 'setup_aros'
    end
    
  end
   
  resources :acos do
    collection do 
      get 'setup_acos'
    end
  end

   

  resources :sys_modules do
    
  end

   
   

  resources :agency_departments do
    
  end

  resources :projects do
    
  end

   
  resources :un_contracts do
    member do
      post 'new'
    end
    collection do
      get 'select_project_account'
      get 'reports_for_lta'
      
    end
  end

  

  resources :funds

  

 resources :reports  

   

 resources :supplier_reports do
   collection do
      post 'index'
    end
 end

  resources :consultant_reports do
    collection do
      post 'index'
    end
  end



 resources :ngo_reports do
   collection do
      post 'index'
    end
 end

  

  resources :sp_ratings   

   
  resources :ng_ratings  
 

   resources :cons_ratings do
    
   end

  resources :n_ratings do
  member do 
     post 'new'
   end
   collection do
     get 'select_ngos'
   end  
  end

   

   resources :s_ratings do
   member do 
     post 'new'
   end
   collection do
     get 'select_sups'
   end 
  end
   

  resources :c_ratings do
   member do 
     post 'new'
   end
   
   collection do
     get 'select_cons'
     get 'search_vendor'
     put 'search_vendor'
   end 
  end

   

  resources :recruitments do
    collection do
       get 'export_to_excel'
   end
  end

   

  resources :app_attachments do
    
  end

 
  resources :vac_applications do
   collection do
       get 'export_to_excel'
   end
  end

   

  resources :vacancies do
    collection do
      get 'view'
      get 'adv_search'
      post 'adv_search'
    end
    
  end

   

  resources :e_mails do
    member do
      #get 'new'
      post 'new'
     get 'view'
     end
     collection do
     get 'send_auto_pass'
    end
    
    collection do
      get 'feedback'
    end
    
    
  end

   
  resources :audit_trails , :only => [:show,:index,:destroy]

  resources :system_wide do
        
    collection do 
      get 'metrics'
      post 'collate_for_export'
    end
    
  end

   

 resources :s_certifications do
 end

  resources :s_contracts do
  
  end
  resources :s_technical_documents do
    
  end

   
  resources :s_bank_infos do
    
  end

   
 resources :s_financials do
   
 end

  

  resources :stelephone do
    
  end

  resources :s_contacts do
    
  end

  resources :suppliers do
    collection do 
     get 'export_to_excel'
     get 'notifications'
   end
     
  end

  resources :n_certifications do
    
  end

  resources :n_contracts do
    
  end

 resources :n_technical_documents do 
    
  end
  

   

  resources :n_bank_infos do
    
  end

 
 resources :n_financials do
   
 end

  

resources :n_contacts do
  
end
resources :ntelephones do 
  
end

 

  resources :ngos do
    collection do 
     get 'export_to_excel'
     get 'notifications'
   end
     
  end
 resources :ngo_services do
   
 end

 resources :c_uploads do
    
  end

   

  resources :c_ext_family_in_undps do
     get 'add' , :on => :collection
     get 'remove' , :on => :collection
  end

  resources :c_miscs do
     get 'add' , :on => :collection
     get 'remove' , :on => :collection
  end

   
  resources :c_languages do
   get 'add' , :on => :collection
   get 'remove' , :on => :collection  
  end

   
  resources :c_de_pendents do
     get 'add' , :on => :collection
     get 'remove' , :on => :collection
  end
  
  resources :c_family_in_undps do
     get 'add' , :on => :collection
     get 'remove' , :on => :collection
  end

   

  resources :c_publications do
    get 'add' , :on => :collection
    get 'remove' , :on => :collection
  end
  resources :c_pro_societies do
   get 'add' , :on => :collection
   get 'remove' , :on => :collection 
  end
   
 resources :cundp_certifications do
   get 'add' , :on => :collection
   get 'remove' , :on => :collection
 end
 
 resources :cun_languages do
  get 'add' , :on => :collection
  get 'remove' , :on => :collection 
 end
   

 resources :c_post_qualifications do
    get 'add' , :on => :collection
    get 'remove' , :on => :collection
 end

 resources :c_educations do
      get 'add' , :on => :collection
      get 'remove' , :on => :collection
     end

 resources :c_employment_records do
    get 'add' , :on => :collection
    get 'remove' , :on => :collection
 end

 resources :c_employment_details do 
     
 end
  
 resources :c_references do
    
 end

  

 resources :ctelephones do
   
 end

 resources :c_contact_details do
   
 end
 resources :consultants do
   collection do 
     get 'export_to_excel'
   end
    
 end

 resources :conservices do
    
 end

 resources :conservtypes do
     
 end

 resources :supservices do
    
 end

  resources :supservtypes do 
    
  end

  resources :statuses do
    
  end

  resources :vpas do
   member do
     get 'listed'
   end
   
   end
   
   resources :vsses do
    member do
       get 'listed' 
   end 
   end
  resources :c_certifications do
    
  end
  resources :vendor_types do
    collection do
    get 't_view_mgr'  
    post 'cons_sup_service' 
    get 'vendor_manager'  
    end
     
  end
  
  resource :logins do 
     collection  do 
       get 'view'
       get 'export_to_excel'
       get 'view'
       get 'index'
       get 'show' 
        
      end
   end
  delete 'logins/reset'
 
  match 'logins/:user_id/view' => 'logins#view' 
  match 'audited/:id' => 'logins#show' 
  match 'audited/:id/remove' => 'logins#destroy'  
  get "pages/home"
  get "pages/faq"
  root :to => "pages#home"
  
  resource :dashboards , :only => :index , :via => :get
  resource :usershomes , :only => :usershome , :via => :get
  resource :supplieruser , :only => [:index ,:supplieruser] , :via => :get
  resource :ngouser , :only => [:index ,:ngouser] , :via => :get
 
  resources :undpusers do
    collection do
      get 'confirmation'
      get 'request_confirmation_link'
      post 'request_confirmation_link'
      put 'signup_change_password'
      get 'signup_change_password'
      get 'forgot_password'
      post 'forgot_password'
      get 'report'
      get 'create_vendor'
      get 'create_staff'
      get 'export_to_excel'
      
    end
    member do
      get 'change_password'
      put 'change_password'
      get 'edit_profile'
      put 'edit_profile'
      get 'vendor_select'
      put 'vendor_select'
   end
    
  end
  
  resources :sessions do
    
  end
  match "/dashboard" => "dashboards#index"
  match "/chome" => "usershomes#usershome"
  match "/shome" => "supplieruser#supplieruser"
  match "/nhome" => "ngouser#ngouser"
  match "/faq" => 'pages#faq'
  match "/login" =>'sessions#new'
  match "/logout" =>'sessions#destroy'
  match "/signup" =>'undpusers#new'
  match "/undpusers/confirmation/:uni_id" => 'undpusers#confirmation'
  match "/vpas/listed/:vendor_type_id" => 'vpas#listed'
  match "/vsses/listed/:vendor_type_id/:vpa_id" => 'vsses#listed'
  match "/conservtypes/:undpuser_id/new" => 'conservtypes#new'
  match "/consultants/:undpuser_id/new" => 'consultants#new'
  match '/feedback' => "e_mails#feedback"
  match 'metrics' => 'system_wide#metrics'
end
